//package com.amber.amberdevice.logic;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.TimeZone;
//import java.util.TreeSet;
//import java.util.concurrent.TimeUnit;
//
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//
//import com.amber.logic.AmberTelematicsDailyDrivingFleetReportLogic;
//import com.amber.logic.AmberTelematicsDailyDrivingReportLogic;
//import com.amber.logic.AmberTelematicsMappingLogic;
//import com.amber.logic.AmberTelematicsSettingsLogic;
//import com.amber.logic.DevicesLogic;
//import com.amber.logic.ReportExecutor;
//import com.amber.logic.TripsLogic;
//import com.amber.logic.TripsSearchLogic;
//import com.amber.model.AmberTelematicsDailyDrivingReport;
//import com.amber.model.AmberTelematicsDailyFleetReport;
//import com.amber.model.AmberTelematicsMapping;
//import com.amber.model.AmberTelematicsSettings;
//import com.amber.model.GPSPoints;
//import com.amber.model.Trips;
//import com.amber.util.AmberDrivingReportType;
//import com.amber.util.Constants;
//import com.amber.util.InsuranceReportParameter;
//import com.amber.util.PropertyLoader;
//import com.amber.util.UtilFunction;
//import com.model.subdoc.TelematicsSettings.FatigueEvent;
//import com.model.subdoc.TelematicsSettings.IdlingEvents;
//import com.model.subdoc.TelematicsSettings.OutOfHoursEvents;
//import com.model.subdoc.TelematicsSettings.SpeedingEvents;
//import com.mongodb.BasicDBList;
//import com.mongodb.BasicDBObject;
//import com.mongodb.DBObject;
//
//public class AmberFleetTripsSearchLogic {
//	private static final Logger LOGGER = Logger.getLogger(AmberFleetTripsSearchLogic.class.getName());
//	private AmberTelematicsDailyDrivingReportLogic reportLogic = new AmberTelematicsDailyDrivingReportLogic();
//	private AmberTelematicsDailyDrivingFleetReportLogic fleetReportLogic = new AmberTelematicsDailyDrivingFleetReportLogic();
//	private TripsLogic tripsLogic = new TripsLogic();
//	private Date created_date = new Date();
//	private AmberTelematicsSettings telematicsSetting = null;
//	private boolean update_device_count = false;
//
//	public void getTripByAmberAuthTokenAndDate(AmberTelematicsSettings telematicsSetting, String amber_auth_token,
//			String fleet_id, Date day) {
//		getTripOnDayAndCalculate(telematicsSetting, amber_auth_token, fleet_id, null, day);
//		ReportExecutor.updateFleetSize(fleet_id, amber_auth_token, UtilFunction.getDateMonthYear(day));
//	}
//
//	public void getTripByAmberAuthTokenAndDate(AmberTelematicsSettings telematicsSetting, String amber_auth_token,
//			String fleet_id, Date today, Boolean isAllDay) {
//		BasicDBObject basicDBObject = new BasicDBObject();
//		basicDBObject.append(Constants.FIELD_AMBER_AUTH_TOKEN, amber_auth_token);
//		LOGGER.info("amber_auth_token=" + amber_auth_token + ";fleet_id=" + fleet_id + "; today=" + today + ";isAllDay="
//				+ isAllDay);
//		String dateMonthYear = UtilFunction.getDateMonthYear(today);
//		if (isAllDay) {
//			LOGGER.info("amber_auth_token=" + amber_auth_token);
//			Set<String> allDaysOfTripsByAmber = getAllDaysOfTripsByAmber(amber_auth_token);
//			boolean skipCurrentDate = false;
//			try {
//				skipCurrentDate = Boolean.parseBoolean(
//						PropertyLoader.getInstance().getProperty(Constants.PROP_SKIP_CURRENT_DATE).toString());
//			} catch (Exception e) {
//				LOGGER.error(Constants.PROP_SKIP_CURRENT_DATE + " property not configured properly;", e);
//			}
//			if (skipCurrentDate) {
//				allDaysOfTripsByAmber.remove(dateMonthYear);
//			}
//			LOGGER.info("Days=" + allDaysOfTripsByAmber);
//			for (String day : allDaysOfTripsByAmber) {
//				getTripOnDayAndCalculate(telematicsSetting, amber_auth_token, fleet_id, day, null);
//				// updateFleetSize(fleet_id, amber_auth_token, day);
//				ReportExecutor.updateFleetSize(fleet_id, amber_auth_token, day);
//			}
//		} else {
//			Date previous_day = new Date(today.getTime() - Constants.ONE_DAY);
//			getTripOnDayAndCalculate(telematicsSetting, amber_auth_token, fleet_id, null, previous_day);
//			// updateFleetSize(fleet_id, amber_auth_token,
//			// UtilFunction.getDateMonthYear(previous_day));
//			ReportExecutor.updateFleetSize(fleet_id, amber_auth_token, UtilFunction.getDateMonthYear(previous_day));
//		}
//		// System.out.println(ReportExecutor.fleet_size_count);
//	}
//
//	public void updateDeviceCountInFleetMapping() {
//		AmberTelematicsMappingLogic mappingLogic = new AmberTelematicsMappingLogic();
//		List<AmberTelematicsMapping> allRecords = mappingLogic.getAllRecords();
//		DevicesLogic devicesLogic = new DevicesLogic();
//		for (AmberTelematicsMapping telematicsMapping : allRecords) {
//			Long count = devicesLogic.getDeviceCountByFleetIdGroupIdSubGroupId(telematicsMapping.getFleetId(),null,null);
//			LOGGER.info("fleet id=" + telematicsMapping.getFleetId() + "; count=" + count);
//			mappingLogic.updateFleetSize(telematicsMapping, count);
//		}
//	}
//
//	public void updateDeviceCountInFleet(Date today) {
//		String dateMonthYear[] = UtilFunction.getDateMonthYear(today).split("_");
//		int date = Integer.parseInt(dateMonthYear[0]);
//		int month = Integer.parseInt(dateMonthYear[1]);
//		int year = Integer.parseInt(dateMonthYear[2]);
//		AmberTelematicsDailyDrivingFleetReportLogic fleetReportLogic = new AmberTelematicsDailyDrivingFleetReportLogic();
//		List<AmberTelematicsDailyFleetReport> allRecordsByDateMonthYear = fleetReportLogic
//				.getAllRecordsByDateMonthYear(date, month, year);
//		DevicesLogic devicesLogic = new DevicesLogic();
//		for (AmberTelematicsDailyFleetReport dailyFleetReport : allRecordsByDateMonthYear) {
//			long count = devicesLogic.getDeviceCountByFleetIdGroupIdSubGroupId(dailyFleetReport.getFleet_id(),null,null);
//			LOGGER.info("fleet id=" + dailyFleetReport.getFleet_id() + "; count=" + count);
//			fleetReportLogic.updateDeviceCount(dailyFleetReport, count);
//		}
//	}
//
//	private void getTripOnDayAndCalculate(AmberTelematicsSettings telematicsSetting, String amber_auth_token,
//			String fleet_id, String day, Date parsed_date) {
//		LOGGER.info("Amber token=" + amber_auth_token + "; fleet_id=" + fleet_id + ";day=" + day + "parsed_date="
//				+ parsed_date);
//		if (day != null) {
//			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy");
//			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
//			parsed_date = UtilFunction.getParsedDate(day, simpleDateFormat);
//		}
//		String timeZone = telematicsSetting.getTimeZone();
//		Long timeInMSFromTimeZone = UtilFunction.getTimeInMSFromTimeZone(timeZone);
//		List<Trips> trips = tripsLogic.getAllTripOfDayByAmber(amber_auth_token, parsed_date, timeInMSFromTimeZone);
//		LOGGER.info("Amber token=" + amber_auth_token + "; allRecords=" + trips.size());
//		if (trips.size() > 0) {
//			calculateReport(telematicsSetting, trips, amber_auth_token, fleet_id);
//
//		}
//	}
//
//	public void updateFleetSize(String fleet_id, String day, int count, int total_device_count,
//			int units_not_update_count, String report_type) {
//		AmberTelematicsDailyFleetReport modelByFleetId = fleetReportLogic.getModelByFleetIdAndDay(fleet_id, day);
//		if (modelByFleetId != null) {
//			modelByFleetId.setFleet_size(count);
//			BasicDBObject query = new BasicDBObject();
//			query.put(Constants.FIELD_FLEET_REPORT_FLEET_ID, fleet_id);
//			String day_month_year[] = day.split("_");
//			query.put(Constants.FIELD_AMBER_REPORT_YEAR, Integer.parseInt(day_month_year[2]));
//			query.put(Constants.FIELD_AMBER_REPORT_MONTH, Integer.parseInt(day_month_year[1]));
//			query.put(Constants.FIELD_AMBER_REPORT_DATE, Integer.parseInt(day_month_year[0]));
//			if (report_type.equals(AmberDrivingReportType.DAY.toString())) {
//				query.put(Constants.FIELD_AMBER_REPORT_TYPE, AmberDrivingReportType.DAY.toString());
//			}
//			if (report_type.equals(AmberDrivingReportType.WEEK.toString())) {
//				query.put(Constants.FIELD_AMBER_REPORT_TYPE, AmberDrivingReportType.WEEK.toString());
//				Date current_date = UtilFunction.getDateFromString(day.replaceAll("-|_", "/"));
//				String dateWeekMonthYear[] = UtilFunction.getDateWeekMonthYear(current_date).split("_");
//				int week_of_month = Integer.parseInt(dateWeekMonthYear[1]);
//				int week_of_year = UtilFunction.getWeekOfYear(current_date);
//				LOGGER.info("week_of_month=" + week_of_month + "; week_of_year=" + week_of_year);
//				query.put(Constants.FIELD_AMBER_REPORT_WEEK_OF_MONTH, week_of_month);
//				query.put(Constants.FIELD_AMBER_REPORT_WEEK_OF_YEAR, week_of_year);
//				query.remove(Constants.FIELD_AMBER_REPORT_DATE);
//			}
//			if (report_type.equals(AmberDrivingReportType.MONTH.toString())) {
//				query.put(Constants.FIELD_AMBER_REPORT_TYPE, AmberDrivingReportType.MONTH.toString());
//				query.remove(Constants.FIELD_AMBER_REPORT_DATE);
//			}
//			if (report_type.equals(AmberDrivingReportType.YEAR.toString())) {
//				query.put(Constants.FIELD_AMBER_REPORT_TYPE, AmberDrivingReportType.YEAR.toString());
//				query.remove(Constants.FIELD_AMBER_REPORT_DATE);
//				query.remove(Constants.FIELD_AMBER_REPORT_MONTH);
//			}
//			BasicDBObject document = new BasicDBObject();
//			document.append(Constants.FIELD_FLEET_REPORT_FLEET_SIZE, modelByFleetId.getFleet_size());
//			document.append(Constants.FIELD_TOTAL_DEVICE_COUNT, total_device_count);
//			document.append(Constants.FIELD_UNITS_NOT_UPDATE_COUNT, units_not_update_count);
//			LOGGER.info("query=" + query);
//			fleetReportLogic.update(document, query);
//		}
//	}
//
//	private Map<String, Float> getCountMap() {
//		Map<String, Float> count_map = new HashMap<String, Float>();
//		count_map.put(Constants.FIELD_AMBER_REPORT_OVER_SPEED_COUNT, 0.0f);
//		count_map.put(Constants.FIELD_AMBER_REPORT_IDEL_COUNT, 0.0f);
//		count_map.put(Constants.FIELD_AMBER_REPORT_HARSH_BREAK_COUNT, 0.0f);
//		count_map.put(Constants.FIELD_AMBER_REPORT_AFTER_HOUR_COUNT, 0.0f);
//		count_map.put(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_DAY_COUNT, 0.0f);
//		count_map.put(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_NIGHT_COUNT, 0.0f);
//		count_map.put(Constants.FIELD_AMBER_REPORT_DISTANCE, 0.0f);
//		count_map.put(Constants.FIELD_AMBER_REPORT_HARSH_ACCELERATION, 0.0f);
//		return count_map;
//	}
//
//	private void calculateReport(AmberTelematicsSettings telematicsSetting, List<Trips> trips, String amber_auth_token,
//			String fleet_id) {
//		this.telematicsSetting = telematicsSetting;
//		Map<String, Float> countMap = getCountMap();
//		InsuranceReportParameter parameter = InsuranceReportParameter.OVER_SPEED;
//		float idele_time_count = 0.0f;
//		float sb_event = 0.0f;
//		float sa_event = 0.0f;
//		Date trip_start_date = null;
//		float trip_count = 0;
//		float trip_run_time = 0.0f;
//		float idel_time = 0.0f;
//		float trip_count_min_distance = 0.0f;
//		float max_speed = 0;
//		float overspeed_duration = 0;
//		float overspeed_distance = 0;
//		for (Trips trip : trips) {
//			if (isValidTrip(trip)) {
//				trip_count++;
//				// only hr, min, sec
//				if (trip_start_date == null && trip.getStartTime() != null) {
//					trip_start_date = UtilFunction.getStartEndDayOfDate(trip.getStartTime(), true);
//				}
//				Long trip_starttime_in_ms = UtilFunction.getMsFromTime_(trip.getStartTime());
//				switch (parameter) {
//				case OVER_SPEED:
//					Map<String, Float> speed_info_details = calculateSpeedCount(trip);
//					Float calculateSpeedCount = (Float) speed_info_details.get(Constants.SPEEDING_EVENT);
//					calculateSpeedCount = calculateSpeedCount
//							+ countMap.get(Constants.FIELD_AMBER_REPORT_OVER_SPEED_COUNT);
//					countMap.put(Constants.FIELD_AMBER_REPORT_OVER_SPEED_COUNT, calculateSpeedCount);
//					float top_speed = speed_info_details.get(Constants.MAX_SPEED);
//					if (max_speed < top_speed) {
//						max_speed = top_speed;
//					}
//					overspeed_duration += speed_info_details.get(Constants.TIME_ON_OVER_SPEED);
//					overspeed_distance += speed_info_details.get(Constants.DISTANCE_WITH_OVER_SPEED);
//				case IDEL:
//					// TODO: replace 600000 with db value
//					IdlingEvents idlingEvents = telematicsSetting.getIdlingEvent();
//					if (idlingEvents != null) {
//						if (trip.getIdleTime() != null && trip.getIdleTime() >= (idlingEvents.getIdlingTime() * 1000)) {
//							idele_time_count++;
//						}
//					}
//				case HARSH_BREAKING:
//					try {
//						if (trip.getSB() != null && Integer.parseInt(trip.getSB()) > 0) {
//							sb_event++;
//						}
//					} catch (Exception e) {
//						LOGGER.error("String to number parse exception;input=" + trip.getSB());
//					}
//				case HARSH_ACCELERATION:
//					try {
//						if (trip.getHA() != null && Integer.parseInt(trip.getHA()) > 0) {
//							sa_event++;
//						}
//					} catch (Exception e) {
//						LOGGER.error("String to number parse exception;input=" + trip.getHA());
//					}
//				case AFTER_HOURS:
//					OutOfHoursEvents outOfHoursEvent = telematicsSetting.getOutOfHoursEvent();
//					if (outOfHoursEvent != null && isAfterHourEvent(trip)) {
//						float after_hr_event_count = countMap.get(Constants.FIELD_AMBER_REPORT_IDEL_COUNT) + 1;
//						countMap.put(Constants.FIELD_AMBER_REPORT_AFTER_HOUR_COUNT, after_hr_event_count);
//					}
//				case FATIGUE_DAY_DRIVE:
//					FatigueEvent fatigueDayEvent = telematicsSetting.getFatigueDayEvent();
//					if (fatigueDayEvent != null) {
//						String start_time[] = fatigueDayEvent.getStartTimeIP().split(":");
//						String end_time[] = fatigueDayEvent.getEndTimeIP().split(":");
//						long fatigue_day_start_in_ms = UtilFunction.getMsFromTime(start_time[0], start_time[1],
//								start_time[2]);
//						long fatigue_day_end_in_ms = UtilFunction.getMsFromTime(end_time[0], end_time[1], end_time[2]);
//						if (trip_starttime_in_ms != null && trip_starttime_in_ms >= fatigue_day_end_in_ms
//								&& trip_starttime_in_ms <= fatigue_day_start_in_ms && isFatigueDayDrive(trip,
//										fatigueDayEvent.getDriveDistance(), fatigueDayEvent.getDriveTime())) {
//							float after_hr_event_count = countMap
//									.get(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_DAY_COUNT) + 1;
//							countMap.put(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_DAY_COUNT, after_hr_event_count);
//						}
//					}
//				case FATIGUE_NIGHT_DRIVE:
//					FatigueEvent fatigueNightEvent = telematicsSetting.getFatigueNightEvent();
//					String start_time[] = fatigueNightEvent.getStartTimeIP().split(":");
//					String end_time[] = fatigueNightEvent.getEndTimeIP().split(":");
//					if (fatigueNightEvent != null) {
//						long fatigue_end_start_in_ms = UtilFunction.getMsFromTime(start_time[0], start_time[1],
//								start_time[2]);
//						long fatigue_end_end_in_ms = UtilFunction.getMsFromTime(end_time[0], end_time[1], end_time[2]);
//						if (trip_starttime_in_ms != null && trip_starttime_in_ms <= fatigue_end_start_in_ms
//								&& trip_starttime_in_ms >= fatigue_end_end_in_ms && isFatigueNightDrive(trip,
//										fatigueNightEvent.getDriveDistance(), fatigueNightEvent.getDriveTime())) {
//							float after_hr_event_count = countMap
//									.get(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_NIGHT_COUNT) + 1;
//							countMap.put(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_NIGHT_COUNT, after_hr_event_count);
//						}
//					}
//				case DISTANCE:
//					if (trip.getDistance() != null) {
//						float distance_in_km = trip.getDistance();
//						countMap.put(Constants.FIELD_AMBER_REPORT_DISTANCE,
//								countMap.get(Constants.FIELD_AMBER_REPORT_DISTANCE) + distance_in_km);
//					}
//				case DRIVE_TIME:
//					if (trip.getRunTime() != null) {
//						//float minutes = TimeUnit.MILLISECONDS.toSeconds(trip.getRunTime())/60;
//						 float minutes = trip.getRunTime()/60000;
//						trip_run_time = trip_run_time + minutes;
//						 
//					}
//				case IDLE_TIME:
//					if (trip.getIdleTime() != null) {
//						 float minutes =  trip.getIdleTime()/60000;
//						//float minutes = trip.getIdleTime()/60000;
//					 
//						idel_time = idel_time + minutes;
//					}
//				case TRIPS:
//					if (telematicsSetting.getTripsMinDistance() != null && trip.getDistance() != null
//							&& (trip.getDistance() / 1000) > telematicsSetting.getTripsMinDistance()) {
//						trip_count_min_distance++;
//					} else {
//						LOGGER.info("Trip min distance=" + telematicsSetting.getTripsMinDistance() + ";"
//								+ "Trip distance=" + trip.getDistance());
//					}
//				}
//			}
//		}
//		countMap.put(Constants.FIELD_AMBER_REPORT_DISTANCE, countMap.get(Constants.FIELD_AMBER_REPORT_DISTANCE) / 1000);
//		countMap.put(Constants.FIELD_AMBER_REPORT_IDEL_COUNT, idele_time_count);
//		countMap.put(Constants.FIELD_AMBER_REPORT_HARSH_BREAK_COUNT, sb_event);
//		countMap.put(Constants.FIELD_AMBER_REPORT_HARSH_ACCELERATION, sa_event);
//		countMap.put(Constants.FIELD_AMBER_REPORT_TRIP_COUNT, trip_count);
//		countMap.put(Constants.FIELD_AMBER_REPORT_DRIVE_TIME, trip_run_time);
//		countMap.put(Constants.FIELD_AMBER_REPORT_IDLE_TIME, idel_time);
//		countMap.put(Constants.FIELD_AMBER_REPORT_TRIPS, trip_count_min_distance);
//		countMap.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, 0.0f + max_speed);
//		overspeed_duration = (overspeed_duration / 1000) / 60;
//		countMap.put(Constants.FIELD_AMBER_REPORT_OVER_SPEED_DURATION, overspeed_duration);
//		countMap.put(Constants.FIELD_AMBER_REPORT_OVER_SPEED_DISTANCE, overspeed_distance);
//		LOGGER.info("countMap=" + countMap);
//		saveCountDetails(countMap, telematicsSetting.getTelematicsId(), amber_auth_token, fleet_id, "",
//				trip_start_date);
//	}
//
//	public boolean isValidTrip(Trips trip) {
//		return (trip.getDistance() > 100 && (trip.getRunTime()/(1000*60)) > 1);
//		// return true;
//	}
//
//
//	private void saveCountDetails(Map<String, Float> countMap, String telematics_id, String amber_auth_token,
//			String fleet_id, String polict_number, Date trip_start_date) {
//		AmberTelematicsDailyDrivingReport report = new AmberTelematicsDailyDrivingReport();
//		report.setAmberAuthToken(amber_auth_token);
//		report.setFleet_id(fleet_id);
//		report.setCreated_date(created_date);
//		LOGGER.info("created date=" + created_date);
//		report.setOver_speed_count(countMap.get(Constants.FIELD_AMBER_REPORT_OVER_SPEED_COUNT));
//		report.setIdel_event_count(countMap.get(Constants.FIELD_AMBER_REPORT_IDEL_COUNT));
//		report.setHarsh_break_event_count(countMap.get(Constants.FIELD_AMBER_REPORT_HARSH_BREAK_COUNT));
//		report.setAfter_hour_event_count(countMap.get(Constants.FIELD_AMBER_REPORT_AFTER_HOUR_COUNT));
//		report.setFatigue_driving_day_event(countMap.get(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_DAY_COUNT));
//		report.setFatigue_driving_night_event(countMap.get(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_NIGHT_COUNT));
//		report.setDistance(countMap.get(Constants.FIELD_AMBER_REPORT_DISTANCE));
//		report.setAcceleration(countMap.get(Constants.FIELD_AMBER_REPORT_HARSH_ACCELERATION));
//		report.setTrip_date(trip_start_date);
//		report.setTelematics_id(telematics_id);
//		report.setTotal_trip_count(countMap.get(Constants.FIELD_AMBER_REPORT_TRIP_COUNT));
//		report.setMin_distance_trip_count(countMap.get(Constants.FIELD_AMBER_REPORT_TRIPS));
//		report.setIdel_time(countMap.get(Constants.FIELD_AMBER_REPORT_IDLE_TIME));
//		report.setDrive_time(countMap.get(Constants.FIELD_AMBER_REPORT_DRIVE_TIME));
//		report.setDelerte_flag(Constants.DELETE_FLAG_N);
//		report.setMax_speed(countMap.get(Constants.FIELD_AMBER_REPORT_MAX_SPEED));
//		report.setOverspeed_duration(countMap.get(Constants.FIELD_AMBER_REPORT_OVER_SPEED_DURATION));
//		report.setOverspeed_distance(countMap.get(Constants.FIELD_AMBER_REPORT_OVER_SPEED_DISTANCE));
//		reportLogic.processAndSave(report);
//	}
//
//	private Boolean isFatigueNightDrive(Trips trip, String distance_str, String drive_time_str) {
//		boolean is_fatigue = false;
//		int distance = Integer.parseInt(distance_str);
//		int drive_time = Integer.parseInt(drive_time_str);
//		if (trip.getDistance() != null && (trip.getDistance() / 1000) >= distance) {
//			is_fatigue = true;
//		}
//		if (trip.getRunTime() != null) {
//			int run_time_in_minutes = (int) ((trip.getRunTime() / (1000 * 60)) % 60);
//			// TODO: replace 129 with param value
//			if (run_time_in_minutes >= (drive_time / 60)) {
//				is_fatigue = true;
//			}
//		}
//		return is_fatigue;
//	}
//
//	private Boolean isFatigueDayDrive(Trips trip, String distance_str, String drive_time_str) {
//		boolean is_fatigue = false;
//		int distance = Integer.parseInt(distance_str);
//		int drive_time = Integer.parseInt(drive_time_str);
//		if (!StringUtils.isEmpty(distance_str) && !StringUtils.isEmpty(drive_time_str)) {
//			if (trip.getDistance() != null && (trip.getDistance() / 1000) >= distance) {
//				is_fatigue = true;
//			}
//			if (trip.getRunTime() != null) {
//				int run_time_in_minutes = (int) ((trip.getRunTime() / (1000 * 60)) % 60);
//				// TODO: replace 240 with param value
//				if (run_time_in_minutes >= (drive_time / 60)) {
//					is_fatigue = true;
//				}
//			}
//		}
//		return is_fatigue;
//	}
//
//	private Boolean isAfterHourEvent(Trips trip) {
//		Date startTime = trip.getStartTime();
//		Date endTime = trip.getEndTime();
//		// TODO: Replace string values
//		OutOfHoursEvents outOfHoursEvent = this.telematicsSetting.getOutOfHoursEvent();
//		String start_time_cond_str[] = outOfHoursEvent.getStartTimeIP().split(":");
//		String end_time_cond_str[] = outOfHoursEvent.getEndTimeIP().split(":");
//		long start_time_cond_long = UtilFunction.getMsFromTime(start_time_cond_str[0], start_time_cond_str[1],
//				start_time_cond_str[2]);
//		long end_time_cond_long = UtilFunction.getMsFromTime(end_time_cond_str[0], end_time_cond_str[1],
//				end_time_cond_str[2]);
//
//		if (startTime != null) {
//			long from_trip_cond_long = UtilFunction.getMsFromTime_(startTime);
//			if (from_trip_cond_long >= start_time_cond_long || from_trip_cond_long <= end_time_cond_long) {
//				return true;
//			}
//		}
//		if (endTime != null) {
//			long end_trip_cond_long = UtilFunction.getMsFromTime_(endTime);
//			if (end_trip_cond_long >= start_time_cond_long || end_trip_cond_long <= end_time_cond_long) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	public Map<String, Float> calculateSpeedCount(Trips trip) {
//		SpeedingEvents speedingEvents = telematicsSetting.getSpeedingEvent();
//		LOGGER.info("Trip ID=" + trip.getTripId() + ";\n amber auth token:" + trip.getAmberAuthToken() + "\n"
//				+ "trip date=" + trip.getStartTime() + "\n" + trip.getEndTime());
//		LOGGER.info("Speed even limit=" + speedingEvents.getSpeedLimit());
//		Float speeding_event = 0.0f;
//		int max_speed = 0;
//		int time_on_over_speed = 0;
//		float distance_with_over_speed = 0;
//		if (speedingEvents != null) {
//			int i = 0;
//			try {
//				for (GPSPoints gpsPoints : trip.getGPSPoints()) {
//					// TODO: replace hot coded values
//					int speed = gpsPoints.getSpeed();
//					if (speed > max_speed) {
//						max_speed = speed;
//					}
//					if (speed >= speedingEvents.getSpeedLimit() && i <= trip.getGPSPoints().size() - 1) {
//						Integer timeDiff = trip.getGPSPoints().get(i + 1).getTimeDiff();
//						if (timeDiff != null && (timeDiff / 1000) > speedingEvents.getSpeedingTime()) {
//							speeding_event++;
//							time_on_over_speed = time_on_over_speed + timeDiff;
//							Float distanceDiff = trip.getGPSPoints().get(i + 1).getDistanceDiff();
//							if (distanceDiff != null && distanceDiff > 0) {
//								distance_with_over_speed = distance_with_over_speed + distanceDiff;
//							}
//						}
//					}
//					i++;
//					// speed=0;
//				}
//			} catch (Exception exception) {
//				LOGGER.error("speeding_event=" + exception);
//			}
//		}
//		LOGGER.info("speeding_event=" + speeding_event);
//		Map<String, Float> data = new HashMap<String, Float>();
//		data.put(Constants.SPEEDING_EVENT, speeding_event);
//		data.put(Constants.TIME_ON_OVER_SPEED, 0.0f + time_on_over_speed);
//		data.put(Constants.DISTANCE_WITH_OVER_SPEED, 0.0f + distance_with_over_speed);
//		data.put(Constants.MAX_SPEED, 0.0f + max_speed);
//		return data;
//	}
//
//	private Set<String> getAllDaysOfTripsByAmber(String amber_auth_token) {
//		Set<String> days = new TreeSet<String>();
//		BasicDBObject allQuery = new BasicDBObject();
//		allQuery.put(Constants.FIELD_AMBER_AUTH_TOKEN, amber_auth_token);
//		BasicDBObject fields = new BasicDBObject();
//		fields.put(Constants.FIELD_START_TIME, 1);
//		List<DBObject> allRecordsDBObject = tripsLogic.getAllRecordsDBObject(allQuery, fields, new BasicDBObject());
//		LOGGER.info("Amber token=" + amber_auth_token + "; size=" + allRecordsDBObject.size());
//		for (DBObject dbObject : allRecordsDBObject) {
//			if (dbObject.get(Constants.FIELD_START_TIME) != null) {
//				Date date = (Date) dbObject.get(Constants.FIELD_START_TIME);
//				days.add(UtilFunction.getDateMonthYear(date));
//			}
//		}
//		return days;
//	}
//
//	public AmberTelematicsSettings getTelematicsSetting() {
//		return telematicsSetting;
//	}
//
//	public void setTelematicsSetting(AmberTelematicsSettings telematicsSetting) {
//		this.telematicsSetting = telematicsSetting;
//	}
///*
//	public static void main(String a[]) {
//		AmberTelematicsSettingsLogic logic2 = new AmberTelematicsSettingsLogic();
//		BasicDBObject startDateQuer1y = new BasicDBObject();
//		startDateQuer1y.put("TelematicsId", "HHHOLLA1");
//		TripsSearchLogic logic1 = new TripsSearchLogic();
//		// logic1.setTelematicsSetting(logic2.getModelByQuery(startDateQuer1y));
//		// logic.getAllAmberAuthTokne(true);
//		// System.out.println(((1000000 / (1000 * 60)) % 60));
//		// System.out.println(14400/60);
//		// System.out.println(TimeUnit.MILLISECONDS.toMinutes(300000));
//		// System.out.println(new Date());
//		String amber_auth_token = "1PUV5S7RMTQK";
//		TripsLogic logic = new TripsLogic();
//		Date startDate = UtilFunction.getStartEndDayOfDate(UtilFunction.getDateFromString("01/01/2018"), true);
//		Date endDate = UtilFunction.getStartEndDayOfDate(UtilFunction.getDateFromString("31/01/2018"), false);
//		BasicDBObject startDateQuery = new BasicDBObject();
//		startDateQuery.append(Constants.FIELD_START_TIME, new BasicDBObject("$gte", startDate));
//		BasicDBObject endDateQuery = new BasicDBObject();
//		endDateQuery.append(Constants.FIELD_START_TIME, new BasicDBObject("$lte", endDate));
//		BasicDBList andValues = new BasicDBList();
//		andValues.add(startDateQuery);
//		andValues.add(endDateQuery);
//		andValues.add(new BasicDBObject(Constants.FIELD_AMBER_AUTH_TOKEN, amber_auth_token));
//		andValues.add(new BasicDBObject(Constants.FIELD_TRIP_COMPLETED, "Y"));
//		andValues.add(new BasicDBObject(Constants.FIELD_TRIP_DELETE_FLAG, "N"));
//		BasicDBObject query = new BasicDBObject("$and", andValues);
//		List<Trips> allRecords = logic.getAllRecords(query); 
//		Map<String, Float> data = new HashMap<String, Float>();
//		data.put(Constants.SPEEDING_EVENT, 0f);
//		data.put(Constants.TIME_ON_OVER_SPEED, 0.0f);
//		data.put(Constants.DISTANCE_WITH_OVER_SPEED, 0.0f);
//		data.put(Constants.MAX_SPEED, 0.0f);
//		for (Trips trips : allRecords) {
//			Map<String, Float> calculateSpeedCount = logic1.calculateSpeedCount(trips);
//			data.put(Constants.SPEEDING_EVENT,
//					calculateSpeedCount.get(Constants.SPEEDING_EVENT) + data.get(Constants.SPEEDING_EVENT));
//			data.put(Constants.TIME_ON_OVER_SPEED,
//					calculateSpeedCount.get(Constants.TIME_ON_OVER_SPEED) + data.get(Constants.TIME_ON_OVER_SPEED));
//			data.put(Constants.DISTANCE_WITH_OVER_SPEED, calculateSpeedCount.get(Constants.DISTANCE_WITH_OVER_SPEED)
//					+ data.get(Constants.DISTANCE_WITH_OVER_SPEED));
//			if (calculateSpeedCount.get(Constants.MAX_SPEED) > data.get(Constants.MAX_SPEED)) {
//				data.put(Constants.MAX_SPEED, calculateSpeedCount.get(Constants.MAX_SPEED));
//			}
//		} 
//	}
//	*/
//}
