//package com.amber.amberdevice.logic;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import org.apache.log4j.Logger;
//
//import com.amber.logic.AmberTelematicsDailyDrivingFleetReportLogic;
//import com.amber.logic.AmberTelematicsDailyDrivingReportLogic;
//import com.amber.logic.AmberTelematicsFleetRiskScoreLogic;
//import com.amber.logic.AmberTelematicsMappingLogic;
//import com.amber.logic.AmberTelematicsSettingsLogic;
//import com.amber.logic.DevicesLogic;
//import com.amber.model.AmberTelematicsDailyFleetReport;
//import com.amber.model.AmberTelematicsMapping;
//import com.amber.model.AmberTelematicsSettings;
//import com.amber.util.AmberDrivingReportType;
//import com.amber.util.Constants;
//import com.amber.util.UtilFunction;
//import com.mongodb.BasicDBObject;
//import com.mongodb.DBObject;
//
//public class AmberFleetReportProcessor {
//	private static final Logger LOGGER = Logger.getLogger(AmberFleetReportExecutor.class.getName());
//
//	private AmberTelematicsMappingLogic mappingLogic = new AmberTelematicsMappingLogic();
//	private AmberTelematicsFleetRiskScoreLogic fleetRiskScoreLogic = new AmberTelematicsFleetRiskScoreLogic();
//	private AmberTelematicsDailyDrivingFleetReportLogic fleetReportLogic = new AmberTelematicsDailyDrivingFleetReportLogic();
//	private AmberTelematicsSettingsLogic amberTelematicsSettings = new AmberTelematicsSettingsLogic();
//	private DevicesLogic devicesLogic = new DevicesLogic();
//	private AmberFleetTripsSearchLogic tripsSearchLogic = new AmberFleetTripsSearchLogic();
//	public static Map<String, Map<String, Set<String>>> fleet_size_count = new HashMap<String, Map<String, Set<String>>>();
//
//	public void clearExistingScore(AmberTelematicsMapping telematicsMapping) {
//		LOGGER.info("Clear existing score; fleet id=" + telematicsMapping.getFleetId() + "; telematics id="
//				+ telematicsMapping.getTelematicsId());
//		try {
//			fleetRiskScoreLogic.clearExistingScore(telematicsMapping.getTelematicsId(), telematicsMapping.getFleetId());
//		} catch (Exception e) {
//			LOGGER.error("Either telematics id or fleet id is null; failed to remove existing score", e);
//		}
//	}
//
//	public void clearExistingReport(AmberTelematicsMapping telematicsMapping) {
//		LOGGER.info("Clear existing score; fleet id=" + telematicsMapping.getFleetId() + "; telematics id="
//				+ telematicsMapping.getTelematicsId());
//		AmberTelematicsDailyDrivingReportLogic drivingReportLogic = new AmberTelematicsDailyDrivingReportLogic();
//		try {
//			drivingReportLogic.clearByFleetIdTelematicsId(telematicsMapping.getFleetId(),
//					telematicsMapping.getTelematicsId());
//		} catch (Exception e) {
//			LOGGER.error("fleet id or telematics id not exist; failed to clear daily report" + "," + telematicsMapping,
//					e);
//		}
//		AmberTelematicsDailyDrivingFleetReportLogic fleetReportLogic = new AmberTelematicsDailyDrivingFleetReportLogic();
//		try {
//			fleetReportLogic.clearByFleetIdTelematicsId(telematicsMapping.getFleetId(),
//					telematicsMapping.getTelematicsId());
//		} catch (Exception e) {
//			LOGGER.error("fleet id or telematics id not exist; failed to clear daily report" + "," + telematicsMapping,
//					e);
//		}
//	}
//
//	public void executeFleetScoreReset(AmberTelematicsMapping telematicsMapping) {
//		clearExistingScore(telematicsMapping);
//		List<AmberTelematicsDailyFleetReport> dailyFleetReports = fleetReportLogic
//				.getAllRecordsByTelematicsIdAndFleetId(telematicsMapping.getTelematicsId(),
//						telematicsMapping.getFleetId());
//		for (AmberTelematicsDailyFleetReport fleetReport : dailyFleetReports) {
//			if (fleetReport.getAmberDrivingReportType().equals(AmberDrivingReportType.MONTH)
//					|| fleetReport.getAmberDrivingReportType().equals(AmberDrivingReportType.YEAR)) {
//				fleetRiskScoreLogic.updateScore(fleetReport);
//			}
//		}
//	}
//
//	public void executeFleetReportReset(AmberTelematicsMapping telematicsMapping) {
//		normalExecution(true, telematicsMapping);
//	}
//
//	/**
//	 * Get Telematics settings by telematics id; If settings not found for the given
//	 * telematics id, by default amber telematics settings will be used for the
//	 * 
//	 * @param telematics_id
//	 * @return
//	 */
//	public AmberTelematicsSettings getTelematicsSettingByTelematicsId(String telematics_id) {
//		BasicDBObject allQuery = new BasicDBObject();
//		allQuery.append(Constants.FIELD_TELEMATICS_ID, telematics_id);
//		AmberTelematicsSettings modelByQuery = null;
//		modelByQuery = amberTelematicsSettings.getModelByQuery(allQuery);
//		if (modelByQuery == null) {
//
//		}
//		return modelByQuery;
//	}
//
//	public List<DBObject> getAllDeviceByFleetId(String fleet_id) {
//		List<DBObject> devices = new ArrayList<DBObject>();
//		BasicDBObject allQuery = new BasicDBObject();
//		allQuery.append(Constants.FIELD_FLEET_ID, fleet_id);
//		allQuery.append(Constants.FIELD_USER_MAPPED_STATUS, 1);
//		allQuery.append(Constants.FIELD_DEVICE_STATUS, "Y");
//		devices = devicesLogic.getAllRecordsDBObject(allQuery, new BasicDBObject(), new BasicDBObject());
//		LOGGER.info("Devices record size=" + devices.size());
//		return devices;
//	}
//
//	public void normalExecution(boolean isAllDay, AmberTelematicsMapping telematicsMapping) {
//		LOGGER.info(isAllDay + "," + telematicsMapping);
//		if (telematicsMapping.getFleetId() != null && telematicsMapping.getTelematicsId() != null) {
//			AmberTelematicsSettings telematicsSetting = getTelematicsSettingByTelematicsId(
//					telematicsMapping.getTelematicsId().toString());
//			List<DBObject> allDeviceByFleetId = getAllDeviceByFleetId(telematicsMapping.getFleetId());
//			for (DBObject dbObject : allDeviceByFleetId) {
//				LOGGER.info(dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN));
//				if (dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN) != null) {
//					tripsSearchLogic.getTripByAmberAuthTokenAndDate(telematicsSetting,
//							dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN).toString(),
//							telematicsMapping.getFleetId().toString(), new Date(), isAllDay);
//				}
//			}
//		}
//		updateCountDetails();
//	}
//
//	public int getUnitsNotUpdteCount(String fleet_id) {
//		String telematics_id = mappingLogic.getTelematicsIdByFleetId(fleet_id);
//		String url = Constants.API_URL_UNITS_NOT_UPDATE.replaceAll(Constants.URL_LABEL_FLEET_ID, fleet_id);
//		url = url.replaceAll(Constants.URL_LABEL_TELEMATICS_ID, telematics_id);
//		String units_not_update = UtilFunction.getContentFromURL(url);
//		try {
//			if (!units_not_update.isEmpty() && units_not_update.contains("InactiveDevice")) {
//				String data[] = units_not_update.split(",");
//				String count = data[1].replaceAll("\"InactiveDevice\":|,", "");
//				return Integer.parseInt(count);
//			}
//		} catch (Exception e) {
//			LOGGER.error("failed to get units not update count; input fleet id=" + fleet_id, e);
//		}
//		return 0;
//	}
//
//	public void updateCountDetails() {
//		LOGGER.info(fleet_size_count);
//		for (String fleet_id : fleet_size_count.keySet()) {
//			Set<String> keySet = fleet_size_count.get(fleet_id).keySet();
//			int units_not_update_count = getUnitsNotUpdteCount(fleet_id);
//			for (String day : keySet) {
//				int count = fleet_size_count.get(fleet_id).get(day).size();
//				Long total_device_count = devicesLogic.getDeviceCountByFleetIdGroupIdSubGroupId(fleet_id,null,null);
//				tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(),
//						units_not_update_count, AmberDrivingReportType.DAY.toString());
//				LOGGER.info("total device count=" + total_device_count + "; count=" + count + "; day=" + day);
//				if (day.equals(UtilFunction.getDateMonthYear(new Date(new Date().getTime() - 86400000)))) {
//					tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(),
//							units_not_update_count, AmberDrivingReportType.MONTH.toString());
//					tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(),
//							units_not_update_count, AmberDrivingReportType.WEEK.toString());
//					tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(),
//							units_not_update_count, AmberDrivingReportType.YEAR.toString());
//				}
//				if (UtilFunction.isEndOFMonth(day)) {
//					LOGGER.info("end of month update");
//					tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(),
//							units_not_update_count, AmberDrivingReportType.MONTH.toString());
//
//				}
//				if (UtilFunction.isEndOfWeek(day) || UtilFunction.isEndOFMonth(day)) {
//					LOGGER.info("end of week update");
//					tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(),
//							units_not_update_count, AmberDrivingReportType.WEEK.toString());
//				}
//				if (UtilFunction.isEndOfYear(day)) {
//					LOGGER.info("end of year update");
//					tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(),
//							units_not_update_count, AmberDrivingReportType.YEAR.toString());
//				}
//			}
//		}
//	}
//
//}
