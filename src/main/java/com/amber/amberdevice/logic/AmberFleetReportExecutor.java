//package com.amber.amberdevice.logic;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import org.apache.log4j.Logger;
//
//import com.amber.logic.AmberTelematicsMappingLogic;
//import com.amber.logic.DevicesLogic;
//import com.amber.model.AmberTelematicsMapping;
//import com.amber.util.Constants;
//
//public class AmberFleetReportExecutor {
//	private static final Logger LOGGER = Logger.getLogger(AmberFleetReportExecutor.class.getName());
//
//	private AmberTelematicsMappingLogic logic = new AmberTelematicsMappingLogic();
//
//	private DevicesLogic devicesLogic = new DevicesLogic();
//	private AmberFleetReportProcessor fleetReportProcessor = new AmberFleetReportProcessor();
//	public static Map<String, Map<String, Set<String>>> fleet_size_count = new HashMap<String, Map<String, Set<String>>>();
//
//	@SuppressWarnings("unchecked")
//	public void execute() {
//		List<String> distinctValuesOfField = (List<String>) devicesLogic
//				.getDistinctValuesOfField(Constants.FIELD_FLEET_ID);
//		for (String fleet_id : distinctValuesOfField) {
//			if (fleet_id != null && !fleet_id.isEmpty() && !fleet_id.equals("null")) {
//				AmberTelematicsMapping telematicsMapping = logic.getModelByFleetId(fleet_id);
//				if (telematicsMapping == null) {
//					telematicsMapping = logic.createModelByFleet(fleet_id);
//				}
//				executeByMappings(telematicsMapping);
//			}
//		}
//	}
//
//	public void executeByMappings(AmberTelematicsMapping telematicsMapping) {
//		boolean is_report_full_calculation = false;
//		boolean is_risk_score_full_calculation = false;
//		if (telematicsMapping.getIs_report_full_calculation() != null) {
//			is_report_full_calculation = telematicsMapping.getIs_report_full_calculation();
//		}
//		if (telematicsMapping.getIs_risk_score_full_calculation() != null) {
//			is_risk_score_full_calculation = telematicsMapping.getIs_risk_score_full_calculation();
//		}
//		LOGGER.info(is_report_full_calculation + "," + is_risk_score_full_calculation);
//		if (!is_report_full_calculation && !is_risk_score_full_calculation) {
//			LOGGER.info("Normal execution started- last day only");
//			fleetReportProcessor.normalExecution(false, telematicsMapping);
//		} else if (is_report_full_calculation && is_risk_score_full_calculation) {
//			LOGGER.info("Normal execution started- full day");
//			fleetReportProcessor.clearExistingScore(telematicsMapping);
//			fleetReportProcessor.clearExistingReport(telematicsMapping);
//			fleetReportProcessor.normalExecution(true, telematicsMapping);
//			logic.updateIsFullCalculationFalse(telematicsMapping.getBasicDBObject(), true, true);
//		} else {
//			if (is_report_full_calculation) {
//				fleetReportProcessor.clearExistingScore(telematicsMapping);
//				fleetReportProcessor.clearExistingReport(telematicsMapping);
//				fleetReportProcessor.executeFleetReportReset(telematicsMapping);
//				LOGGER.info("executeFleetReportReset");
//				logic.updateIsFullCalculationFalse(telematicsMapping.getBasicDBObject(), true, true);
//			}
//			if (is_risk_score_full_calculation) {
//				fleetReportProcessor.executeFleetScoreReset(telematicsMapping);
//				logic.updateIsFullCalculationFalse(telematicsMapping.getBasicDBObject(), false, true);
//			}
//		}
//	}
//
//	public static void main(String a[]) {
//		AmberFleetReportExecutor fleetReportExecutor = new AmberFleetReportExecutor();
//		fleetReportExecutor.execute();
//	}
//
//}
