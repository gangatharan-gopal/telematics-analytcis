package com.amber.cron;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import com.amber.util.Constants;
import com.amber.util.PropertyLoader;

public class CronExecutor {
	public static final String PROP_JOB_SCHEDULE = "JOB_SCHEDULE";

	public static void main(String a[]) {
		// screen name 88414 prod
		// screen name 5571 dev 
		JobDetail job = new JobDetail();
		job.setName("TelematicsReportJob");
		job.setJobClass(CronJob.class);
		CronTrigger trigger = new CronTrigger();
		trigger.setName("TelematicsReportTriggerName");
		try {
			// EVERY_ONE_MIN=0 0/1 * 1/1 * ? *
			// EVERY_DAY_BY_1_oCLOCK=0 0 13 1/1 * ? *
			PropertyLoader.getInstance().initPorperty();
			String job_setting = PropertyLoader.getInstance().getProperty(PROP_JOB_SCHEDULE);
			trigger.setCronExpression(job_setting);
			// schedule it
			Scheduler scheduler;
			try {
				scheduler = new StdSchedulerFactory().getScheduler();
				scheduler.start();
				scheduler.scheduleJob(job, trigger);
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			final Object charArray = Array.newInstance(Character.TYPE, 5);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
