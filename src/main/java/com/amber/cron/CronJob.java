package com.amber.cron;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.amber.exception.MongoDBConnectionException;
import com.amber.logic.ReportExecutor;

public class CronJob implements Job {
	private static final Logger LOGGER = Logger.getLogger(CronJob.class.getName());

	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			ReportExecutor executor = new ReportExecutor();
			executor.execute();
		} catch (MongoDBConnectionException e) {
			LOGGER.error("Failed to establish connection to mongo server; Plese check"
					+ "mongo instance is running or inputs provided in the config file", e);
		}
	}

}