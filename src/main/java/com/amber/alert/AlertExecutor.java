package com.amber.alert;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.amber.logic.DevicesLogic;
import com.amber.logic.NotificationAlertsLogic;
import com.amber.logic.TelematicsMappingLogic;
import com.amber.logic.TelematicsNotificationAlertsReportLogic;
import com.amber.model.Devices;
import com.amber.model.NotificationAlerts;
import com.amber.model.TelematicsMapping;
import com.amber.model.TripAggregation;
import com.amber.util.UtilFunction;

public class AlertExecutor implements Runnable {
	private static final Logger LOGGER = Logger.getLogger(AlertExecutor.class.getName());
	private DevicesLogic devicesLogic = DevicesLogic.getInstance();
	private NotificationAlertsLogic alertsLogic = new NotificationAlertsLogic();
	private TelematicsNotificationAlertsReportLogic alertsReportLogic = new TelematicsNotificationAlertsReportLogic();
	private TelematicsMappingLogic telematicsMappingLogic = new TelematicsMappingLogic();

	public void run() {
		execute();
	}

	public void execute() {
		List<TelematicsMapping> allRecords = telematicsMappingLogic.getAllRecords();
		for (TelematicsMapping telematicsMapping : allRecords) {
			boolean isFullCalculation = false;
			if (telematicsMapping.getIs_alert_full_calculation() == null || telematicsMapping.getIs_alert_full_calculation()) {
				isFullCalculation = true;
				alertsReportLogic.clearByFleetAndDeviceId(telematicsMapping.getFleetId());
			}
			executeDevicesByFleet(isFullCalculation, telematicsMapping.getFleetId());
			telematicsMappingLogic.updateAlertsFullCalculation(telematicsMapping, false);
		}
	}

	public void executeDevicesByFleet(boolean isFullCalculation, String fleet_id) {
		try {
			List<Devices> allModelByFleet = devicesLogic.getModelByFleet(fleet_id);
			LOGGER.info("Fleet id="+fleet_id+"; device count="+allModelByFleet.size());
			for (Devices devices : allModelByFleet) {
				LOGGER.info(devices.toJson());
				if (isFullCalculation) {
					// all notification alerts
					processAllDayNotification(devices);
				} else {
					// last day only
					processLastDayNotification(devices);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Failed to get device," + fleet_id);
		}
	}

	public void processLastDayNotification(Devices devices) {
		Date previous_day = new Date(System.currentTimeMillis() - 1000 * 60 * 60 * 24);
		String dateMonthYear[] = UtilFunction.getDateMonthYear(previous_day).split("_");
		processNotificationAlerts(Integer.parseInt(dateMonthYear[0]), Integer.parseInt(dateMonthYear[1]), Integer.parseInt(dateMonthYear[2]),
				devices);
	}

	public void processAllDayNotification(Devices devices) {
		ArrayList<TripAggregation> distinctAlertDateStringForAmberAuthToken = alertsLogic
				.getDistinctAlertDateStringForAmberAuthToken(devices.getDeviceAuthenticationToken());
		for (TripAggregation aggregation : distinctAlertDateStringForAmberAuthToken) {
			processNotificationAlerts(aggregation.get_id().getDay(), aggregation.get_id().getMonth(), aggregation.get_id().getYear(), devices);
		}
	}

	public void processNotificationAlerts(int day, int month, int year, Devices devices) {
		Date start_date = UtilFunction.getMongoDate("" + day, "" + month, "" + year, "00", "00", "00");
		Date end_date = UtilFunction.getMongoDate("" + day, "" + month, "" + year, "23", "59", "59");
		List<NotificationAlerts> alertsBetweenDates = this.alertsLogic.getAlertsBetweenDates(devices.getDeviceAuthenticationToken(), start_date,
				end_date);
		for (NotificationAlerts alerts : alertsBetweenDates) {
			System.out.println(alerts.getNotificationType() + "," + alerts.getDateCreated());
			alertsReportLogic.save(alerts.getNotificationType(), alerts.getDateCreated(), devices);
		}
	}

	public static void main(String a[]) {
		AlertExecutor alertExecutor = new AlertExecutor();
		 Thread start = new Thread(alertExecutor);
		 start.run();
//		Devices devices = new Devices();
//		devices.setDeviceAuthenticationToken("TEST");
//		devices.setFleetId("FleetId1");
//		devices.setGroupId("GroupId1");
//		devices.setSubGroupId("SubGroup1");
//		alertExecutor.processAllDayNotification(devices);
	}
}
