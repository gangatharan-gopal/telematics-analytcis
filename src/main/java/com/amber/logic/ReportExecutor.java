package com.amber.logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.amber.model.TelematicsDailyFleetReport;
import com.amber.model.TelematicsMapping;
import com.amber.model.TelematicsSettings;
import com.amber.model.Trips;
import com.amber.util.AmberDrivingReportType;
import com.amber.util.Constants;
import com.amber.util.ReportExecutionType;
import com.amber.util.UtilFunction;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class ReportExecutor {
	private static final Logger LOGGER = Logger.getLogger(ReportExecutor.class.getName());
	private TelematicsMappingLogic logic = new TelematicsMappingLogic();
	private DevicesLogic devicesLogic = DevicesLogic.getInstance();
	private TelematicsSettingsLogic settingsLogic = new TelematicsSettingsLogic();
	private TelematicsFleetRiskScoreLogic fleetRiskScoreLogic = new TelematicsFleetRiskScoreLogic();
	private TelematicsDailyDrivingFleetReportLogic fleetReportLogic = new TelematicsDailyDrivingFleetReportLogic();
	private TelematicsDailyDriverReportLogic dailyDriverReportLogic = new TelematicsDailyDriverReportLogic();
	private TelematicsDailyDrivingReportLogic dailyDrivingReportLogic = new TelematicsDailyDrivingReportLogic();
	private TelematicsEventReportLogic eventReportLogic = new TelematicsEventReportLogic();
	public static Map<String, Map<String, Set<String>>> fleet_size_count = new HashMap<String, Map<String, Set<String>>>();
	private TelematicsMappingLogic mappingLogic = new TelematicsMappingLogic();
	private Map<String, List<String>> fleet_group_id = new HashMap<String, List<String>>();
	private Map<String, HashMap<String, ArrayList<String>>> fleet_group_subgroup_id = new HashMap<String, HashMap<String, ArrayList<String>>>();
	private Map<String, List<TelematicsSettings>> fleetsettings = new HashMap<String, List<TelematicsSettings>>();

	private List<Date> getDatesToExecuteFromPropFile() {
		List<Date> dates = new ArrayList<Date>();
		Scanner date_file_scanner = null;
		try {
			date_file_scanner = new Scanner(new File("dates_to_execute.txt"));
			while (date_file_scanner.hasNextLine()) {
				String date = date_file_scanner.nextLine();
				if (!date.startsWith("**")) {
					dates.add(UtilFunction.getDateFromString(date));
				}
			}
			date_file_scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return dates;
	}

	public void execute_() {
		List<String> fleets_to_execute = UtilFunction.getFleetsFromConfigFile();
		for (String fleet : fleets_to_execute) {
			BasicDBObject basicDBObject = new BasicDBObject();
			basicDBObject.append(Constants.FIELD_DELETE_FLAG, "N");
			basicDBObject.append(Constants.FIELD_FLEET_ID, fleet);
			TelematicsMapping modelByQuery = logic.getModelByQuery(basicDBObject);
		}
	}

	public void execute() {
		BasicDBObject basicDBObject = new BasicDBObject();
		basicDBObject.append(Constants.FIELD_DELETE_FLAG, "N");
//		List<DBObject> allRecordsDBObject = logic.getAllRecordsDBObject(basicDBObject, new BasicDBObject(),
//				new BasicDBObject());
//		LOGGER.info("TelematicsMapping record size=" + allRecordsDBObject.size());
		FleetUsersLogic fleetUsersLogic = new FleetUsersLogic();
		List<String> fleet_ids = new ArrayList<String>();
		List<String> fleets_to_execute = UtilFunction.getFleetsFromConfigFile();
		for (String fleets : fleets_to_execute) {
			basicDBObject.append(Constants.FIELD_FLEET_ID, fleets);
			DBObject telematicsMapping = logic.getDBObjectByQuery(basicDBObject);
			String fleet_id = null;
			if (telematicsMapping != null && telematicsMapping.get(Constants.FIELD_FLEET_ID) != null) {
				fleet_id = telematicsMapping.get(Constants.FIELD_FLEET_ID).toString();
			} else {
				LOGGER.warn("Telematics mapping not exist for fleet id=" + basicDBObject.toString());
			}
			if (fleet_id != null && fleets_to_execute.contains(fleet_id)) {
				Map<String, List<String>> allFleetUsersByFleetId = fleetUsersLogic
						.getAllFleetUsersByFleetId(telematicsMapping.get(Constants.FIELD_FLEET_ID).toString());
				// String fleet_id = telematicsMapping.get(Constants.FIELD_FLEET_ID).toString();
				String telematics_id = telematicsMapping.get(Constants.FIELD_TELEMATICS_ID).toString();
				boolean is_event_report_generation = false;
				if (telematicsMapping.get(Constants.FIELD_IS_EVENT_REPORT_GENERATION) != null) {
					is_event_report_generation = (boolean) telematicsMapping
							.get(Constants.FIELD_IS_EVENT_REPORT_GENERATION);
				}
				if (is_event_report_generation) {
					fleet_ids.add(fleet_id);
				}
				TelematicsSettings fleet_telematicsSettings = null;
				switch (ReportExecutionType.FLEET) {
				case FLEET:
					fleet_telematicsSettings = settingsLogic.getModelByFleetIdGroupIdSubGroupId(telematics_id, fleet_id,
							null, null, Constants.FIELD_TELEMATICSSETTINGS_SETTING_TYPE_FLEET);
					if (fleet_telematicsSettings != null) {
						fleet_telematicsSettings.setIs_report_full_calculation(false);
						fleet_telematicsSettings.setIs_risk_score_full_calculation(false);
						fleet_telematicsSettings.setIs_event_report_generation(is_event_report_generation);
						LOGGER.info("Fleet=" + fleet_id);
						executeBySettings(fleet_telematicsSettings, null, null, is_event_report_generation);
						LOGGER.info("FLEET PROCESS COMPLETED");
					}
				case FLEET_GROUP:
					for (String group_id : allFleetUsersByFleetId.keySet()) {
						TelematicsSettings group_telematicsSettings = settingsLogic.getModelByFleetIdGroupIdSubGroupId(
								telematics_id, fleet_id, group_id, null, Constants.FIELD_RISK_SCORE_SETTING_TYPE_GROUP);
						LOGGER.info("Gr=" + group_id);
						boolean setting_created = false;
						if (group_telematicsSettings == null) {
							group_telematicsSettings = settingsLogic
									.createSettingsByFleetSetting(fleet_telematicsSettings, group_id, null);
							group_telematicsSettings.setIs_event_report_generation(is_event_report_generation);
							setting_created = true;
						}
						executeBySettings(fleet_telematicsSettings, group_telematicsSettings, setting_created,
								is_event_report_generation);
					}
					LOGGER.info("FLEET GROUP PROCESS COMPLETED");
				case FLEET_GROUP_SUBGROUP:
					for (String group_id : allFleetUsersByFleetId.keySet()) {
						for (String sub_group_id : allFleetUsersByFleetId.get(group_id)) {
							TelematicsSettings sub_group_telematicsSettings = settingsLogic
									.getModelByFleetIdGroupIdSubGroupId(telematics_id, fleet_id, group_id, sub_group_id,
											Constants.FIELD_RISK_SCORE_SETTING_TYPE_SUBGROUP);
							LOGGER.info("Gr=" + group_id + "; Sub grp=" + sub_group_id);
							boolean setting_created = false;
							if (sub_group_telematicsSettings == null) {
								sub_group_telematicsSettings = settingsLogic
										.createSettingsByFleetSetting(fleet_telematicsSettings, group_id, sub_group_id);
								sub_group_telematicsSettings.setIs_event_report_generation(is_event_report_generation);
								setting_created = true;
							}
							executeBySettings(fleet_telematicsSettings, sub_group_telematicsSettings, setting_created,
									is_event_report_generation);
						}
					}
					LOGGER.info("FLEET GROUP SUB GROUP PROCESS COMPLETED");
					break;
				}
				System.out.println("Fleet " + fleet_id + " process completed");
			}
		}
		String content = "";
		content = updateEventReportLocation(fleet_ids);
		fullCalculationExecution();
		content = content + "\n" + fleets_to_execute.toString();
		UtilFunction.sendJobMail(Constants.MAIL_SUB_TELEMATICS_DRIVING_REPORT, content);
	}

	public String updateEventReportLocation(List<String> fleet_ids) {
		TelematicsEventReportLogic telematicsEventReportLogic = new TelematicsEventReportLogic();
		String msg = "<br><br><b>GPSPointsUpdateSummary</b><br><html><head></head><body><table  border=\"1\"><tr><th> FleetId</th><th>TotalCompletedCount</th><th>TotalNotCompletedCount</th>"
				+ "<th>TotalCompletedCountByDate</th><th>TotalNotCompletedCountByDate</th>";
		Set<String> keySet = fleetsettings.keySet();
		for (String fleet_id : fleet_ids) {
			if (!keySet.contains(fleet_id)) {
				Date report_date = new Date(new Date().getTime() - (1000 * 60 * 60 * 24));
				telematicsEventReportLogic.processGPSLocationByDate(
						UtilFunction.getStartEndDayOfDate(report_date, true),
						UtilFunction.getStartEndDayOfDate(report_date, false), fleet_id);
				// if (telematicsEventReportLogic.getLocation_update_count() <
				// telematicsEventReportLogic.getMax_location_update_count()) {
				// telematicsEventReportLogic.checkAndUpdateOldGPSLocation(fleet_id);
				// }
				msg = msg + telematicsEventReportLogic.getCountMessageByFleetId(fleet_id, new Date());
			}
		}
		msg = msg + "</table></body></html>";
		// UtilFunction.sendJobMail(Constants.MAIL_SUB_TELEMATICS_DRIVING_REPORT, msg);
		return msg;
	}

	/**
	 * As of now it has not been used; Most of the time we reset is_full_cal flag to
	 * true; So for particular day calculation has not been called
	 */
	public void executeParticularDay() {
		BasicDBObject basicDBObject = new BasicDBObject();
		basicDBObject.append(Constants.FIELD_DELETE_FLAG, "N");
		List<DBObject> allRecordsDBObject = logic.getAllRecordsDBObject(basicDBObject, new BasicDBObject(),
				new BasicDBObject());
		LOGGER.info("TelematicsMapping record size=" + allRecordsDBObject.size());
		List<Date> datesToExecuteFromPropFile = getDatesToExecuteFromPropFile();
		for (DBObject telematicsMapping : allRecordsDBObject) {
			if (telematicsMapping.get(Constants.FIELD_FLEET_ID) != null
					&& telematicsMapping.get(Constants.FIELD_TELEMATICS_ID) != null) {
				List<TelematicsSettings> telematicsSettingByTelematicsId = getTelematicsSettingByTelematicsId(
						telematicsMapping.get(Constants.FIELD_TELEMATICS_ID).toString());
				for (TelematicsSettings telematicsSetting : telematicsSettingByTelematicsId) {
					List<DBObject> allDeviceByFleetId = getAllDeviceByFleetId(telematicsSetting.getFleetId(), null,
							null);
					processByFleetGroupSubGroupForDay(allDeviceByFleetId, telematicsSetting, telematicsMapping,
							datesToExecuteFromPropFile);
					allDeviceByFleetId = getAllDeviceByFleetId(telematicsSetting.getFleetId(),
							telematicsSetting.getGroupId(), null);
					allDeviceByFleetId = getAllDeviceByFleetId(telematicsSetting.getFleetId(),
							telematicsSetting.getGroupId(), telematicsSetting.getSubGroupId());
				}
			}
			// updateCountDetails();
		}
	}

	/**
	 * It is not used for now. Most of the time we reset the flag to true for full
	 * calculation Not tried and tested the execution for particular day
	 * 
	 * @param allDeviceByFleetId
	 * @param telematicsSetting
	 * @param telematicsMapping
	 * @param datesToExecuteFromPropFile
	 */
	public void processByFleetGroupSubGroupForDay(List<DBObject> allDeviceByFleetId,
			TelematicsSettings telematicsSetting, DBObject telematicsMapping, List<Date> datesToExecuteFromPropFile) {
		for (DBObject dbObject : allDeviceByFleetId) {
			LOGGER.info(dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN));
			if (dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN) != null) {
				for (Date date : datesToExecuteFromPropFile) {
					// tripsSearchLogic.getTripByAmberAuthTokenAndDate(telematicsSetting,
					// dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN).toString(),
					// telematicsMapping.get(Constants.FIELD_FLEET_ID).toString(), date);
				}
			}
		}
	}

	public void executeBySettings(TelematicsSettings fleet_telematicsSettings, TelematicsSettings telematicsSettings,
			Boolean setting_created, boolean is_event_report_generation) {
		Long deviceCount = 0l;
		if (fleet_telematicsSettings != null && telematicsSettings != null) {
			deviceCount = devicesLogic.getDeviceCountByFleetIdGroupIdSubGroupId(fleet_telematicsSettings.getFleetId(),
					telematicsSettings.getGroupId(), telematicsSettings.getSubGroupId());
			if (fleet_telematicsSettings.getIs_report_full_calculation() && telematicsSettings.getUpdatedBy() != null
					&& (telematicsSettings.getUpdatedBy().equals(Constants.TELEMATICS_REPORT_JOB))
					&& !setting_created) {
				settingsLogic.deleteByFleetIdGroupIdSubGroupId(telematicsSettings.getFleetId(),
						telematicsSettings.getGroupId(), telematicsSettings.getSubGroupId());
				LOGGER.info("Setting deleted  " + telematicsSettings.getFleetId() + ","
						+ telematicsSettings.getGroupId() + "," + telematicsSettings.getSubGroupId());
				if (deviceCount > 0) {
					telematicsSettings = settingsLogic.createSettingsByFleetSetting(fleet_telematicsSettings,
							telematicsSettings.getGroupId(), telematicsSettings.getSubGroupId());
					LOGGER.info("Setting created  " + telematicsSettings.getFleetId() + ","
							+ telematicsSettings.getGroupId() + "," + telematicsSettings.getSubGroupId());
				} else {
					LOGGER.info("Settings not created; Device count=" + deviceCount + "; fleet="
							+ fleet_telematicsSettings.getFleetId() + "; grp id=" + telematicsSettings.getGroupId()
							+ "sub grp id=" + telematicsSettings.getSubGroupId());
				}
			}
			telematicsSettings.setIs_event_report_generation(is_event_report_generation);
			if (telematicsSettings != null && deviceCount > 0) {
				normalExecution(telematicsSettings);
			}
		} else if (fleet_telematicsSettings != null && telematicsSettings == null) {
			normalExecution(fleet_telematicsSettings);
		}
	}

	public void executeByTelematicsSettings(TelematicsSettings telematicsSettings, boolean isAllDay) {
		try {
			List<DBObject> allDeviceByFleetId = getAllDeviceByFleetId(telematicsSettings.getFleetId(),
					telematicsSettings.getGroupId(), telematicsSettings.getSubGroupId());
			LOGGER.info(
					telematicsSettings.getSettingType() + "," + allDeviceByFleetId.size() + "," + allDeviceByFleetId);
			System.out.println(allDeviceByFleetId.size());
			for (DBObject dbObject : allDeviceByFleetId) {
				System.out.println(dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN));
			}
			processByFleetGroupSubGroup(allDeviceByFleetId, telematicsSettings, isAllDay);
		} catch (NullPointerException e) {
			LOGGER.error("Null pointer exception ", e);
		}
		System.out.println("Fleet id process completed");
	}

	public void fullCalculationExecution() {
		Set<String> keySet = fleetsettings.keySet();
		for (String fleet_id : keySet) {
			List<TelematicsSettings> settings = fleetsettings.get(fleet_id);
			for (TelematicsSettings telematicsSettings : settings) {
				try {
					this.fleetReportLogic.clearExistingReport(telematicsSettings);
					this.fleetRiskScoreLogic.clearExistingReport(telematicsSettings);
					this.dailyDriverReportLogic.clearExistingReport(telematicsSettings);
					this.dailyDrivingReportLogic.clearExistingReport(telematicsSettings);
					this.eventReportLogic.clearExistingReport(telematicsSettings.getFleetId(),
							telematicsSettings.getGroupId(), telematicsSettings.getSubGroupId(),
							telematicsSettings.getSettingType());
					executeByTelematicsSettings(telematicsSettings, true);
				} catch (Exception e) {
					LOGGER.error("Exception occured failed to calculation full calculation for" + settings);
				}
			}
		}
		fleetsettings = new HashMap<String, List<TelematicsSettings>>();
		updateEventReportLocation(new ArrayList<String>(keySet));

	}

	public void normalExecution(TelematicsSettings telematicsSettings) {
		LOGGER.info(telematicsSettings.getIs_report_full_calculation() + ","
				+ telematicsSettings.getIs_risk_score_full_calculation());
		if (!telematicsSettings.getIs_report_full_calculation()
				&& !telematicsSettings.getIs_risk_score_full_calculation()) {
			LOGGER.info("Normal execution started- last day only");
			executeByTelematicsSettings(telematicsSettings, false);
		} else if (telematicsSettings.getIs_report_full_calculation()
				&& telematicsSettings.getIs_risk_score_full_calculation()) {
			LOGGER.info("Normal execution started- full day");
			/*
			 * code changes for full calculation process has to run at last after event
			 * report calculation
			 * 
			 * this.fleetReportLogic.clearExistingReport(telematicsSettings);
			 * this.fleetRiskScoreLogic.clearExistingReport(telematicsSettings);
			 * this.dailyDriverReportLogic.clearExistingReport(telematicsSettings);
			 * this.dailyDrivingReportLogic.clearExistingReport(telematicsSettings);
			 * this.eventReportLogic.clearExistingReport(telematicsSettings.getFleetId(),
			 * telematicsSettings.getGroupId(), telematicsSettings.getSubGroupId(),
			 * telematicsSettings.getSettingType());
			 * executeByTelematicsSettings(telematicsSettings, true);
			 */
			String fleetId = telematicsSettings.getFleetId();
			List<TelematicsSettings> settings_list = null;
			if (fleetsettings.get(fleetId) == null) {
				settings_list = new ArrayList<TelematicsSettings>();
			} else {
				settings_list = fleetsettings.get(fleetId);
			}
			settings_list.add(telematicsSettings);
			fleetsettings.put(fleetId, settings_list);
		} else {
			if (telematicsSettings.getIs_report_full_calculation()) {
				this.fleetReportLogic.clearExistingReport(telematicsSettings);
				this.dailyDriverReportLogic.clearExistingReport(telematicsSettings);
				this.dailyDrivingReportLogic.clearExistingReport(telematicsSettings);
				this.eventReportLogic.clearExistingReport(telematicsSettings.getFleetId(),
						telematicsSettings.getGroupId(), telematicsSettings.getSubGroupId(),
						telematicsSettings.getSettingType());
				executeByTelematicsSettings(telematicsSettings, true);
				LOGGER.info("executeFleetReportReset");
			}
			if (telematicsSettings.getIs_risk_score_full_calculation()) {
				this.fleetRiskScoreLogic.clearExistingReport(telematicsSettings);
				executeFleetScoreReset(telematicsSettings);
			}
		}
	}

	public void processByFleetGroupSubGroup(List<DBObject> allDeviceByFleetId, TelematicsSettings settings,
			boolean isAllDay) {
		ExecutorService fixedExecutorService = Executors.newFixedThreadPool(1);
		TelematicsDailyDrivingFleetReportLogic fleetReportLogic = new TelematicsDailyDrivingFleetReportLogic();

		for (DBObject dbObject : allDeviceByFleetId) {
			LOGGER.info(dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN));
			if (dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN) != null) {
				Runnable tripsSearchLogic = new TripsSearchLogic(settings,
						dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN).toString(), settings.getFleetId(),
						new Date(), isAllDay, fleetReportLogic);
				fixedExecutorService.execute(tripsSearchLogic);

			}
		}
		fixedExecutorService.shutdown();
		while (!fixedExecutorService.isTerminated()) {
			try {
				Thread.sleep(5000);
			} catch (Exception e) {
				LOGGER.error("Failed to wait in processByFleetGroupSubGroup; update is full will be true; ", e);
			}
		}
		settingsLogic.updateIsFullCalculationFalse(settings, true, true);
	}

	public void updateFleetGroupId(String fleet_id, String group_id) {
		if (fleet_group_id.isEmpty() || !fleet_group_id.containsKey(fleet_id)) {
			fleet_group_id.put(fleet_id, new ArrayList<String>());
		}
		List<String> list = fleet_group_id.get(fleet_id);
		if (!list.contains(group_id)) {
			list.add(group_id);
		}
		fleet_group_id.put(fleet_id, list);
	}

	public void updateFleetGroupSubGroupId(String fleet_id, String group_id, String sub_group_id) {
		// fleet_group_subgroup_id
		if (fleet_group_subgroup_id.isEmpty() || !fleet_group_subgroup_id.containsKey(fleet_id)) {
			fleet_group_subgroup_id.put(fleet_id, new HashMap<String, ArrayList<String>>());
		}
		HashMap<String, ArrayList<String>> hashMap = fleet_group_subgroup_id.get(fleet_id);
		if (!hashMap.containsKey(group_id)) {
			hashMap.put(group_id, new ArrayList<String>());
		}
		ArrayList<String> arrayList = hashMap.get(group_id);
		arrayList.add(sub_group_id);
		hashMap.put(group_id, arrayList);
		fleet_group_subgroup_id.put(fleet_id, hashMap);
	}

	public int getUnitsNotUpdteCount(String fleet_id) {
		String telematics_id = mappingLogic.getTelematicsIdByFleetId(fleet_id);
		String url = Constants.API_URL_UNITS_NOT_UPDATE.replaceAll(Constants.URL_LABEL_FLEET_ID, fleet_id);
		url = url.replaceAll(Constants.URL_LABEL_TELEMATICS_ID, telematics_id);
		String units_not_update = UtilFunction.getContentFromURL(url);
		try {
			if (!units_not_update.isEmpty() && units_not_update.contains("InactiveDevice")) {
				String data[] = units_not_update.split(",");
				String count = data[1].replaceAll("\"InactiveDevice\":|,", "");
				return Integer.parseInt(count);
			}
		} catch (Exception e) {
			LOGGER.error("failed to get units not update count; input fleet id=" + fleet_id, e);
		}
		return 0;
	}

	// public void updateCountDetails() {
	// LOGGER.info(fleet_size_count);
	// for (String fleet_id : fleet_size_count.keySet()) {
	// Set<String> keySet = fleet_size_count.get(fleet_id).keySet();
	// int units_not_update_count = getUnitsNotUpdteCount(fleet_id);
	// for (String day : keySet) {
	// int count = fleet_size_count.get(fleet_id).get(day).size();
	// Long total_device_count =
	// devicesLogic.getDeviceCountByFleetIdGroupIdSubGroupId(fleet_id, null, null);
	// tripsSearchLogic.updateFleetSize(fleet_id, day, count,
	// total_device_count.intValue(), units_not_update_count,
	// AmberDrivingReportType.DAY.toString());
	// LOGGER.info("total device count=" + total_device_count + "; count=" + count +
	// "; day=" + day);
	// if (day.equals(UtilFunction.getDateMonthYear(new Date(new Date().getTime() -
	// 86400000)))) {
	// tripsSearchLogic.updateFleetSize(fleet_id, day, count,
	// total_device_count.intValue(), units_not_update_count,
	// AmberDrivingReportType.MONTH.toString());
	// tripsSearchLogic.updateFleetSize(fleet_id, day, count,
	// total_device_count.intValue(), units_not_update_count,
	// AmberDrivingReportType.WEEK.toString());
	// tripsSearchLogic.updateFleetSize(fleet_id, day, count,
	// total_device_count.intValue(), units_not_update_count,
	// AmberDrivingReportType.YEAR.toString());
	// }
	// if (UtilFunction.isEndOFMonth(day)) {
	// LOGGER.info("end of month update");
	// tripsSearchLogic.updateFleetSize(fleet_id, day, count,
	// total_device_count.intValue(), units_not_update_count,
	// AmberDrivingReportType.MONTH.toString());
	// }
	// if (UtilFunction.isEndOfWeek(day) || UtilFunction.isEndOFMonth(day)) {
	// LOGGER.info("end of week update");
	// tripsSearchLogic.updateFleetSize(fleet_id, day, count,
	// total_device_count.intValue(), units_not_update_count,
	// AmberDrivingReportType.WEEK.toString());
	// }
	// if (UtilFunction.isEndOfYear(day)) {
	// LOGGER.info("end of year update");
	// tripsSearchLogic.updateFleetSize(fleet_id, day, count,
	// total_device_count.intValue(), units_not_update_count,
	// AmberDrivingReportType.YEAR.toString());
	// }
	// }
	// }
	// }
	public void executeFleetScoreReset(TelematicsSettings telematicsSettings) {
		List<TelematicsDailyFleetReport> dailyFleetReports = fleetReportLogic.getAllRecordsByTelematicsIdAndFleetId(
				telematicsSettings.getFleetId(), telematicsSettings.getGroupId(), telematicsSettings.getSubGroupId());
		for (TelematicsDailyFleetReport fleetReport : dailyFleetReports) {
			if (fleetReport.getAmberDrivingReportType().equals(AmberDrivingReportType.MONTH)
					|| fleetReport.getAmberDrivingReportType().equals(AmberDrivingReportType.YEAR)) {
				fleetRiskScoreLogic.updateScore(fleetReport);
			}
		}
	}

	/**
	 * Get Telematics settings by telematics id; If settings not found for the given
	 * telematics id, by default amber telematics settings will be used for the
	 * 
	 * @param telematics_id
	 * @return
	 */
	public List<TelematicsSettings> getTelematicsSettingByTelematicsId(String telematics_id) {
		BasicDBObject allQuery = new BasicDBObject();
		allQuery.append(Constants.FIELD_TELEMATICS_ID, telematics_id);
		List<TelematicsSettings> allRecords = settingsLogic.getAllRecords(allQuery);
		return allRecords;
	}

	public List<DBObject> getAllDeviceByFleetIdGroupIdSubgroupId(String fleet_id, String group_id,
			String sub_group_id) {
		return getAllDeviceByFleetId(fleet_id, group_id, sub_group_id);
	}

	public List<DBObject> getAllDeviceByFleetIdGroupId(String fleet_id, String group_id) {
		return getAllDeviceByFleetId(fleet_id, group_id, null);
	}

	public List<DBObject> getAllDeviceByFleetId(String fleet_id, String group_id, String sub_group_id) {
		List<DBObject> devices = new ArrayList<DBObject>();
		BasicDBObject allQuery = new BasicDBObject();
		allQuery.append(Constants.FIELD_FLEET_ID, fleet_id);
		if (group_id != null) {
			allQuery.append(Constants.FIELD_GROUP_ID, group_id);
		}
		if (sub_group_id != null) {
			allQuery.append(Constants.FIELD_SUB_GROUP_ID, sub_group_id);
		}
		allQuery.append(Constants.FIELD_USER_MAPPED_STATUS, 1);
		allQuery.append(Constants.FIELD_DEVICE_STATUS, "Y");
		devices = devicesLogic.getAllRecordsDBObject(allQuery, new BasicDBObject(), new BasicDBObject());
		LOGGER.info("Devices record size=" + devices.size());
		return devices;
	}

	public static void updateFleetSize(String fleet_id, String amberauth_token, String day) {
		if (fleet_size_count.get(fleet_id) == null) {
			Map<String, Set<String>> day_amber_auth_token = new HashMap<String, Set<String>>();
			Set<String> amber_auth_token_set = new TreeSet<String>();
			day_amber_auth_token.put(day, amber_auth_token_set);
			fleet_size_count.put(fleet_id, day_amber_auth_token);
		}
		if (fleet_size_count.get(fleet_id).get(day) == null) {
			Map<String, Set<String>> day_amber_auth_token = fleet_size_count.get(fleet_id);
			Set<String> amber_auth_token_set = new TreeSet<String>();
			day_amber_auth_token.put(day, amber_auth_token_set);
			fleet_size_count.put(fleet_id, day_amber_auth_token);
		}
		Set<String> set = fleet_size_count.get(fleet_id).get(day);
		set.add(amberauth_token);
		Map<String, Set<String>> map = fleet_size_count.get(fleet_id);
		map.put(day, set);
		fleet_size_count.put(fleet_id, map);
	}

	public static void main1(String a[]) {
		TripsLogic tripsLogic = new TripsLogic();
		float total_distance = 0.0f;
		String nextLine = "C3FP5M8DB1G2";
		List<Trips> allTripOfDayByAmber = tripsLogic.getAllTripOfDayByAmber(nextLine,
				UtilFunction.getMongoDate("06", "07", "2018", "00", "00", "00"),
				UtilFunction.getTimeInMSFromTimeZone("-05:30"));
		float distance = 0.0f;
		for (Trips trips : allTripOfDayByAmber) {
			distance = distance + trips.getDistance();
			System.out.println(trips.getTripId());
		}
		System.out.println(nextLine + "," + distance);
		total_distance = total_distance + distance;
		System.out.println(total_distance);
		System.out.println("-------------14 end");
		allTripOfDayByAmber = tripsLogic.getAllTripOfDayByAmber(nextLine,
				UtilFunction.getMongoDate("15", "07", "2018", "00", "00", "00"),
				UtilFunction.getTimeInMSFromTimeZone("-05:30"));
		distance = 0.0f;
		for (Trips trips : allTripOfDayByAmber) {
			distance = distance + trips.getDistance();
			System.out.println(trips.getTripId());
		}
		System.out.println(nextLine + "," + distance);
		total_distance = total_distance + distance;
		System.out.println(total_distance);
		System.out.println("-------------15 end");
		// ArrayList<TripAggregation> tripDistanceCountByDayWeekMonthYear =
		// tripsLogic.getTripDistanceCountByDayWeekMonthYear("2TUNZ8JFB64P","day");
		// for(TripAggregation aggregation:tripDistanceCountByDayWeekMonthYear) {
		// System.out.println(aggregation);
		// }
	}

	public static void main_(String a[]) {
		ReportExecutor executor = new ReportExecutor();
		List<String> ids = new ArrayList<String>();
		ids.add("959506");
		executor.updateEventReportLocation(ids);
	}

	public static void main(String a[]) throws IOException {
		// Insuracne device score - 51034
		// Telematcis portal - 907525
		// Telematics dev - 26981
		// ------------------------------------------------
		// screen -r 15206(A)
		// screen -r 3500
		ReportExecutor executor = new ReportExecutor();
		executor.execute();
		// File f = new File("lock.bin");
		// if (!f.exists()) {
		// try {
		// f.createNewFile();
		// } catch (Exception exception) {
		// LOGGER.error("File creation failed" + exception);
		// }
		// executor.execute(true);
		// } else {
		// LOGGER.info("File created already");
		// executor.execute(false);
		// }
		// UtilFunction.sendJobMail(Constants.MAIL_SUB_TELEMATICS_DRIVING_REPORT);
		// executor.executeParticularDay();
	}
}
