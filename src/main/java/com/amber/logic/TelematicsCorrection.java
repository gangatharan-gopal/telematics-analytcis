package com.amber.logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.amber.model.Devices;
import com.amber.model.TelematicsSettings;

/**
 * TelematicsDailyDrivingReport - Day report has to be removed; "MONTH", "WEEK", "YEAR" - Report events has to be subtract by day report
 * @author HP
 *
 */
public class TelematicsCorrection {
	private TelematicsSettingsLogic settingsLogic = new TelematicsSettingsLogic();
	private TelematicsDailyDrivingFleetReportLogic dailyFleetReportLogic=new TelematicsDailyDrivingFleetReportLogic();
	private TelematicsDailyDriverReportLogic dailyDriverReportLogic=new TelematicsDailyDriverReportLogic();
	private TelematicsDailyDrivingReportLogic dailyDrivingReportLogic=new TelematicsDailyDrivingReportLogic();
	
	private DevicesLogic devicesLogic = new DevicesLogic();
	List<String> amber_tokens = new ArrayList<String>();

	private void setTokenMap(String fleet_id) {
		List<TelematicsSettings> allModelByFleetId = settingsLogic.getAllModelByFleetId(fleet_id);
		for (TelematicsSettings telematicsSettings : allModelByFleetId) {
			List<Devices> devices = devicesLogic.getDeviceByFleetIdGroupIdSubGroupId(fleet_id, telematicsSettings.getGroupId(),
					telematicsSettings.getSubGroupId());
			for (Devices device : devices) {
				amber_tokens.add(device.getDeviceAuthenticationToken());
			}
		}
	}

	private List<String> getAllDatesBetweenStartAndEnd(String start_date, String end_date) {
		return null;
	}

	private void updateReportByDateAndToken(String date) {
		for (String amber_auth_token : amber_tokens) {
		}
	}

	public void executeByDates(String fleet_id, String dates[]) {
		setTokenMap(fleet_id);
		for (String date : dates) {
			updateReportByDateAndToken(date);
		}
	}

	public void exectueByDateRange(String fleet_id, String start_date, String end_date) {
		setTokenMap(fleet_id);
		List<String> dates = getAllDatesBetweenStartAndEnd(start_date, end_date);
		for (String date : dates) {
			updateReportByDateAndToken(date);
		}
	}

	public static void main(String a[]) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("*** Telematics Correction ***");
		System.out.println("-----------------------------");
		System.out.println("Enter the Fleet ");
		String fleet_id = reader.readLine();
		System.out.println("[1] By List of Dates(, separated). [2] Date Range ");
		System.out.println("Date format dd-MM-yyyy [Ex. 20-12-1986]");
		String option = reader.readLine();
		if (option.equals("1")) {
			String dates_str = reader.readLine();
			String date[] = dates_str.split(",");
		} else if (option.equals("2")) {
			System.out.println("Start Date ?");
			String start_date = reader.readLine();
			System.out.println("End Date ?");
			String end_date = reader.readLine();
		}
	}
}
