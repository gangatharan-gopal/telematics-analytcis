//package com.amber.logic;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Scanner;
//import java.util.Set;
//import java.util.TreeSet;
//
//import org.apache.log4j.Logger;
//
//import com.amber.model.TelematicsDailyFleetReport;
//import com.amber.model.TelematicsSettings;
//import com.amber.util.AmberDrivingReportType;
//import com.amber.util.Constants;
//import com.amber.util.UtilFunction;
//import com.mongodb.BasicDBObject;
//import com.mongodb.DBObject;
//
//public class ReportExecutor2 {
//	private static final Logger LOGGER = Logger.getLogger(ReportExecutor2.class.getName());
//	private TelematicsMappingLogic logic = new TelematicsMappingLogic();
//	private DevicesLogic devicesLogic = new DevicesLogic();
//	private TripsSearchLogic tripsSearchLogic = new TripsSearchLogic();
//	private TelematicsSettingsLogic settingsLogic = new TelematicsSettingsLogic();
//	private TelematicsFleetRiskScoreLogic fleetRiskScoreLogic = new TelematicsFleetRiskScoreLogic();
//	private TelematicsDailyDrivingFleetReportLogic fleetReportLogic = new TelematicsDailyDrivingFleetReportLogic();
//	TelematicsDailyDriverReportLogic dailyDriverReportLogic = new TelematicsDailyDriverReportLogic();
//	public static Map<String, Map<String, Set<String>>> fleet_size_count = new HashMap<String, Map<String, Set<String>>>();
//	private TelematicsMappingLogic mappingLogic = new TelematicsMappingLogic();
//	private Map<String, List<String>> fleet_group_id = new HashMap<String, List<String>>();
//	private Map<String, HashMap<String, ArrayList<String>>> fleet_group_subgroup_id = new HashMap<String, HashMap<String, ArrayList<String>>>();
//
//	private List<Date> getDatesToExecuteFromPropFile() {
//		List<Date> dates = new ArrayList<Date>();
//		Scanner date_file_scanner = null;
//		try {
//			date_file_scanner = new Scanner(new File("dates_to_execute.txt"));
//			while (date_file_scanner.hasNextLine()) {
//				String date = date_file_scanner.nextLine();
//				if (!date.startsWith("**")) {
//					dates.add(UtilFunction.getDateFromString(date));
//				}
//			}
//			date_file_scanner.close();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//		return dates;
//	}
//
//	public void executeParticularDay() {
//		BasicDBObject basicDBObject = new BasicDBObject();
//		basicDBObject.append(Constants.FIELD_DELETE_FLAG, "N");
//		List<DBObject> allRecordsDBObject = logic.getAllRecordsDBObject(basicDBObject, new BasicDBObject(), new BasicDBObject());
//		LOGGER.info("TelematicsMapping record size=" + allRecordsDBObject.size());
//		List<Date> datesToExecuteFromPropFile = getDatesToExecuteFromPropFile();
//		for (DBObject telematicsMapping : allRecordsDBObject) {
//			if (telematicsMapping.get(Constants.FIELD_FLEET_ID) != null && telematicsMapping.get(Constants.FIELD_TELEMATICS_ID) != null) {
//				List<TelematicsSettings> telematicsSettingByTelematicsId = getTelematicsSettingByTelematicsId(
//						telematicsMapping.get(Constants.FIELD_TELEMATICS_ID).toString());
//				for (TelematicsSettings telematicsSetting : telematicsSettingByTelematicsId) {
//					List<DBObject> allDeviceByFleetId = getAllDeviceByFleetId(telematicsSetting.getFleetId(), null, null);
//					processByFleetGroupSubGroupForDay(allDeviceByFleetId, telematicsSetting, telematicsMapping, datesToExecuteFromPropFile);
//					allDeviceByFleetId = getAllDeviceByFleetId(telematicsSetting.getFleetId(), telematicsSetting.getGroupId(), null);
//					allDeviceByFleetId = getAllDeviceByFleetId(telematicsSetting.getFleetId(), telematicsSetting.getGroupId(),
//							telematicsSetting.getSubGroupId());
//				}
//			}
//			updateCountDetails();
//		}
//	}
//
//	public void processByFleetGroupSubGroupForDay(List<DBObject> allDeviceByFleetId, TelematicsSettings telematicsSetting, DBObject telematicsMapping,
//			List<Date> datesToExecuteFromPropFile) {
//		for (DBObject dbObject : allDeviceByFleetId) {
//			LOGGER.info(dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN));
//			if (dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN) != null) {
//				for (Date date : datesToExecuteFromPropFile) {
//					tripsSearchLogic.getTripByAmberAuthTokenAndDate(telematicsSetting,
//							dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN).toString(),
//							telematicsMapping.get(Constants.FIELD_FLEET_ID).toString(), date);
//				}
//			}
//		}
//	}
//
//	public void execute(boolean isAllDay) {
//		BasicDBObject basicDBObject = new BasicDBObject();
//		basicDBObject.append(Constants.FIELD_DELETE_FLAG, "N");
//		List<DBObject> allRecordsDBObject = logic.getAllRecordsDBObject(basicDBObject, new BasicDBObject(), new BasicDBObject());
//		LOGGER.info("TelematicsMapping record size=" + allRecordsDBObject.size());
//		for (DBObject telematicsMapping : allRecordsDBObject) {
//			boolean is_report_full_calculation = false;
//			boolean is_risk_score_full_calculation = false;
//			if (telematicsMapping.get(Constants.FIELD_REPORT_FULL_CALCULATION) != null) {
//				is_report_full_calculation = (Boolean) telematicsMapping.get(Constants.FIELD_REPORT_FULL_CALCULATION);
//			}
//			if (telematicsMapping.get(Constants.FIELD_RISK_SCORE_FULL_CALCULATION) != null) {
//				is_risk_score_full_calculation = (Boolean) telematicsMapping.get(Constants.FIELD_RISK_SCORE_FULL_CALCULATION);
//			}
//			LOGGER.info(is_report_full_calculation + "," + is_risk_score_full_calculation);
//			if (!is_report_full_calculation && !is_risk_score_full_calculation) {
//				LOGGER.info("Normal execution started- last day only");
//				normalExecution(false, telematicsMapping);
//			} else if (is_report_full_calculation && is_risk_score_full_calculation) {
//				LOGGER.info("Normal execution started- full day");
//				clearExistingScore(telematicsMapping);
//				clearExistingReport(telematicsMapping);
//				normalExecution(true, telematicsMapping);
//				logic.updateIsFullCalculationFalse(telematicsMapping, true, true);
//			} else {
//				if (is_report_full_calculation) {
//					clearExistingScore(telematicsMapping);
//					clearExistingReport(telematicsMapping);
//					executeFleetReportReset(telematicsMapping);
//					LOGGER.info("executeFleetReportReset");
//					logic.updateIsFullCalculationFalse(telematicsMapping, true, true);
//				}
//				if (is_risk_score_full_calculation) {
//					executeFleetScoreReset(telematicsMapping);
//					logic.updateIsFullCalculationFalse(telematicsMapping, false, true);
//				}
//			}
//		}
//		UtilFunction.sendJobMail(Constants.MAIL_SUB_TELEMATICS_DRIVING_REPORT);
//	}
//
//	private void clearExistingReport(DBObject telematicsMapping) {
//		TelematicsDailyDrivingReportLogic drivingReportLogic = new TelematicsDailyDrivingReportLogic();
//		try {
//			drivingReportLogic.clearByFleetIdTelematicsId(telematicsMapping.get(Constants.FIELD_FLEET_ID).toString(),
//					telematicsMapping.get(Constants.FIELD_TELEMATICS_ID).toString());
//		} catch (Exception e) {
//			LOGGER.error("fleet id or telematics id not exist; failed to clear daily report" + "," + telematicsMapping, e);
//		}
//		TelematicsDailyDrivingFleetReportLogic fleetReportLogic = new TelematicsDailyDrivingFleetReportLogic();
//		try {
//			fleetReportLogic.clearByFleetIdTelematicsId(telematicsMapping.get(Constants.FIELD_FLEET_ID).toString(),
//					telematicsMapping.get(Constants.FIELD_TELEMATICS_ID).toString());
//		} catch (Exception e) {
//			LOGGER.error("fleet id or telematics id not exist; failed to clear daily report" + "," + telematicsMapping, e);
//		}
//	}
//
//	public void executeByTelematicsSettings(TelematicsSettings fleet_telematicsSettings, TelematicsSettings telematicsSettings, boolean isAllDay,
//			DBObject telematicsMapping) {
//		if ("true".equals(telematicsMapping.get(Constants.FIELD_REPORT_FULL_CALCULATION).toString())
//				&& "true".equals(telematicsMapping.get(Constants.FIELD_RISK_SCORE_FULL_CALCULATION).toString())
//				&& telematicsSettings.getUpdatedBy() != null && telematicsSettings.getUpdatedBy().equals(Constants.TELEMATICS_REPORT_JOB)) {
//			settingsLogic.deleteByFleetIdGroupIdSubGroupId(telematicsSettings.getFleetId(), telematicsSettings.getGroupId(),
//					telematicsSettings.getSubGroupId());
//			fleet_telematicsSettings.setGroupId(telematicsSettings.getGroupId());
//			fleet_telematicsSettings.setSubGroupId(telematicsSettings.getSubGroupId());
//			telematicsSettings = settingsLogic.save(fleet_telematicsSettings.getBasicDBObjectCopy());
//		} else if (fleet_telematicsSettings.getIs_report_full_calculation()) {
//			dailyDriverReportLogic.deleteByFleetGroupSubGroupId(fleet_telematicsSettings.getFleetId(), fleet_telematicsSettings.getGroupId(),
//					fleet_telematicsSettings.getSubGroupId());
//		}
//		try {
//			List<DBObject> allDeviceByFleetId = getAllDeviceByFleetId(telematicsSettings.getFleetId(), telematicsSettings.getGroupId(),
//					telematicsSettings.getSubGroupId());
//			processByFleetGroupSubGroup(allDeviceByFleetId, telematicsSettings, telematicsMapping, isAllDay);
//		} catch (NullPointerException e) {
//			LOGGER.error("Null pointer exception ", e);
//		}
//		System.out.println("Fleet id process completed");
//	}
//
//	public void normalExecution(boolean isAllDay, DBObject telematicsMapping) {
////		LOGGER.info(isAllDay + "," + telematicsMapping);
////		if (telematicsMapping.get(Constants.FIELD_FLEET_ID) != null && telematicsMapping.get(Constants.FIELD_TELEMATICS_ID) != null) {
////			FleetUsersLogic fleetUsersLogic = new FleetUsersLogic();
////			Map<String, List<String>> allFleetUsersByFleetId = fleetUsersLogic
////					.getAllFleetUsersByFleetId(telematicsMapping.get(Constants.FIELD_FLEET_ID).toString());
////			Set<String> keySet = allFleetUsersByFleetId.keySet();
////			TelematicsSettings modelByFleetId = settingsLogic
////					.getModelByFleetIdGroupIdSubGroupId(telematicsMapping.get(Constants.FIELD_FLEET_ID).toString(), null, null);
////			if (modelByFleetId != null) {
////				executeByTelematicsSettings(null, modelByFleetId, isAllDay, telematicsMapping);
////				for (String group_ids : keySet) {
////					TelematicsSettings modelByFleetIdGroupId = settingsLogic
////							.getModelByFleetIdGroupIdSubGroupId(telematicsMapping.get(Constants.FIELD_FLEET_ID).toString(), group_ids, null);
////					if (modelByFleetIdGroupId == null && modelByFleetId != null) {
////						modelByFleetId.setGroupId(group_ids);
////						modelByFleetIdGroupId = settingsLogic.save(modelByFleetId.getBasicDBObjectCopy());
////						executeByTelematicsSettings(modelByFleetId, modelByFleetIdGroupId, isAllDay, telematicsMapping);
////					}
////					List<String> sub_group_ids = allFleetUsersByFleetId.get(group_ids);
////					for (String fleet_ids : sub_group_ids) {
////						TelematicsSettings modelByFleetIdGroupIdSubGroupId = settingsLogic
////								.getModelByFleetIdGroupIdSubGroupId(telematicsMapping.get(Constants.FIELD_FLEET_ID).toString(), group_ids, fleet_ids);
////						if (modelByFleetIdGroupIdSubGroupId == null && modelByFleetId != null) {
////							modelByFleetId.setGroupId(group_ids);
////							modelByFleetId.setSubGroupId(fleet_ids);
////							modelByFleetIdGroupIdSubGroupId = settingsLogic.save(modelByFleetId.getBasicDBObjectCopy());
////							executeByTelematicsSettings(modelByFleetId, modelByFleetIdGroupIdSubGroupId, isAllDay, telematicsMapping);
////						}
////					}
////				}
////			} else {
////				LOGGER.warn("Telematics Settings not found for this fleet id;" + telematicsMapping.get(Constants.FIELD_FLEET_ID).toString());
////			}
////		}
////		updateCountDetails();
//	}
//
//	public void processByFleetGroupSubGroup(List<DBObject> allDeviceByFleetId, TelematicsSettings settings, DBObject telematicsMapping,
//			boolean isAllDay) {
//		for (DBObject dbObject : allDeviceByFleetId) {
//			LOGGER.info(dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN));
//			if (dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN) != null) {
//				tripsSearchLogic.getTripByAmberAuthTokenAndDate(settings, dbObject.get(Constants.FIELD_DEVICE_AUTHENTICATION_TOKEN).toString(),
//						telematicsMapping.get(Constants.FIELD_FLEET_ID).toString(), new Date(), isAllDay);
//			}
//		}
//	}
//
//	public void updateFleetGroupId(String fleet_id, String group_id) {
//		if (fleet_group_id.isEmpty() || !fleet_group_id.containsKey(fleet_id)) {
//			fleet_group_id.put(fleet_id, new ArrayList<String>());
//		}
//		List<String> list = fleet_group_id.get(fleet_id);
//		if (!list.contains(group_id)) {
//			list.add(group_id);
//		}
//		fleet_group_id.put(fleet_id, list);
//	}
//
//	public void updateFleetGroupSubGroupId(String fleet_id, String group_id, String sub_group_id) {
//		// fleet_group_subgroup_id
//		if (fleet_group_subgroup_id.isEmpty() || !fleet_group_subgroup_id.containsKey(fleet_id)) {
//			fleet_group_subgroup_id.put(fleet_id, new HashMap<String, ArrayList<String>>());
//		}
//		HashMap<String, ArrayList<String>> hashMap = fleet_group_subgroup_id.get(fleet_id);
//		if (!hashMap.containsKey(group_id)) {
//			hashMap.put(group_id, new ArrayList<String>());
//		}
//		ArrayList<String> arrayList = hashMap.get(group_id);
//		arrayList.add(sub_group_id);
//		hashMap.put(group_id, arrayList);
//		fleet_group_subgroup_id.put(fleet_id, hashMap);
//	}
//
//	public int getUnitsNotUpdteCount(String fleet_id) {
//		String telematics_id = mappingLogic.getTelematicsIdByFleetId(fleet_id);
//		String url = Constants.API_URL_UNITS_NOT_UPDATE.replaceAll(Constants.URL_LABEL_FLEET_ID, fleet_id);
//		url = url.replaceAll(Constants.URL_LABEL_TELEMATICS_ID, telematics_id);
//		String units_not_update = UtilFunction.getContentFromURL(url);
//		try {
//			if (!units_not_update.isEmpty() && units_not_update.contains("InactiveDevice")) {
//				String data[] = units_not_update.split(",");
//				String count = data[1].replaceAll("\"InactiveDevice\":|,", "");
//				return Integer.parseInt(count);
//			}
//		} catch (Exception e) {
//			LOGGER.error("failed to get units not update count; input fleet id=" + fleet_id, e);
//		}
//		return 0;
//	}
//
//	public void updateCountDetails() {
//		LOGGER.info(fleet_size_count);
//		for (String fleet_id : fleet_size_count.keySet()) {
//			Set<String> keySet = fleet_size_count.get(fleet_id).keySet();
//			int units_not_update_count = getUnitsNotUpdteCount(fleet_id);
//			for (String day : keySet) {
//				int count = fleet_size_count.get(fleet_id).get(day).size();
//				Long total_device_count = 0l;
//				// Long total_device_count = devicesLogic.getDeviceCountByFleetId(fleet_id);
//				// tripsSearchLogic.updateFleetSize(fleet_id, day, count,
//				// total_device_count.intValue(),
//				// units_not_update_count, AmberDrivingReportType.DAY.toString());
//				LOGGER.info("total device count=" + total_device_count + "; count=" + count + "; day=" + day);
//				if (day.equals(UtilFunction.getDateMonthYear(new Date(new Date().getTime() - 86400000)))) {
//					tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(), units_not_update_count,
//							AmberDrivingReportType.MONTH.toString());
//					tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(), units_not_update_count,
//							AmberDrivingReportType.WEEK.toString());
//					tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(), units_not_update_count,
//							AmberDrivingReportType.YEAR.toString());
//				}
//				if (UtilFunction.isEndOFMonth(day)) {
//					LOGGER.info("end of month update");
//					tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(), units_not_update_count,
//							AmberDrivingReportType.MONTH.toString());
//				}
//				if (UtilFunction.isEndOfWeek(day) || UtilFunction.isEndOFMonth(day)) {
//					LOGGER.info("end of week update");
//					tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(), units_not_update_count,
//							AmberDrivingReportType.WEEK.toString());
//				}
//				if (UtilFunction.isEndOfYear(day)) {
//					LOGGER.info("end of year update");
//					tripsSearchLogic.updateFleetSize(fleet_id, day, count, total_device_count.intValue(), units_not_update_count,
//							AmberDrivingReportType.YEAR.toString());
//				}
//			}
//		}
//	}
//
//	public void executeFleetReportReset(DBObject telematicsMapping) {
//		logic.updateByTelematisIdAndFleetId(telematicsMapping.get(Constants.FIELD_TELEMATICS_ID).toString(),
//				telematicsMapping.get(Constants.FIELD_FLEET_ID).toString());
//		fleetRiskScoreLogic.updateByTelematisIdAndFleetId(telematicsMapping.get(Constants.FIELD_TELEMATICS_ID).toString(),
//				telematicsMapping.get(Constants.FIELD_FLEET_ID).toString());
//		normalExecution(true, telematicsMapping);
//	}
//
//	public void executeFleetScoreReset(DBObject telematicsMapping) {
//		clearExistingScore(telematicsMapping);
//		fleetRiskScoreLogic.updateByTelematisIdAndFleetId(telematicsMapping.get(Constants.FIELD_TELEMATICS_ID).toString(),
//				telematicsMapping.get(Constants.FIELD_FLEET_ID).toString());
//		/* null at end added to fix build errorx */
//		List<TelematicsDailyFleetReport> dailyFleetReports = fleetReportLogic.getAllRecordsByTelematicsIdAndFleetId(
//				telematicsMapping.get(Constants.FIELD_TELEMATICS_ID).toString(), telematicsMapping.get(Constants.FIELD_FLEET_ID).toString(), null);
//		for (TelematicsDailyFleetReport fleetReport : dailyFleetReports) {
//			if (fleetReport.getAmberDrivingReportType().equals(AmberDrivingReportType.MONTH)
//					|| fleetReport.getAmberDrivingReportType().equals(AmberDrivingReportType.YEAR)) {
//				fleetRiskScoreLogic.updateScore(fleetReport);
//			}
//		}
//	}
//
//	public void clearExistingScore(DBObject telematicsMapping) {
//		try {
//			fleetRiskScoreLogic.clearExistingScore(telematicsMapping.get(Constants.FIELD_TELEMATICS_ID).toString(),
//					telematicsMapping.get(Constants.FIELD_FLEET_ID).toString());
//		} catch (Exception e) {
//			LOGGER.error("Either telematics id or fleet id is null; failed to remove existing score", e);
//		}
//	}
//
//	/**
//	 * Get Telematics settings by telematics id; If settings not found for the given
//	 * telematics id, by default amber telematics settings will be used for the
//	 * 
//	 * @param telematics_id
//	 * @return
//	 */
//	public List<TelematicsSettings> getTelematicsSettingByTelematicsId(String telematics_id) {
//		BasicDBObject allQuery = new BasicDBObject();
//		allQuery.append(Constants.FIELD_TELEMATICS_ID, telematics_id);
//		TelematicsSettings modelByQuery = null;
//		List<TelematicsSettings> allRecords = settingsLogic.getAllRecords(allQuery);
//		return allRecords;
//	}
//
//	public List<DBObject> getAllDeviceByFleetIdGroupIdSubgroupId(String fleet_id, String group_id, String sub_group_id) {
//		return getAllDeviceByFleetId(fleet_id, group_id, sub_group_id);
//	}
//
//	public List<DBObject> getAllDeviceByFleetIdGroupId(String fleet_id, String group_id) {
//		return getAllDeviceByFleetId(fleet_id, group_id, null);
//	}
//
//	public List<DBObject> getAllDeviceByFleetId(String fleet_id, String group_id, String sub_group_id) {
//		List<DBObject> devices = new ArrayList<DBObject>();
//		BasicDBObject allQuery = new BasicDBObject();
//		allQuery.append(Constants.FIELD_FLEET_ID, fleet_id);
//		if (group_id != null) {
//			allQuery.append(Constants.FIELD_GROUP_ID, group_id);
//		}
//		if (sub_group_id != null) {
//			allQuery.append(Constants.FIELD_SUB_GROUP_ID, sub_group_id);
//		}
//		allQuery.append(Constants.FIELD_USER_MAPPED_STATUS, 1);
//		allQuery.append(Constants.FIELD_DEVICE_STATUS, "Y");
//		devices = devicesLogic.getAllRecordsDBObject(allQuery, new BasicDBObject(), new BasicDBObject());
//		LOGGER.info("Devices record size=" + devices.size());
//		return devices;
//	}
//
//	public static void updateFleetSize(String fleet_id, String amberauth_token, String day) {
//		if (fleet_size_count.get(fleet_id) == null) {
//			Map<String, Set<String>> day_amber_auth_token = new HashMap<String, Set<String>>();
//			Set<String> amber_auth_token_set = new TreeSet<String>();
//			day_amber_auth_token.put(day, amber_auth_token_set);
//			fleet_size_count.put(fleet_id, day_amber_auth_token);
//		}
//		if (fleet_size_count.get(fleet_id).get(day) == null) {
//			Map<String, Set<String>> day_amber_auth_token = fleet_size_count.get(fleet_id);
//			Set<String> amber_auth_token_set = new TreeSet<String>();
//			day_amber_auth_token.put(day, amber_auth_token_set);
//			fleet_size_count.put(fleet_id, day_amber_auth_token);
//		}
//		Set<String> set = fleet_size_count.get(fleet_id).get(day);
//		set.add(amberauth_token);
//		Map<String, Set<String>> map = fleet_size_count.get(fleet_id);
//		map.put(day, set);
//		fleet_size_count.put(fleet_id, map);
//	}
//
//	public static void main(String a[]) throws IOException {
//		// screen -r 15206
//		ReportExecutor2 executor = new ReportExecutor2();
//		File f = new File("lock.bin");
//		if (!f.exists()) {
//			try {
//				f.createNewFile();
//			} catch (Exception exception) {
//				LOGGER.error("File creation failed" + exception);
//			}
//			executor.execute(true);
//		} else {
//			LOGGER.info("File created already");
//			executor.execute(false);
//		}
//		// UtilFunction.sendJobMail(Constants.MAIL_SUB_TELEMATICS_DRIVING_REPORT);
//		// executor.executeParticularDay();
//	}
//}
