package com.amber.logic;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.amber.event.FatigueDayEventCalculator;
import com.amber.event.FatigueNightEventCalculator;
import com.amber.event.HarshAccelarationEventCalculator;
import com.amber.event.HarshBreakEventCalculator;
import com.amber.event.IdleTimeEventCalculator;
import com.amber.event.OverSpeedEventCalculator;
import com.amber.event.TelematicsEventsCalculator;
import com.amber.model.TelematicsDailyDrivingReport;
import com.amber.model.TelematicsDailyFleetReport;
import com.amber.model.TelematicsSettings;
import com.amber.model.Trips;
import com.amber.util.AmberDrivingReportType;
import com.amber.util.Constants;
import com.amber.util.InsuranceReportParameter;
import com.amber.util.PropertyLoader;
import com.amber.util.UtilFunction;
import com.model.subdoc.TelematicsSettings.OutOfHoursEvents;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class TripsSearchLogic implements Runnable {
	private static final Logger LOGGER = Logger.getLogger(TripsSearchLogic.class.getName());
	private TelematicsDailyDrivingReportLogic reportLogic = new TelematicsDailyDrivingReportLogic();
	private TelematicsDailyDrivingFleetReportLogic fleetReportLogic = null;
	private TelematicsDailyDriverReportLogic dailyDriverReportLogic = new TelematicsDailyDriverReportLogic();
	private TripsLogic tripsLogic = new TripsLogic();
	private Date created_date = new Date();
	private boolean update_device_count = false;
	// code change 02-07-2018
	private TelematicsSettings telematicsSetting = null;
	private String amber_auth_token;
	private String fleet_id;
	private Date today;
	private Boolean isAllDay;

	public TripsSearchLogic(TelematicsSettings telematicsSetting, String amber_auth_token, String fleet_id, Date today,
			Boolean isAllDay, TelematicsDailyDrivingFleetReportLogic fleetReportLogic) {
		this.telematicsSetting = telematicsSetting;
		setAmber_auth_token(amber_auth_token);
		setFleet_id(fleet_id);
		setToday(today);
		setIsAllDay(isAllDay);
		this.fleetReportLogic = fleetReportLogic;
	}

	public void run() {
		getTripByAmberAuthTokenAndDate(getAmber_auth_token(), getFleet_id(), getToday(), getIsAllDay());
	}

	public String getAmber_auth_token() {
		return amber_auth_token;
	}

	public void setAmber_auth_token(String amber_auth_token) {
		this.amber_auth_token = amber_auth_token;
	}

	public String getFleet_id() {
		return fleet_id;
	}

	public void setFleet_id(String fleet_id) {
		this.fleet_id = fleet_id;
	}

	public Date getToday() {
		return today;
	}

	public void setToday(Date today) {
		this.today = today;
	}

	public Boolean getIsAllDay() {
		return isAllDay;
	}

	public void setIsAllDay(Boolean isAllDay) {
		this.isAllDay = isAllDay;
	}

	public void getTripByAmberAuthTokenAndDate(TelematicsSettings telematicsSetting, String amber_auth_token,
			String fleet_id, Date day) {
		// getTripOnDayAndCalculate(telematicsSetting, amber_auth_token, fleet_id, null,
		// day);
		ReportExecutor.updateFleetSize(fleet_id, amber_auth_token, UtilFunction.getDateMonthYear(day));
	}

	public void getTripByAmberAuthTokenAndDate(String amber_auth_token, String fleet_id, Date today, Boolean isAllDay) {
		/**
		 * As per Raja Request, full calculation process will not taken. isAllDay set as
		 * false;
		 */
		isAllDay = false;
		BasicDBObject basicDBObject = new BasicDBObject();
		basicDBObject.append(Constants.FIELD_AMBER_AUTH_TOKEN, amber_auth_token);
		LOGGER.info("amber_auth_token=" + amber_auth_token + ";fleet_id=" + fleet_id + "; today=" + today + ";isAllDay="
				+ isAllDay);
		String dateMonthYear = UtilFunction.getDateMonthYear(today);
		String key = telematicsSetting.getFleetId();
		key = telematicsSetting.getGroupId() != null ? key + "," + telematicsSetting.getGroupId() : key;
		key = telematicsSetting.getSubGroupId() != null
				? key + "," + telematicsSetting.getGroupId() + "," + telematicsSetting.getSubGroupId()
				: key;
		if (isAllDay) {
			LOGGER.info("amber_auth_token=" + amber_auth_token);
			Set<String> allDaysOfTripsByAmber = getAllDaysOfTripsByAmber(amber_auth_token);
			boolean skipCurrentDate = false;
			try {
				skipCurrentDate = Boolean.parseBoolean(
						PropertyLoader.getInstance().getProperty(Constants.PROP_SKIP_CURRENT_DATE).toString());
			} catch (Exception e) {
				LOGGER.error(Constants.PROP_SKIP_CURRENT_DATE + " property not configured properly;", e);
			}
			if (skipCurrentDate) {
				allDaysOfTripsByAmber.remove(dateMonthYear);
			}
			LOGGER.info("Days=" + allDaysOfTripsByAmber);
			for (String day : allDaysOfTripsByAmber) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy");
				simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
				Date trip_date = UtilFunction.getParsedDate(day, simpleDateFormat);
				getTripOnDayAndCalculate(amber_auth_token, fleet_id, day, null, trip_date);
				// updateFleetSize(fleet_id, amber_auth_token, day);
				ReportExecutor.updateFleetSize(key, amber_auth_token, day);
			}
		} else {
			// TODO: un comment this line;
			Date previous_day = new Date(today.getTime() - Constants.ONE_DAY);
			previous_day = UtilFunction.getStartEndDayOfDate(previous_day, true);
			getTripOnDayAndCalculate(amber_auth_token, fleet_id, null, previous_day, previous_day);
			ReportExecutor.updateFleetSize(key, amber_auth_token, UtilFunction.getDateMonthYear(previous_day));

		}
		// System.out.println(ReportExecutor.fleet_size_count);
	}

	// public void updateDeviceCountInFleetMapping() {
	// TelematicsMappingLogic mappingLogic = new TelematicsMappingLogic();
	// List<TelematicsMapping> allRecords = mappingLogic.getAllRecords();
	// DevicesLogic devicesLogic = new DevicesLogic();
	// for (TelematicsMapping telematicsMapping : allRecords) {
	// Long count =
	// devicesLogic.getDeviceCountByFleetId(telematicsMapping.getFleetId());
	// LOGGER.info("fleet id=" + telematicsMapping.getFleetId() + "; count=" +
	// count);
	// mappingLogic.updateFleetSize(telematicsMapping, count);
	// }
	// }
	public List<String> getDates() {
		List<String> dates = new ArrayList<String>();
		dates.add("01/10/2018");
		dates.add("02/10/2018");
		dates.add("03/10/2018");
		dates.add("04/10/2018");
		dates.add("05/10/2018");
		dates.add("06/10/2018");
		dates.add("07/10/2018");
		return dates;
	}

	public void updateDeviceCountInFleet(Date today) {
		String dateMonthYear[] = UtilFunction.getDateMonthYear(today).split("_");
		int date = Integer.parseInt(dateMonthYear[0]);
		int month = Integer.parseInt(dateMonthYear[1]);
		int year = Integer.parseInt(dateMonthYear[2]);
		TelematicsDailyDrivingFleetReportLogic fleetReportLogic = new TelematicsDailyDrivingFleetReportLogic();
		List<TelematicsDailyFleetReport> allRecordsByDateMonthYear = fleetReportLogic.getAllRecordsByDateMonthYear(date,
				month, year);
		DevicesLogic devicesLogic = DevicesLogic.getInstance();
		for (TelematicsDailyFleetReport dailyFleetReport : allRecordsByDateMonthYear) {
			long count = devicesLogic.getDeviceCountByFleetIdGroupIdSubGroupId(dailyFleetReport.getFleet_id(),
					dailyFleetReport.getGroupId(), dailyFleetReport.getSubGroupId());
			LOGGER.info("fleet id=" + dailyFleetReport.getFleet_id() + "; count=" + count);
			fleetReportLogic.updateDeviceCount(dailyFleetReport, count);
		}
	}

	private void getTripOnDayAndCalculate(String amber_auth_token, String fleet_id, String day, Date parsed_date,
			Date trip_date) {
		LOGGER.info("Amber token=" + amber_auth_token + "; fleet_id=" + fleet_id + ";day=" + day + "parsed_date="
				+ parsed_date);
		if (day != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd_MM_yyyy");
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			parsed_date = UtilFunction.getParsedDate(day, simpleDateFormat);
		}
		try {
			String timeZone = telematicsSetting.getTimeZone();
			Long timeInMSFromTimeZone = UtilFunction.getTimeInMSFromTimeZone(timeZone);
			List<Trips> trips = tripsLogic.getAllTripOfDayByAmber(amber_auth_token, parsed_date, timeInMSFromTimeZone);
			LOGGER.info("Amber token=" + amber_auth_token + "; allRecords=" + trips.size());
			if (trips.size() > 0) {
				calculateReport(trips, amber_auth_token, fleet_id, trip_date);
			}
		} catch (Exception e) {
			LOGGER.info(telematicsSetting.toJson());
			LOGGER.error("Exception occured;" + telematicsSetting.getTimeZone(), e);
		}
	}

	public void updateFleetSize(String fleet_id, String day, int count, int total_device_count,
			int units_not_update_count, String report_type) {
		String[] split = fleet_id.split(",");
		TelematicsDailyFleetReport modelByFleetId = null;
		BasicDBObject query = new BasicDBObject();
		if (split.length == 3) {
			modelByFleetId = fleetReportLogic.getModelByFleetIdAndDay(split[0], split[1], split[2], day);
			query.put(Constants.FIELD_FLEET_ID, split[0]);
			query.put(Constants.FIELD_GROUP_ID, split[1]);
			query.put(Constants.FIELD_SUB_GROUP_ID, split[2]);
		} else if (split.length == 2) {
			modelByFleetId = fleetReportLogic.getModelByFleetIdAndDay(split[0], split[1], null, day);
			query.put(Constants.FIELD_FLEET_ID, split[0]);
			query.put(Constants.FIELD_GROUP_ID, split[1]);
		} else if (split.length == 1) {
			modelByFleetId = fleetReportLogic.getModelByFleetIdAndDay(split[0], null, null, day);
			query.put(Constants.FIELD_FLEET_ID, split[0]);
		}
		if (modelByFleetId != null) {
			String day_month_year[] = day.split("_");
			query.put(Constants.FIELD_AMBER_REPORT_YEAR, Integer.parseInt(day_month_year[2]));
			query.put(Constants.FIELD_AMBER_REPORT_MONTH, Integer.parseInt(day_month_year[1]));
			query.put(Constants.FIELD_AMBER_REPORT_DATE, Integer.parseInt(day_month_year[0]));
			if (report_type.equals(AmberDrivingReportType.DAY.toString())) {
				query.put(Constants.FIELD_AMBER_REPORT_TYPE, AmberDrivingReportType.DAY.toString());
			}
			if (report_type.equals(AmberDrivingReportType.WEEK.toString())) {
				query.put(Constants.FIELD_AMBER_REPORT_TYPE, AmberDrivingReportType.WEEK.toString());
				Date current_date = UtilFunction.getDateFromString(day.replaceAll("-|_", "/"));
				String dateWeekMonthYear[] = UtilFunction.getDateWeekMonthYear(current_date).split("_");
				int week_of_month = Integer.parseInt(dateWeekMonthYear[1]);
				int week_of_year = UtilFunction.getWeekOfYear(current_date);
				LOGGER.info("week_of_month=" + week_of_month + "; week_of_year=" + week_of_year);
				query.put(Constants.FIELD_AMBER_REPORT_WEEK_OF_MONTH, week_of_month);
				query.put(Constants.FIELD_AMBER_REPORT_WEEK_OF_YEAR, week_of_year);
				query.remove(Constants.FIELD_AMBER_REPORT_DATE);
			}
			if (report_type.equals(AmberDrivingReportType.MONTH.toString())) {
				query.put(Constants.FIELD_AMBER_REPORT_TYPE, AmberDrivingReportType.MONTH.toString());
				query.remove(Constants.FIELD_AMBER_REPORT_DATE);
			}
			if (report_type.equals(AmberDrivingReportType.YEAR.toString())) {
				query.put(Constants.FIELD_AMBER_REPORT_TYPE, AmberDrivingReportType.YEAR.toString());
				query.remove(Constants.FIELD_AMBER_REPORT_DATE);
				query.remove(Constants.FIELD_AMBER_REPORT_MONTH);
			}
			BasicDBObject document = new BasicDBObject();
			document.append(Constants.FIELD_FLEET_REPORT_FLEET_SIZE, modelByFleetId.getFleet_size());
			document.append(Constants.FIELD_TOTAL_DEVICE_COUNT, total_device_count);
			document.append(Constants.FIELD_UNITS_NOT_UPDATE_COUNT, units_not_update_count);
			LOGGER.info("query=" + query);
			fleetReportLogic.update(document, query);
		}
	}

	private Map<String, Float> getCountMap() {
		Map<String, Float> count_map = new HashMap<String, Float>();
		count_map.put(Constants.FIELD_AMBER_REPORT_OVER_SPEED_COUNT, 0.0f);
		count_map.put(Constants.FIELD_AMBER_REPORT_IDEL_COUNT, 0.0f);
		count_map.put(Constants.FIELD_AMBER_REPORT_HARSH_BREAK_COUNT, 0.0f);
		count_map.put(Constants.FIELD_AMBER_REPORT_AFTER_HOUR_COUNT, 0.0f);
		count_map.put(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_DAY_COUNT, 0.0f);
		count_map.put(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_NIGHT_COUNT, 0.0f);
		count_map.put(Constants.FIELD_AMBER_REPORT_DISTANCE, 0.0f);
		count_map.put(Constants.FIELD_AMBER_REPORT_HARSH_ACCELERATION, 0.0f);
		return count_map;
	}

	private void calculateReport(List<Trips> trips, String amber_auth_token, String fleet_id, Date trip_start_date) {
		Map<String, Float> countMap = getCountMap();
		InsuranceReportParameter parameter = InsuranceReportParameter.OVER_SPEED;
		float idele_time_count = 0.0f;
		float sb_event = 0.0f;
		float sa_event = 0.0f;
		float fDay = 0.0f;
		float fnight = 0.0f;
		float trip_count = 0;
		float trip_run_time = 0.0f;
		float idel_time = 0.0f;
		float total_trip_idle_time = 0f;
		float trip_count_min_distance = 0.0f;
		float max_speed = 0;
		float overspeed_duration = 0;
		float overspeed_distance = 0;
		for (Trips trip : trips) {
			if (isValidTrip(trip)) {
				Map<String, Float> driver_report_count_map = getCountMap();
				trip_count++;
				driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_TRIP_COUNT, 1.0f);
				// only hr, min, sec
				if (trip_start_date == null && trip.getStartTime() != null) {
					trip_start_date = UtilFunction.getStartEndDayOfDate(trip.getStartTime(), true);
				}
				switch (parameter) {
				case OVER_SPEED:
					TelematicsEventsCalculator eventCalculator = new OverSpeedEventCalculator(trip, telematicsSetting);
					Map<String, Object> speed_info_details = eventCalculator.executeEventCalculation();
					Float calculateSpeedCount = (Float) speed_info_details.get(Constants.SPEEDING_EVENT);
					calculateSpeedCount = calculateSpeedCount
							+ countMap.get(Constants.FIELD_AMBER_REPORT_OVER_SPEED_COUNT);
					countMap.put(Constants.FIELD_AMBER_REPORT_OVER_SPEED_COUNT, calculateSpeedCount);
					float top_speed = (Float) speed_info_details.get(Constants.MAX_SPEED);
					if (max_speed < top_speed) {
						max_speed = top_speed;
					}
					overspeed_duration += (Float) speed_info_details.get(Constants.TIME_ON_OVER_SPEED);
					overspeed_distance += (Float) speed_info_details.get(Constants.DISTANCE_WITH_OVER_SPEED);
					driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, 0.0f + max_speed);
					driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_OVER_SPEED_DURATION,
							(((Float) speed_info_details.get(Constants.TIME_ON_OVER_SPEED)) / 1000) / 60);
					driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_OVER_SPEED_DISTANCE,
							(float) speed_info_details.get(Constants.DISTANCE_WITH_OVER_SPEED));
					driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_OVER_SPEED_COUNT,
							(Float) speed_info_details.get(Constants.SPEEDING_EVENT));
				case IDEL:
					eventCalculator = new IdleTimeEventCalculator(trip, telematicsSetting);
					Map<String, Object> result = eventCalculator.executeEventCalculation();
					if (result != null && !result.isEmpty()) {
						idele_time_count = idele_time_count + (int) result.get(Constants.EVENT_COUNT);
						driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_IDEL_COUNT, idele_time_count);

						float minutes = 0f;
						if (result.get(Constants.EVENT_COUNT) != null
								&& ((int) result.get(Constants.EVENT_COUNT) > 0)) {
							minutes = (float) result.get(Constants.FIELD_AMBER_REPORT_IDLE_TIME);
							// minutes = minutes / 60000;
							minutes = (minutes / 1000) / 60;
						}
						idel_time = idel_time + minutes;
						driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_IDLE_TIME, idel_time);
					}
				case HARSH_BREAKING:
					try {
						TelematicsEventsCalculator hb_eventCalculator = new HarshBreakEventCalculator(trip,
								telematicsSetting);
						result = hb_eventCalculator.executeEventCalculation();
						if (result.get(Constants.EVENT_COUNT) != null
								&& ((int) result.get(Constants.EVENT_COUNT)) > 0) {
							sb_event = sb_event + (int) result.get(Constants.EVENT_COUNT);
							driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_HARSH_BREAK_COUNT,
									0f + (int) result.get(Constants.EVENT_COUNT));
						}
					} catch (Exception e) {
						LOGGER.error("String to number parse exception;input=" + trip.getSB());
					}
				case HARSH_ACCELERATION:
					try {
						TelematicsEventsCalculator ha_eventCalculator = new HarshAccelarationEventCalculator(trip,
								telematicsSetting);
						result = ha_eventCalculator.executeEventCalculation();
						if (result.get(Constants.EVENT_COUNT) != null
								&& ((int) result.get(Constants.EVENT_COUNT)) > 0) {
							sa_event = sa_event + (int) result.get(Constants.EVENT_COUNT);
							driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_HARSH_ACCELERATION,
									0f + (int) result.get(Constants.EVENT_COUNT));
						}
					} catch (Exception e) {
						LOGGER.error("String to number parse exception;input=" + trip.getHA());
					}
				case AFTER_HOURS:
					OutOfHoursEvents outOfHoursEvent = telematicsSetting.getOutOfHoursEvent();
					if (outOfHoursEvent != null && isAfterHourEvent(trip)) {
						float after_hr_event_count = countMap.get(Constants.FIELD_AMBER_REPORT_AFTER_HOUR_COUNT) + 1;
						countMap.put(Constants.FIELD_AMBER_REPORT_AFTER_HOUR_COUNT, after_hr_event_count);
						driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_AFTER_HOUR_COUNT, 1.0f);
					}
				case FATIGUE_DAY_DRIVE:
					FatigueDayEventCalculator dayEventCalculator = new FatigueDayEventCalculator(trip,
							telematicsSetting);
					result = dayEventCalculator.executeEventCalculation();
					if (result.get(Constants.EVENT_COUNT) != null && ((float) result.get(Constants.EVENT_COUNT)) > 0) {
						fDay = fDay + (float) result.get(Constants.EVENT_COUNT);
						countMap.put(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_DAY_COUNT, fDay);
						driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_DAY_COUNT, 1.0f);
					}
				case FATIGUE_NIGHT_DRIVE:
					FatigueNightEventCalculator nightEventCalculator = new FatigueNightEventCalculator(trip,
							telematicsSetting);
					result = nightEventCalculator.executeEventCalculation();
					if (result.get(Constants.EVENT_COUNT) != null && ((float) result.get(Constants.EVENT_COUNT)) > 0) {
						fnight = fnight + (float) result.get(Constants.EVENT_COUNT);
						countMap.put(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_NIGHT_COUNT, fnight);
						driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_NIGHT_COUNT, 1.0f);
					}
				case DISTANCE:
					if (trip.getDistance() != null) {
						float distance_in_km = trip.getDistance();
						countMap.put(Constants.FIELD_AMBER_REPORT_DISTANCE,
								countMap.get(Constants.FIELD_AMBER_REPORT_DISTANCE) + distance_in_km);
						driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_DISTANCE, (distance_in_km / 1000));
					}
				case DRIVE_TIME:
					float minutes = 0f;
					if (trip.getRunTime() != null) {
						// float minutes = TimeUnit.MILLISECONDS.toSeconds(trip.getRunTime())/60;
						minutes = trip.getRunTime() / 60000;
						trip_run_time = trip_run_time + minutes;
						driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_DRIVE_TIME, 0.0f + minutes);
					}
				case IDLE_TIME:
					if (trip.getIdleTime() != null) {
						Float idleTime = 0f;
						try {
							idleTime = trip.getIdleTime();
						} catch (Exception e) {
							LOGGER.error("Failed to get idel time" + e);
						}
						total_trip_idle_time = total_trip_idle_time + idleTime;
					}
				case TRIPS:
					if (telematicsSetting.getTripsMinDistance() != null && trip.getDistance() != null
							&& (trip.getDistance() / 1000) > telematicsSetting.getTripsMinDistance()) {
						trip_count_min_distance++;
						driver_report_count_map.put(Constants.FIELD_AMBER_REPORT_TRIPS, 1.0f);
					} else {
						LOGGER.info("Trip min distance=" + telematicsSetting.getTripsMinDistance() + ";"
								+ "Trip distance=" + trip.getDistance());
					}
				}
				if (trip.getDriverId() != null) {
					dailyDriverReportLogic.processData(trip.getAmberAuthToken(), trip.getDriverId(),
							driver_report_count_map, telematicsSetting.getTelematicsId(), fleet_id, trip_start_date,
							telematicsSetting.getGroupId(), telematicsSetting.getSubGroupId(), created_date,
							telematicsSetting.getSettingType(), telematicsSetting.getSettingsId());
				} else {
					LOGGER.warn("Since driver id not exist, driver report failed to calculate; trip id="
							+ trip.getTripId());
				}
			}
		}
		countMap.put(Constants.FIELD_AMBER_REPORT_DISTANCE, countMap.get(Constants.FIELD_AMBER_REPORT_DISTANCE) / 1000);
		countMap.put(Constants.FIELD_AMBER_REPORT_IDEL_COUNT, idele_time_count);
		countMap.put(Constants.FIELD_AMBER_REPORT_HARSH_BREAK_COUNT, sb_event);
		countMap.put(Constants.FIELD_AMBER_REPORT_HARSH_ACCELERATION, sa_event);
		countMap.put(Constants.FIELD_AMBER_REPORT_TRIP_COUNT, trip_count);
		countMap.put(Constants.FIELD_AMBER_REPORT_DRIVE_TIME, trip_run_time);
		if (idel_time > trip_run_time) {
			countMap.put(Constants.FIELD_AMBER_REPORT_IDLE_TIME, idel_time);
		} else {
			countMap.put(Constants.FIELD_AMBER_REPORT_IDLE_TIME, (total_trip_idle_time/60000));
		}
		countMap.put(Constants.FIELD_AMBER_REPORT_TRIPS, trip_count_min_distance);
		countMap.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, 0.0f + max_speed);
		overspeed_duration = (overspeed_duration / 1000) / 60;
		countMap.put(Constants.FIELD_AMBER_REPORT_OVER_SPEED_DURATION, overspeed_duration);
		countMap.put(Constants.FIELD_AMBER_REPORT_OVER_SPEED_DISTANCE, overspeed_distance);
		LOGGER.info("countMap=" + countMap);
		saveCountDetails(countMap, amber_auth_token, fleet_id, trip_start_date, telematicsSetting);
	}

	public boolean isValidTrip(Trips trip) {
		return (trip.getDistance() > 100 && (trip.getRunTime() / (1000 * 60)) > 1);
		// return true;
	}

	private void saveCountDetails(Map<String, Float> countMap, String amber_auth_token, String fleet_id,
			Date trip_start_date, TelematicsSettings telematicsSettings) {
		TelematicsDailyDrivingReport report = new TelematicsDailyDrivingReport();
		report.setAmberAuthToken(amber_auth_token);
		report.setFleet_id(fleet_id);
		report.setCreated_date(created_date);
		LOGGER.info("created date=" + created_date);
		report.setOver_speed_count(countMap.get(Constants.FIELD_AMBER_REPORT_OVER_SPEED_COUNT));
		report.setIdel_event_count(countMap.get(Constants.FIELD_AMBER_REPORT_IDEL_COUNT));
		report.setHarsh_break_event_count(countMap.get(Constants.FIELD_AMBER_REPORT_HARSH_BREAK_COUNT));
		report.setAfter_hour_event_count(countMap.get(Constants.FIELD_AMBER_REPORT_AFTER_HOUR_COUNT));
		report.setFatigue_driving_day_event(countMap.get(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_DAY_COUNT));
		report.setFatigue_driving_night_event(countMap.get(Constants.FIELD_AMBER_REPORT_FATIGUE_DRIVE_NIGHT_COUNT));
		report.setDistance(countMap.get(Constants.FIELD_AMBER_REPORT_DISTANCE));
		report.setAcceleration(countMap.get(Constants.FIELD_AMBER_REPORT_HARSH_ACCELERATION));
		report.setTrip_date(trip_start_date);
		report.setTelematics_id(telematicsSettings.getTelematicsId());
		report.setTotal_trip_count(countMap.get(Constants.FIELD_AMBER_REPORT_TRIP_COUNT));
		report.setMin_distance_trip_count(countMap.get(Constants.FIELD_AMBER_REPORT_TRIPS));
		report.setIdel_time(countMap.get(Constants.FIELD_AMBER_REPORT_IDLE_TIME));
		report.setDrive_time(countMap.get(Constants.FIELD_AMBER_REPORT_DRIVE_TIME));
		report.setDelerte_flag(Constants.DELETE_FLAG_N);
		report.setMax_speed(countMap.get(Constants.FIELD_AMBER_REPORT_MAX_SPEED));
		report.setOverspeed_duration(countMap.get(Constants.FIELD_AMBER_REPORT_OVER_SPEED_DURATION));
		report.setOverspeed_distance(countMap.get(Constants.FIELD_AMBER_REPORT_OVER_SPEED_DISTANCE));
		report.setGroupId(telematicsSettings.getGroupId());
		report.setSubGroupId(telematicsSettings.getSubGroupId());
		report.setSetting_id(telematicsSettings.getSettingsId());
		report.setSettingType(telematicsSettings.getSettingType());
		reportLogic.processAndSave(report, telematicsSettings, fleetReportLogic);
	}

	private Boolean isFatigueNightDrive(Trips trip, String distance_str, String drive_time_str) {
		boolean is_fatigue = false;
		int distance = Integer.parseInt(distance_str);
		int drive_time = Integer.parseInt(drive_time_str);
		if (trip.getDistance() != null && (trip.getDistance() / 1000) >= distance) {
			is_fatigue = true;
		}
		if (trip.getRunTime() != null) {
			int run_time_in_minutes = (int) ((trip.getRunTime() / (1000 * 60)) % 60);
			// TODO: replace 129 with param value
			if (run_time_in_minutes >= (drive_time / 60)) {
				is_fatigue = true;
			}
		}
		return is_fatigue;
	}

	private Boolean isFatigueDayDrive(Trips trip, String distance_str, String drive_time_str) {
		boolean is_fatigue = false;
		int distance = Integer.parseInt(distance_str);
		int drive_time = Integer.parseInt(drive_time_str);
		if (!StringUtils.isEmpty(distance_str) && !StringUtils.isEmpty(drive_time_str)) {
			if (trip.getDistance() != null && (trip.getDistance() / 1000) >= distance) {
				is_fatigue = true;
			}
			if (trip.getRunTime() != null) {
				int run_time_in_minutes = (int) ((trip.getRunTime() / (1000 * 60)) % 60);
				// TODO: replace 240 with param value
				if (run_time_in_minutes >= (drive_time / 60)) {
					is_fatigue = true;
				}
			}
		}
		return is_fatigue;
	}

	private Boolean isAfterHourEvent(Trips trip) {
		OutOfHoursEvents outOfHoursEvent = this.telematicsSetting.getOutOfHoursEvent();
		if (outOfHoursEvent != null) {
			return UtilFunction.isBetweenTwoHrS(outOfHoursEvent.getStartTimeUTC(), outOfHoursEvent.getEndTimeUTC(),
					UtilFunction.getHrMinSec(trip.getStartTime()));
		}
		// Date startTime = trip.getStartTime();
		// Date endTime = trip.getEndTime();
		// //OutOfHoursEvents outOfHoursEvent =
		// this.telematicsSetting.getOutOfHoursEvent();
		// String start_time_cond_str[] = outOfHoursEvent.getStartTimeIP().split(":");
		// String end_time_cond_str[] = outOfHoursEvent.getEndTimeIP().split(":");
		// long start_time_cond_long =
		// UtilFunction.getMsFromTime(start_time_cond_str[0], start_time_cond_str[1],
		// start_time_cond_str[2]);
		// long end_time_cond_long = UtilFunction.getMsFromTime(end_time_cond_str[0],
		// end_time_cond_str[1], end_time_cond_str[2]);
		// if (startTime != null) {
		// long from_trip_cond_long = UtilFunction.getMsFromTime_(startTime);
		// if (from_trip_cond_long >= start_time_cond_long || from_trip_cond_long <=
		// end_time_cond_long) {
		// return true;
		// }
		// }
		// if (endTime != null) {
		// long end_trip_cond_long = UtilFunction.getMsFromTime_(endTime);
		// if (end_trip_cond_long >= start_time_cond_long || end_trip_cond_long <=
		// end_time_cond_long) {
		// return true;
		// }
		// }
		return false;
	}

	private Set<String> getAllDaysOfTripsByAmber(String amber_auth_token) {
		Set<String> days = new TreeSet<String>();
		BasicDBObject allQuery = new BasicDBObject();
		allQuery.put(Constants.FIELD_AMBER_AUTH_TOKEN, amber_auth_token);
		BasicDBObject fields = new BasicDBObject();
		fields.put(Constants.FIELD_START_TIME, 1);
		List<DBObject> allRecordsDBObject = tripsLogic.getAllRecordsDBObject(allQuery, fields, new BasicDBObject());
		LOGGER.info("Amber token=" + amber_auth_token + "; size=" + allRecordsDBObject.size());
		for (DBObject dbObject : allRecordsDBObject) {
			if (dbObject.get(Constants.FIELD_START_TIME) != null) {
				Date date = (Date) dbObject.get(Constants.FIELD_START_TIME);
				date = new Date(date.getTime() - UtilFunction.getTimeInMSFromTimeZone(telematicsSetting.getTimeZone()));
				days.add(UtilFunction.getDateMonthYear(date));
			}
		}
		return days;
	}

	public static void main(String a[]) {
		// TelematicsSettingsLogic logic2 = new TelematicsSettingsLogic();
		// BasicDBObject startDateQuer1y = new BasicDBObject();
		// startDateQuer1y.put("TelematicsId", "HHHOLLA1");
		// TripsSearchLogic logic1 = new TripsSearchLogic();
		// logic1.setTelematicsSetting(logic2.getModelByQuery(startDateQuer1y));
		// // logic.getAllAmberAuthTokne(true);
		// // System.out.println(((1000000 / (1000 * 60)) % 60));
		// // System.out.println(14400/60);
		// // System.out.println(TimeUnit.MILLISECONDS.toMinutes(300000));
		// // System.out.println(new Date());
		// String amber_auth_token = "1PUV5S7RMTQK";
		// TripsLogic logic = new TripsLogic();
		// Date startDate =
		// UtilFunction.getStartEndDayOfDate(UtilFunction.getDateFromString("01/01/2018"),
		// true);
		// Date endDate =
		// UtilFunction.getStartEndDayOfDate(UtilFunction.getDateFromString("31/01/2018"),
		// false);
		// BasicDBObject startDateQuery = new BasicDBObject();
		// startDateQuery.append(Constants.FIELD_START_TIME, new BasicDBObject("$gte",
		// startDate));
		// BasicDBObject endDateQuery = new BasicDBObject();
		// endDateQuery.append(Constants.FIELD_START_TIME, new BasicDBObject("$lte",
		// endDate));
		// BasicDBList andValues = new BasicDBList();
		// andValues.add(startDateQuery);
		// andValues.add(endDateQuery);
		// andValues.add(new BasicDBObject(Constants.FIELD_AMBER_AUTH_TOKEN,
		// amber_auth_token));
		// andValues.add(new BasicDBObject(Constants.FIELD_TRIP_COMPLETED, "Y"));
		// andValues.add(new BasicDBObject(Constants.FIELD_TRIP_DELETE_FLAG, "N"));
		// BasicDBObject query = new BasicDBObject("$and", andValues);
		// List<Trips> allRecords = logic.getAllRecords(query);
		// Map<String, Float> data = new HashMap<String, Float>();
		// data.put(Constants.SPEEDING_EVENT, 0f);
		// data.put(Constants.TIME_ON_OVER_SPEED, 0.0f);
		// data.put(Constants.DISTANCE_WITH_OVER_SPEED, 0.0f);
		// data.put(Constants.MAX_SPEED, 0.0f);
		// for (Trips trips : allRecords) {
		// Map<String, Float> calculateSpeedCount = logic1.calculateSpeedCount(trips);
		// data.put(Constants.SPEEDING_EVENT,
		// calculateSpeedCount.get(Constants.SPEEDING_EVENT) +
		// data.get(Constants.SPEEDING_EVENT));
		// data.put(Constants.TIME_ON_OVER_SPEED,
		// calculateSpeedCount.get(Constants.TIME_ON_OVER_SPEED) +
		// data.get(Constants.TIME_ON_OVER_SPEED));
		// data.put(Constants.DISTANCE_WITH_OVER_SPEED,
		// calculateSpeedCount.get(Constants.DISTANCE_WITH_OVER_SPEED) +
		// data.get(Constants.DISTANCE_WITH_OVER_SPEED));
		// if (calculateSpeedCount.get(Constants.MAX_SPEED) >
		// data.get(Constants.MAX_SPEED)) {
		// data.put(Constants.MAX_SPEED, calculateSpeedCount.get(Constants.MAX_SPEED));
		// }
		// }

	}
}
