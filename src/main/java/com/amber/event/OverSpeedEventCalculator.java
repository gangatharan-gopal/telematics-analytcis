package com.amber.event;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.amber.logic.DevicesLogic;
import com.amber.logic.DriverLogic;
import com.amber.logic.TripsLogic;
import com.amber.model.Devices;
import com.amber.model.EndPointCoordinates;
import com.amber.model.GPSPoints;
import com.amber.model.StartPointCoordinates;
import com.amber.model.TelematicsEventReport;
import com.amber.model.TelematicsSettings;
import com.amber.model.Trips;
import com.amber.util.Constants;
import com.model.subdoc.TelematicsSettings.SpeedingEvents;
import com.mongodb.BasicDBObject;

public class OverSpeedEventCalculator extends TelematicsEventsCalculator {
	private static final Logger LOGGER = Logger.getLogger(OverSpeedEventCalculator.class.getName());
	// public static final String FIELD_OVER_SPEED="OVERSPEED";
	// public static final String FIELD_OVER_SPEED_GPSPOINT="OVERSPEED_GPSPOINTS";
	public StartPointCoordinates startPointCoordinates;
	public EndPointCoordinates endPointCoordinates;
	public List<GPSPoints> over_speed_gps_points = new ArrayList<GPSPoints>();
	private Date start_time;
	private String event_id;
	private Date end_time;
	// for within trip gps points sets
	private Integer total_timeDiff = 0;
	private Float total_distanceDiff = 0f;
	private Integer max_speed = 0;
	// for report
	private Integer telematicsreport_total_timeDiff = 0;
	private Float telematicsreport_total_distanceDiff = 0f;
	private Integer telematicsreport_max_speed = 0;

	public OverSpeedEventCalculator(Trips trip, TelematicsSettings telematicsSettings) {
		init(trip, telematicsSettings);
	}

	@Override
	public void setTelematicsSettings() {
		// TODO Auto-generated method stub
	}

	@Override
	public void init(Trips trip, TelematicsSettings telematicsSettings) {
		super.trip = trip;
		super.telematicsSettings = telematicsSettings;
		if (trip.getAmberAuthToken() != null) {
			Devices devices = DevicesLogic.getInstance().getModelByDeviceAuthenticationToken(trip.getAmberAuthToken());
			if (devices.getGroupId() != null) {
				super.group_id = devices.getGroupId();
			}
			if (devices.getSubGroupId() != null) {
				super.sub_group_id = devices.getSubGroupId();
			}
		}
		super.initDataMap();
	}

	@Override
	public Map<String, Object> executeEventCalculation() {
		if (telematicsSettings.getSpeedingEvent() != null) {
			return calculateSpeedCount();
		} else {
			return baseCalculateSpeedCount();
		}
	}

	public Map<String, Object> calculateSpeedCount() {
		SpeedingEvents speedingEvents = telematicsSettings.getSpeedingEvent();
		LOGGER.info("Trip ID=" + trip.getTripId() + ";\n amber auth token:" + trip.getAmberAuthToken() + "\n" + "trip date=" + trip.getStartTime()
				+ "\n" + trip.getEndTime());
		LOGGER.info("Speed even limit=" + speedingEvents.getSpeedLimit());
		Float speeding_event = 0.0f;
		int time_on_over_speed = 0;
		float distance_with_over_speed = 0;
		if (speedingEvents != null) {
			int i = 0;
			try {
				for (GPSPoints gpsPoints : trip.getGPSPoints()) {
					int speed = gpsPoints.getSpeed();
					if (speed > max_speed) {
						max_speed = speed;
					}
					if (speed > telematicsreport_max_speed) {
						telematicsreport_max_speed = speed;
					}
					if (speed >= speedingEvents.getSpeedLimit() && i <= trip.getGPSPoints().size() - 1
							&& trip.getGPSPoints().get(i + 1).getSpeed() >= speedingEvents.getSpeedLimit()) {
						if (getStartPointCoordinates() == null) {
							setStartPointCoordinates(gpsPoints);
							start_time = gpsPoints.getTime();
						}
						if (trip.getGPSPoints().get(i + 1).getTimeDiff() != null) {
							time_on_over_speed = time_on_over_speed + trip.getGPSPoints().get(i + 1).getTimeDiff();
						}
						if (trip.getGPSPoints().get(i + 1).getDistanceDiff() != null) {
							distance_with_over_speed = distance_with_over_speed + trip.getGPSPoints().get(i + 1).getDistanceDiff();
						}
						over_speed_gps_points.add(trip.getGPSPoints().get(i));
					} else {
						if (time_on_over_speed > 0 && (time_on_over_speed / 1000) > speedingEvents.getSpeedingTime()) {
							speeding_event++;
							over_speed_gps_points.add(trip.getGPSPoints().get(i));
							total_timeDiff = time_on_over_speed;
							total_distanceDiff = distance_with_over_speed;
							telematicsreport_total_timeDiff = telematicsreport_total_timeDiff + total_timeDiff;
							telematicsreport_total_distanceDiff = telematicsreport_total_distanceDiff + total_distanceDiff;
							end_time = trip.getGPSPoints().get(i).getTime();
							setEndPointCoordinates(trip.getGPSPoints().get(i));
							setTelematicsEventData();
							TelematicsEventReport createTelemeticsEventReport = createTelemeticsEventReport(Constants.EVENTREPORT_OVER_SPEED,
									event_data_map);
							//System.out.println("speeding event=" + speeding_event);
							endPointCoordinates = null;
							if (createTelemeticsEventReport != null) {
								createTelemeticsEventReportGPSPoints(createTelemeticsEventReport);
							}
							over_speed_gps_points = new ArrayList<GPSPoints>();
							time_on_over_speed = 0;
							distance_with_over_speed = 0.0f;
							startPointCoordinates = null;
							max_speed = 0;
						}
					}
					i++;
				}
			} catch (Exception exception) {
				LOGGER.error("speeding_event=" + exception, exception);
			}
		}
		LOGGER.info("speeding_event=" + speeding_event);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put(Constants.SPEEDING_EVENT, speeding_event);
		data.put(Constants.TIME_ON_OVER_SPEED, 0.0f + telematicsreport_total_timeDiff);
		data.put(Constants.DISTANCE_WITH_OVER_SPEED, 0.0f + telematicsreport_total_distanceDiff);
		data.put(Constants.MAX_SPEED, 0.0f + telematicsreport_max_speed);
		return data;
	}

	public StartPointCoordinates getStartPointCoordinates() {
		return startPointCoordinates;
	}

	public void setStartPointCoordinates(GPSPoints gpsPoints) {
		if (gpsPoints != null) {
			this.startPointCoordinates = new StartPointCoordinates();
			startPointCoordinates.setLatitude(gpsPoints.getLatitude());
			startPointCoordinates.setLongitude(gpsPoints.getLongitude());
		}
	}

	public EndPointCoordinates getEndPointCoordinates() {
		return endPointCoordinates;
	}

	public void setEndPointCoordinates(GPSPoints gpsPoints) {
		if (gpsPoints != null) {
			this.endPointCoordinates = new EndPointCoordinates();
			endPointCoordinates.setLatitude(gpsPoints.getLatitude());
			endPointCoordinates.setLongitude(gpsPoints.getLongitude());
		}
	}

	@Override
	public void setTelematicsEventData() {
		event_data_map.put(Constants.FIELD_EVENT_REPORT_START_TIME, start_time);
		event_data_map.put(Constants.FIELD_EVENT_REPORT_END_TIME, end_time);
		event_data_map.put(Constants.FIELD_DURATION, total_timeDiff);
		event_data_map.put(Constants.FIELD_EVENT_REPORT_DISTANCE, total_distanceDiff);
		event_data_map.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, max_speed);
		event_data_map.put(Constants.FIELD_EVENT_ID, event_id);
		event_data_map.put(Constants.FIELD_START_POINT_COORDINATES, startPointCoordinates);
		event_data_map.put(Constants.FIELD_END_POINT_COORDINATES, endPointCoordinates);
	}

	public void createTelemeticsEventReportGPSPoints(TelematicsEventReport overspeed_EventReport) {
		for (GPSPoints gpsPoints : this.over_speed_gps_points) {
			event_data_map = new HashMap<String, Object>();
			start_time = gpsPoints.getGPSInsertedTime();
			total_timeDiff = gpsPoints.getTimeDiff();
			if (gpsPoints.getGPSInsertedTime() != null && total_timeDiff != null) {
				end_time = new Date(gpsPoints.getGPSInsertedTime().getTime() + total_timeDiff);
			}
			total_distanceDiff = gpsPoints.getDistanceDiff();
			max_speed = gpsPoints.getSpeed();
			event_id = overspeed_EventReport.get_id();
			setStartPointCoordinates(gpsPoints);
			setEndPointCoordinates(null);
			setTelematicsEventData();
			createTelemeticsEventReport(Constants.EVENTREPORT_OVER_SPEED_GPSPOINT, event_data_map);
		}
	}

	public static void main(String a[]) {
		// "_id" : "TEST_EVENTREPORT20150921113637",
		// "AmberAuthToken" : "TEST_EVENTREPORT",
		TripsLogic logic = new TripsLogic();
		BasicDBObject allQuery = new BasicDBObject();
		allQuery.append(Constants.FIELD_AMBER_AUTH_TOKEN, "TEST_EVENTREPORT");
		allQuery.append(Constants.FIELD_TRIP_ID, "TEST_EVENTREPORT20180426045434");
		Trips modelByQuery = logic.getModelByQuery(allQuery);
		TelematicsSettings settings = new TelematicsSettings();
		SpeedingEvents speedingEvents = new SpeedingEvents();
		speedingEvents.setCondition("gte");
		speedingEvents.setSpeedLimit(25);
		speedingEvents.setSpeedingTime(10);
		settings.setSpeedingEvent(speedingEvents);
		OverSpeedEventCalculator calculator = new OverSpeedEventCalculator(modelByQuery, settings);
		calculator.calculateSpeedCount();
	}
}