package com.amber.event;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import org.apache.log4j.Logger;

import com.amber.logic.DriverLogic;
import com.amber.logic.TelematicsEventReportLogic;
import com.amber.logic.TripsLogic;
import com.amber.model.Alerts;
import com.amber.model.EndPointCoordinates;
import com.amber.model.GPSPoints;
import com.amber.model.StartPointCoordinates;
import com.amber.model.TelematicsEventReport;
import com.amber.model.TelematicsSettings;
import com.amber.model.Trips;
import com.amber.util.Constants;
import com.amber.util.UtilFunction;
import com.model.subdoc.TelematicsSettings.IdlingEvents;
import com.model.subdoc.TelematicsSettings.SpeedingEvents;

public abstract class TelematicsEventsCalculator {
	private static final Logger LOGGER = Logger.getLogger(TelematicsEventsCalculator.class.getName());
	public Trips trip;
	public TripsLogic tripsLogic = new TripsLogic();
	public TelematicsSettings telematicsSettings;
	public Map<String, Object> data_map = new HashMap<String, Object>();
	public Map<String, Object> event_data_map = new HashMap<String, Object>();
	public TelematicsEventReportLogic telematicsEventReportLogic = new TelematicsEventReportLogic();
	public Date report_date;
	public String driver_name;
	public String group_id;
	public String sub_group_id;
	public Boolean is_event_report_generation;

	public Boolean getIs_event_report_generation() {
		return is_event_report_generation;
	}

	public void setIs_event_report_generation(Boolean is_event_report_generation) {
		this.is_event_report_generation = is_event_report_generation;
	}

	public String getDriver_name() {
		return driver_name;
	}

	public void setDriver_name(String driver_name) {
		this.driver_name = driver_name;
	}

	public Trips getTrip() {
		return trip;
	}

	public void setTrip(Trips trip) {
		this.trip = trip;
	}

	public TelematicsSettings getTelematicsSettings() {
		return telematicsSettings;
	}

	public void setTelematicsSettings(TelematicsSettings telematicsSettings) {
		this.telematicsSettings = telematicsSettings;
	}

	public Map<String, Object> getData_map() {
		return data_map;
	}

	public void setData_map(Map<String, Object> data_map) {
		this.data_map = data_map;
	}

	public abstract void init(Trips trip, TelematicsSettings telematicsSettings);

	public abstract void setTelematicsSettings();

	public abstract Map<String, Object> executeEventCalculation();

	public abstract void setTelematicsEventData();
	//
	// public void setTelematicsEventData() {
	// event_data_map.put(Constants.FIELD_EVENT_REPORT_START_TIME, start_time);
	// event_data_map.put(Constants.FIELD_EVENT_REPORT_END_TIME, end_time);
	// event_data_map.put(Constants.FIELD_DURATION, total_timeDiff);
	// event_data_map.put(Constants.FIELD_EVENT_REPORT_DISTANCE,
	// total_distanceDiff);
	// event_data_map.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, max_speed);
	// event_data_map.put(Constants.FIELD_EVENT_ID, event_id);
	// event_data_map.put(Constants.FIELD_START_POINT_COORDINATES,
	// startPointCoordinates);
	// event_data_map.put(Constants.FIELD_END_POINT_COORDINATES,
	// endPointCoordinates);
	// }

	public void initDataMap() {
		report_date = trip.getStartTime();
		String dateMonthYear[] = UtilFunction.getDateMonthYear(report_date).split("_");
		report_date = UtilFunction.getMongoDate(dateMonthYear[0], dateMonthYear[1], dateMonthYear[2], "00", "00", "00");
		data_map.put(Constants.EVENT_COUNT, 0);
		if (trip.getDriverId() != null) {
			setDriver_name(DriverLogic.getInstance().getNameByDriverId(trip.getDriverId()));
		}
	}

	public Map<String, Object> baseCalculateHBCount() {
		if (trip != null && trip.getSB() != null && !trip.getSB().isEmpty()) {
			data_map.put(Constants.EVENT_COUNT, Integer.parseInt(trip.getSB()));
		}
		Trips tripByTripId = tripsLogic.getTripByTripId(trip.getTripId());
		for (Alerts alert : tripByTripId.getAlerts()) {
			if (alert.getAlertType().equals(Constants.ALERT_TYPE_HB)) {
				event_data_map.put(Constants.FIELD_EVENT_REPORT_START_TIME, alert.getTime());
				event_data_map.put(Constants.FIELD_EVENT_REPORT_END_TIME, alert.getTime());
				event_data_map.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, Integer.parseInt(alert.getSpeed()));
				event_data_map.put(Constants.FIELD_EVENT_ID, null);
				List<GPSPoints> gpsPoints = tripByTripId.getGPSPoints();
				int total_timeDiff = 0;
				float total_distanceDiff = 0f;
				int i = 1;
				try {
					for (GPSPoints points : gpsPoints) {
						// if (points.getTime().getTime() == alert.getTime().getTime() && i <
						// gpsPoints.size() && i < gpsPoints.size()) {
						if (points.getGPSInsertedTime().getTime() == alert.getGPSInsertedTime().getTime() && i < gpsPoints.size()
								&& i < gpsPoints.size()) {
							StartPointCoordinates startPointCoordinates = new StartPointCoordinates();
							EndPointCoordinates endPointCoordinates = new EndPointCoordinates();
							GPSPoints sPoints = gpsPoints.get(i - 1);
							GPSPoints ePoints = gpsPoints.get(i);
							startPointCoordinates.setLatitude(sPoints.getLatitude());
							startPointCoordinates.setLongitude(sPoints.getLongitude());
							endPointCoordinates.setLatitude(ePoints.getLatitude());
							endPointCoordinates.setLongitude(ePoints.getLongitude());
							total_distanceDiff = ePoints.getDistanceDiff();
							total_timeDiff = ePoints.getTimeDiff();
							event_data_map.put(Constants.FIELD_DURATION, total_timeDiff);
							event_data_map.put(Constants.FIELD_EVENT_REPORT_DISTANCE, total_distanceDiff);
							event_data_map.put(Constants.FIELD_START_POINT_COORDINATES, startPointCoordinates);
							event_data_map.put(Constants.FIELD_END_POINT_COORDINATES, endPointCoordinates);
							createTelemeticsEventReport(Constants.EVENTREPORT_HARSHBREAK, event_data_map);
						}
						i++;
					}
				} catch (Exception e) {
					LOGGER.error("Exception occured; ", e);
				}
			}
		}
		return data_map;
	}

	public Map<String, Object> baseCalculateHACount() {
		if (trip != null && trip.getHA() != null && !trip.getHA().isEmpty()) {
			data_map.put(Constants.EVENT_COUNT, Integer.parseInt(trip.getHA()));
		}
		Trips tripByTripId = tripsLogic.getTripByTripId(trip.getTripId());
		try {
			for (Alerts alert : tripByTripId.getAlerts()) {
				if (alert.getAlertType().equals(Constants.ALERT_TYPE_HA)) {
					event_data_map.put(Constants.FIELD_EVENT_REPORT_START_TIME, alert.getTime());
					event_data_map.put(Constants.FIELD_EVENT_REPORT_END_TIME, alert.getTime());
					event_data_map.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, Integer.parseInt(alert.getSpeed()));
					event_data_map.put(Constants.FIELD_EVENT_ID, null);
					List<GPSPoints> gpsPoints = tripByTripId.getGPSPoints();
					int total_timeDiff = 0;
					float total_distanceDiff = 0f;
					int i = 1;
					for (GPSPoints points : gpsPoints) {
						// if (points.getTime().getTime() == alert.getTime().getTime() && i <
						// gpsPoints.size()) {
						if (points.getGPSInsertedTime().getTime() == alert.getGPSInsertedTime().getTime() && i < gpsPoints.size()) {
							StartPointCoordinates startPointCoordinates = new StartPointCoordinates();
							EndPointCoordinates endPointCoordinates = new EndPointCoordinates();
							GPSPoints sPoints = gpsPoints.get(i - 1);
							GPSPoints ePoints = gpsPoints.get(i);
							startPointCoordinates.setLatitude(sPoints.getLatitude());
							startPointCoordinates.setLongitude(sPoints.getLongitude());
							endPointCoordinates.setLatitude(ePoints.getLatitude());
							endPointCoordinates.setLongitude(ePoints.getLongitude());
							total_distanceDiff = ePoints.getDistanceDiff();
							total_timeDiff = ePoints.getTimeDiff();
							event_data_map.put(Constants.FIELD_DURATION, total_timeDiff);
							event_data_map.put(Constants.FIELD_EVENT_REPORT_DISTANCE, total_distanceDiff);
							event_data_map.put(Constants.FIELD_START_POINT_COORDINATES, startPointCoordinates);
							event_data_map.put(Constants.FIELD_END_POINT_COORDINATES, endPointCoordinates);
							createTelemeticsEventReport(Constants.EVENTREPORT_SUDDENACCELARATION, event_data_map);
						}
						i++;
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured; ", e);
		}
		return data_map;
	}

	public Map<String, Object> baseCalculateIdelTimeCount() {
		Map<String, Object> data = new HashMap<String, Object>();
		List<GPSPoints> gpsPoints = trip.getGPSPoints();
		int index = 0;
		long idleTimeCalculation = 0;
		List<GPSPoints> write_points = new ArrayList<GPSPoints>();
		int event_count = 0;
		float total_idel_time = 0;
		for (GPSPoints points : gpsPoints) {
			if (index > 1) {
				long idle_time_diff = gpsPoints.get(index).getTime().getTime() - gpsPoints.get(index - 1).getTime().getTime();
				int pre_speed = 0;
				pre_speed = gpsPoints.get(index - 1).getSpeed();
				long idleTimeDiffBuffer = 0;
				// To avoid buffered packet idle time
				idleTimeDiffBuffer = gpsPoints.get(index - 1).getTime().getTime() - gpsPoints.get(index - 2).getTime().getTime();
				if (pre_speed == 0 && idleTimeDiffBuffer >= 0) {
					// System.out.println("index=" + index + ";pre_speed= " + pre_speed);
					idleTimeCalculation += idle_time_diff;
					// total_idel_time += idleTimeCalculation;
					write_points.add(gpsPoints.get(index));
				} else if (pre_speed < 20 && ((idle_time_diff / 1000) > 50) && idleTimeDiffBuffer >= 0) {
					idleTimeCalculation += idle_time_diff;
					// total_idel_time += idleTimeCalculation;
					write_points.add(gpsPoints.get(index));
				} else {
					if (write_points.size() > 0) {
						// System.out.println("----");
						event_count++;
						updateIdelTimeGPSPoints(gpsPoints.get(index - 1), write_points, ((int) idleTimeCalculation));
						total_idel_time = total_idel_time + idleTimeCalculation;
						write_points = new ArrayList<GPSPoints>();
						idleTimeCalculation = 0;
					}
				}
			}
			index++;
		}
		data_map.put(Constants.EVENT_COUNT, event_count);
		data_map.put(Constants.FIELD_AMBER_REPORT_IDLE_TIME, total_idel_time);
		return data_map;
	}

	public void updateIdelTimeGPSPoints(GPSPoints start_location, List<GPSPoints> write_points, int total_idel_time) {
		StartPointCoordinates startPointCoordinates = new StartPointCoordinates();
		EndPointCoordinates endPointCoordinates = new EndPointCoordinates();
		if (write_points.size() > 0) {
			GPSPoints gpsPoints = write_points.get(0);
			startPointCoordinates.setLatitude(start_location.getLatitude());
			startPointCoordinates.setLongitude(start_location.getLongitude());
			GPSPoints gpsPoints2 = write_points.get(write_points.size() - 1);
			endPointCoordinates.setLatitude(gpsPoints2.getLatitude());
			endPointCoordinates.setLongitude(gpsPoints2.getLongitude());
			int time_diff = 0;
			Date start_time = null;
			Date end_time = null;
			for (GPSPoints points : write_points) {
				if (start_time == null) {
					start_time = points.getTime();
				}
				end_time = points.getTime();
			}
			event_data_map.put(Constants.FIELD_EVENT_REPORT_START_TIME, start_time);
			event_data_map.put(Constants.FIELD_EVENT_REPORT_END_TIME, end_time);
			event_data_map.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, 0);
			event_data_map.put(Constants.FIELD_EVENT_ID, null);
			event_data_map.put(Constants.FIELD_DURATION, total_idel_time);
			event_data_map.put(Constants.FIELD_EVENT_REPORT_DISTANCE, 0f);
			event_data_map.put(Constants.FIELD_START_POINT_COORDINATES, startPointCoordinates);
			event_data_map.put(Constants.FIELD_END_POINT_COORDINATES, endPointCoordinates);
			TelematicsEventReport createTelemeticsEventReport = createTelemeticsEventReport(Constants.EVENTREPORT_IDELTIME, event_data_map);
			if (createTelemeticsEventReport != null) {
				for (GPSPoints points : write_points) {
					event_data_map = new HashMap<String, Object>();
					event_data_map.put(Constants.FIELD_EVENT_REPORT_START_TIME, points.getTime());
					if (points.getTimeDiff() != null && points.getTime() != null) {
						event_data_map.put(Constants.FIELD_EVENT_REPORT_END_TIME, new Date(points.getTime().getTime() + points.getTimeDiff()));
					}
					event_data_map.put(Constants.FIELD_DURATION, points.getTimeDiff());
					event_data_map.put(Constants.FIELD_EVENT_REPORT_DISTANCE, 0f);
					event_data_map.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, 0);
					event_data_map.put(Constants.FIELD_EVENT_ID, createTelemeticsEventReport.get_id());
					event_data_map.put(Constants.FIELD_START_POINT_COORDINATES, startPointCoordinates);
					StartPointCoordinates coordinates = new StartPointCoordinates();
					coordinates.setLatitude(points.getLatitude());
					coordinates.setLongitude(points.getLongitude());
					event_data_map.put(Constants.FIELD_END_POINT_COORDINATES, null);
					if (points.getTimeDiff() != null)
						event_data_map.put(Constants.FIELD_AMBER_REPORT_IDLE_TIME, (float) points.getTimeDiff());
					createTelemeticsEventReport(Constants.EVENTREPORT_IDELTIME_GPSPOINT, event_data_map);
				}
			}
		}
	}

	public Map<String, Object> baseCalculateIdelTimeCount1() {
		Map<String, Object> data = new HashMap<String, Object>();
		int i = 0;
		int time_on_over_speed = 0;
		float distance_with_over_speed = 0;
		StartPointCoordinates startPointCoordinates = null;
		;
		EndPointCoordinates endPointCoordinates = new EndPointCoordinates();
		List<GPSPoints> over_speed_gps_points = new ArrayList<GPSPoints>();
		Date start_time = null;
		Date end_time;
		for (GPSPoints gpsPoints : trip.getGPSPoints()) {
			if (gpsPoints.getSpeed() == 0 && i > 0 && (i < trip.getGPSPoints().size() - 1) && trip.getGPSPoints().get(i + 1).getSpeed() > 0) {
				startPointCoordinates = new StartPointCoordinates();
				startPointCoordinates.setLatitude(gpsPoints.getLatitude());
				startPointCoordinates.setLongitude(gpsPoints.getLongitude());
				over_speed_gps_points.add(gpsPoints);
				if (trip.getGPSPoints().get(i + 1).getTimeDiff() != null) {
					time_on_over_speed = time_on_over_speed + trip.getGPSPoints().get(i + 1).getTimeDiff();
				}
				if (trip.getGPSPoints().get(i + 1).getDistanceDiff() != null) {
					distance_with_over_speed = 0;
				}
				if (start_time == null)
					start_time = gpsPoints.getTime();
			} else {
				if (i > 0 && i < trip.getGPSPoints().size() - 1) {
					endPointCoordinates.setLatitude(trip.getGPSPoints().get(i + 1).getLatitude());
					endPointCoordinates.setLongitude(trip.getGPSPoints().get(i + 1).getLongitude());
					end_time = trip.getGPSPoints().get(i - 1).getTime();
					event_data_map.put(Constants.FIELD_EVENT_REPORT_START_TIME, start_time);
					event_data_map.put(Constants.FIELD_EVENT_REPORT_END_TIME, end_time);
					event_data_map.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, 0);
					event_data_map.put(Constants.FIELD_EVENT_ID, null);
					event_data_map.put(Constants.FIELD_DURATION, time_on_over_speed);
					event_data_map.put(Constants.FIELD_EVENT_REPORT_DISTANCE, distance_with_over_speed);
					event_data_map.put(Constants.FIELD_START_POINT_COORDINATES, startPointCoordinates);
					event_data_map.put(Constants.FIELD_END_POINT_COORDINATES, endPointCoordinates);
					TelematicsEventReport createTelemeticsEventReport = createTelemeticsEventReport(Constants.EVENTREPORT_IDELTIME, event_data_map);
					endPointCoordinates = null;
					if (createTelemeticsEventReport != null) {
						for (GPSPoints points : over_speed_gps_points) {
							event_data_map = new HashMap<String, Object>();
							event_data_map.put(Constants.FIELD_EVENT_REPORT_START_TIME, points.getTime());
							event_data_map.put(Constants.FIELD_EVENT_REPORT_END_TIME, null);
							event_data_map.put(Constants.FIELD_DURATION, points.getTimeDiff());
							event_data_map.put(Constants.FIELD_EVENT_REPORT_DISTANCE, 0f);
							event_data_map.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, 0);
							event_data_map.put(Constants.FIELD_EVENT_ID, createTelemeticsEventReport.get_id());
							event_data_map.put(Constants.FIELD_START_POINT_COORDINATES, startPointCoordinates);
							StartPointCoordinates coordinates = new StartPointCoordinates();
							coordinates.setLatitude(points.getLatitude());
							coordinates.setLongitude(points.getLongitude());
							event_data_map.put(Constants.FIELD_END_POINT_COORDINATES, null);
							event_data_map.put(Constants.FIELD_AMBER_REPORT_IDLE_TIME, (float) points.getTimeDiff());
							createTelemeticsEventReport(Constants.EVENTREPORT_IDELTIME_GPSPOINT, event_data_map);
						}
					}
					over_speed_gps_points = new ArrayList<GPSPoints>();
					time_on_over_speed = 0;
					distance_with_over_speed = 0.0f;
					startPointCoordinates = null;
					start_time = null;
				}
				endPointCoordinates = new EndPointCoordinates();
			}
			i++;
		}
		return data;
	}

	public Map<String, Object> baseCalculateSpeedCount() {
		SpeedingEvents speedingEvents = telematicsSettings.getSpeedingEvent();
		LOGGER.info("Trip ID=" + trip.getTripId() + ";\n amber auth token:" + trip.getAmberAuthToken() + "\n" + "trip date=" + trip.getStartTime()
				+ "\n" + trip.getEndTime());
		LOGGER.info("Speed even limit=" + speedingEvents.getSpeedLimit());
		Float speeding_event = 0.0f;
		int max_speed = 0;
		int time_on_over_speed = 0;
		float distance_with_over_speed = 0;
		int total_timeDiff = 0;
		Float total_distanceDiff = 0.0f;
		if (speedingEvents != null) {
			int i = 0;
			try {
				for (GPSPoints gpsPoints : trip.getGPSPoints()) {
					int speed = gpsPoints.getSpeed();
					if (speed > max_speed) {
						max_speed = speed;
					}
					if (speed >= speedingEvents.getSpeedLimit() && i <= trip.getGPSPoints().size() - 1
							&& trip.getGPSPoints().get(i + 1).getSpeed() >= speedingEvents.getSpeedLimit()) {
						time_on_over_speed = time_on_over_speed + trip.getGPSPoints().get(i + 1).getTimeDiff();
						distance_with_over_speed = distance_with_over_speed + trip.getGPSPoints().get(i + 1).getDistanceDiff();
					} else {
						if (time_on_over_speed > 0 && (time_on_over_speed / 1000) > speedingEvents.getSpeedingTime()) {
							speeding_event++;
							total_timeDiff = total_timeDiff + time_on_over_speed;
							total_distanceDiff = total_distanceDiff + distance_with_over_speed;
						}
						time_on_over_speed = 0;
						distance_with_over_speed = 0.0f;
					}
					i++;
				}
			} catch (Exception exception) {
				LOGGER.error("speeding_event=" + exception);
			}
		}
		LOGGER.info("speeding_event=" + speeding_event);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put(Constants.SPEEDING_EVENT, speeding_event);
		data.put(Constants.TIME_ON_OVER_SPEED, 0.0f + total_timeDiff);
		data.put(Constants.DISTANCE_WITH_OVER_SPEED, 0.0f + total_distanceDiff);
		data.put(Constants.MAX_SPEED, 0.0f + max_speed);
		return data;
	}

	public TelematicsEventReport createTelemeticsEventReport(String report_name, Map<String, Object> event_data_map) {
		if (!telematicsSettings.getCreatedBy().equals(Constants.TELEMATICS_REPORT_JOB) && telematicsSettings.isIs_event_report_generation()) {
			TelematicsEventReport eventReport = new TelematicsEventReport();
			eventReport.setTelematics_id(telematicsSettings.getTelematicsId());
			eventReport.setAmberAuthToken(trip.getAmberAuthToken());
			eventReport.setFleet_id(telematicsSettings.getFleetId());
			eventReport.setGroupId(group_id);
			eventReport.setSubGroupId(sub_group_id);
			eventReport.setCreated_date(new Date());
			eventReport.setReport_date(report_date);
			eventReport.setReport_name(report_name);
			eventReport.setTripId(trip.getTripId());
			eventReport.setDriverName(driver_name);
			eventReport.setDriverId(trip.getDriverId());
			eventReport.setTelematics_id(telematicsSettings.getTelematicsId());
			eventReport.setSettingType(telematicsSettings.getSettingType());
			if (event_data_map.get(Constants.FIELD_START_POINT_COORDINATES) != null) {
				eventReport.setStartPointCoordinates((StartPointCoordinates) event_data_map.get(Constants.FIELD_START_POINT_COORDINATES));
			}
			if (event_data_map.get(Constants.FIELD_END_POINT_COORDINATES) != null) {
				eventReport.setEndPointCoordinates((EndPointCoordinates) event_data_map.get(Constants.FIELD_END_POINT_COORDINATES));
			}
			if (event_data_map.get(Constants.FIELD_EVENT_REPORT_START_TIME) != null) {
				eventReport.setStart_time((Date) event_data_map.get(Constants.FIELD_EVENT_REPORT_START_TIME));
			}
			if (event_data_map.get(Constants.FIELD_EVENT_REPORT_END_TIME) != null) {
				eventReport.setEnd_time((Date) event_data_map.get(Constants.FIELD_EVENT_REPORT_END_TIME));
			}
			if (event_data_map.get(Constants.FIELD_DURATION) != null) {
				eventReport.setDuration((Integer) event_data_map.get(Constants.FIELD_DURATION));
			}
			if (event_data_map.get(Constants.FIELD_EVENT_REPORT_DISTANCE) != null) {
				eventReport.setDistance((Float) event_data_map.get(Constants.FIELD_EVENT_REPORT_DISTANCE));
			}
			if (event_data_map.get(Constants.FIELD_AMBER_REPORT_MAX_SPEED) != null) {
				eventReport.setMax_speed((Integer) event_data_map.get(Constants.FIELD_AMBER_REPORT_MAX_SPEED));
			}
			if (event_data_map.get(Constants.FIELD_EVENT_ID) != null) {
				eventReport.setEvent_id(event_data_map.get(Constants.FIELD_EVENT_ID).toString());
			}
			if (event_data_map.get(Constants.FIELD_AMBER_REPORT_IDLE_TIME) != null) {
				eventReport.setIdleTime((Float) event_data_map.get(Constants.FIELD_AMBER_REPORT_IDLE_TIME));
			}
			if (event_data_map.get(Constants.FIELD_TRIP_START_POINT) != null) {
				eventReport.setTripStartPointLocation(event_data_map.get(Constants.FIELD_TRIP_START_POINT).toString());
			}
			if (event_data_map.get(Constants.FIELD_TRIP_END_POINT) != null) {
				eventReport.setTripEndPointLocation(event_data_map.get(Constants.FIELD_TRIP_END_POINT).toString());
			}
			if (report_name.equals(Constants.EVENTREPORT_FATIGUE_DAY) || report_name.equals(Constants.EVENTREPORT_FATIGUE_NIGHT)) {
				event_data_map.put(Constants.FIELD_TRIP_START_POINT, trip.getTripStartPoint());
				event_data_map.put(Constants.FIELD_TRIP_END_POINT, trip.getTripEndPoint());
			}
			String dateMonthYear[] = UtilFunction.getDateMonthYear(report_date).split("_");
			// TelematicsEventReportLogic telematicsEventReportLogic = new
			// TelematicsEventReportLogic(dateMonthYear[2] + dateMonthYear[1]);
			TelematicsEventReportLogic telematicsEventReportLogic = new TelematicsEventReportLogic();
			return telematicsEventReportLogic.save(eventReport.getBasicDBObject());
		}
		return null;
	}

	public static void main(String a[]) {
		// AIzaSyC-DbuD5SZ-j6Ew0obriWMnUf7ZB29qdd8
		TelematicsSettings telematicsSettings=new TelematicsSettings();
 
		if (!Constants.TELEMATICS_REPORT_JOB.equals("ADMIN") && telematicsSettings.isIs_event_report_generation()) {
			System.out.println("test");
		}else {
			System.out.println("test1");			
		}
	}
}
