package com.amber.event;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amber.logic.DevicesLogic;
import com.amber.logic.TripsLogic;
import com.amber.model.Devices;
import com.amber.model.EndPointCoordinates;
import com.amber.model.GPSPoints;
import com.amber.model.StartPointCoordinates;
import com.amber.model.TelematicsEventReport;
import com.amber.model.TelematicsSettings;
import com.amber.model.Trips;
import com.amber.util.Constants;
import com.model.subdoc.TelematicsSettings.IdlingEvents;
import com.mongodb.BasicDBObject;

public class IdleTimeEventCalculator extends TelematicsEventsCalculator {
	public StartPointCoordinates startPointCoordinates;
	public EndPointCoordinates endPointCoordinates;
	private Integer total_timeDiff = 0;
	private Float total_distanceDiff = 0f;
	private Date start_time;
	private String event_id;
	private Date end_time;
	public List<GPSPoints> over_speed_gps_points = new ArrayList<GPSPoints>();
	private Integer max_speed = 0;

	public IdleTimeEventCalculator(Trips trip, TelematicsSettings telematicsSettings) {
		init(trip, telematicsSettings);
	}

	@Override
	public void init(Trips trip, TelematicsSettings telematicsSettings) {
		super.trip = trip;
		super.telematicsSettings = telematicsSettings;
		if (trip.getAmberAuthToken() != null) {
			Devices devices = DevicesLogic.getInstance().getModelByDeviceAuthenticationToken(trip.getAmberAuthToken());
			if (devices.getGroupId() != null) {
				super.group_id = devices.getGroupId();
			}
			if (devices.getSubGroupId() != null) {
				super.sub_group_id = devices.getSubGroupId();
			}
		}
		super.initDataMap();
	}

	@Override
	public void setTelematicsSettings() {
		// TODO Auto-generated method stub
	}

	@Override
	public Map<String, Object> executeEventCalculation() {
		if (telematicsSettings.getIdlingEvent() != null) {
			return calculateIdelTimeCount();
		} else {
			return baseCalculateIdelTimeCount();
		}
	}

	@Override
	public void setTelematicsEventData() {
		event_data_map.put(Constants.FIELD_EVENT_REPORT_START_TIME, start_time);
		event_data_map.put(Constants.FIELD_EVENT_REPORT_END_TIME, end_time);
		event_data_map.put(Constants.FIELD_DURATION, total_timeDiff);
		event_data_map.put(Constants.FIELD_EVENT_REPORT_DISTANCE, total_distanceDiff);
		event_data_map.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, 0);
		event_data_map.put(Constants.FIELD_EVENT_ID, event_id);
		event_data_map.put(Constants.FIELD_START_POINT_COORDINATES, startPointCoordinates);
		event_data_map.put(Constants.FIELD_END_POINT_COORDINATES, endPointCoordinates);
		event_data_map.put(Constants.FIELD_AMBER_REPORT_IDLE_TIME, (float) total_timeDiff);
	}

	public StartPointCoordinates getStartPointCoordinates() {
		return startPointCoordinates;
	}

	public void setStartPointCoordinates(GPSPoints gpsPoints) {
		if (gpsPoints != null) {
			this.startPointCoordinates = new StartPointCoordinates();
			startPointCoordinates.setLatitude(gpsPoints.getLatitude());
			startPointCoordinates.setLongitude(gpsPoints.getLongitude());
		}
	}

	public EndPointCoordinates getEndPointCoordinates() {
		return endPointCoordinates;
	}

	public void setEndPointCoordinates(GPSPoints gpsPoints) {
		if (gpsPoints != null) {
			this.endPointCoordinates = new EndPointCoordinates();
			endPointCoordinates.setLatitude(gpsPoints.getLatitude());
			endPointCoordinates.setLongitude(gpsPoints.getLongitude());
		}
	}

	public Map<String, Object> calculateIdelTimeCount___old_code() {
		IdlingEvents idlingEvent = telematicsSettings.getIdlingEvent();
		Map<String, Object> data = new HashMap<String, Object>();
		List<GPSPoints> gpsPoints = trip.getGPSPoints();
		int index = 0;
		long idleTimeCalculation = 0;
		List<GPSPoints> write_points = new ArrayList<GPSPoints>();
		int event_count = 0;
		float total_idel_time = 0;
		/**
		 * As per discussion with Raja,27-06-2019, idle time is differs between fleet
		 * portal and telematics portal. So now code changes made to get time from
		 * gpsInsertedTime() instead of time.
		 */
		for (GPSPoints points : gpsPoints) {
			if (index > 1) {
				long idle_time_diff = gpsPoints.get(index).getTime().getTime()
						- gpsPoints.get(index - 1).getTime().getTime();
				int pre_speed = 0;
				pre_speed = gpsPoints.get(index - 1).getSpeed();
				long idleTimeDiffBuffer = 0;
				// To avoid buffered packet idle time
				idleTimeDiffBuffer = gpsPoints.get(index - 1).getTime().getTime()
						- gpsPoints.get(index - 2).getTime().getTime();
				if (pre_speed == 0 && idleTimeDiffBuffer >= 0) {
					// System.out.println("index=" + index + ";pre_speed= " + pre_speed);
					idleTimeCalculation += idle_time_diff; 
					write_points.add(gpsPoints.get(index));
				} else if (pre_speed < 20 && ((idle_time_diff / 1000) > 50) && idleTimeDiffBuffer >= 0) {
					idleTimeCalculation += idle_time_diff; 
					write_points.add(gpsPoints.get(index));
				} else {
					if (write_points.size() > 0) {
						// System.out.println("----");
						if ((idleTimeCalculation / 1000) >= idlingEvent.getIdlingTime()) {
							event_count++;
							updateIdelTimeGPSPoints(gpsPoints.get(index - 1), write_points,
									((int) idleTimeCalculation));
							total_idel_time += idleTimeCalculation;
							write_points = new ArrayList<GPSPoints>();
							idleTimeCalculation = 0;
						}
					}
				}
			}
			index++;
		}
		data_map.put(Constants.EVENT_COUNT, event_count);
		data_map.put(Constants.FIELD_AMBER_REPORT_IDLE_TIME, total_idel_time);
		return data_map;
	}

	public Map<String, Object> calculateIdelTimeCount() {
		IdlingEvents idlingEvent = telematicsSettings.getIdlingEvent();
		Map<String, Object> data = new HashMap<String, Object>();
		List<GPSPoints> gpsPoints = trip.getGPSPoints();
		int index = 0;
		long idleTimeCalculation = 0;
		List<GPSPoints> write_points = new ArrayList<GPSPoints>();
		int event_count = 0;
		float total_idel_time = 0;
		for (GPSPoints points : gpsPoints) {
			if (index > 1) {
				long idle_time_diff = gpsPoints.get(index).getTime().getTime()
						- gpsPoints.get(index - 1).getTime().getTime();
				int pre_speed = 0;
				pre_speed = gpsPoints.get(index - 1).getSpeed();
				long idleTimeDiffBuffer = 0;
				// To avoid buffered packet idle time
				idleTimeDiffBuffer = gpsPoints.get(index - 1).getTime().getTime()
						- gpsPoints.get(index - 2).getTime().getTime();
				if (pre_speed == 0 && idleTimeDiffBuffer >= 0) {
					// System.out.println("index=" + index + ";pre_speed= " + pre_speed);
					idleTimeCalculation += idle_time_diff; 
					write_points.add(gpsPoints.get(index));
				} else if (pre_speed < 20 && ((idle_time_diff / 1000) > 50) && idleTimeDiffBuffer >= 0) {
					idleTimeCalculation += idle_time_diff; 
					write_points.add(gpsPoints.get(index));
				} else {
					if (write_points.size() > 0) {
						// System.out.println("----");
						if ((idleTimeCalculation / 1000) >= idlingEvent.getIdlingTime()) {
							event_count++; 
							updateIdelTimeGPSPoints(gpsPoints.get(index - 1), write_points,
									((int) idleTimeCalculation));
							total_idel_time += idleTimeCalculation;
							write_points = new ArrayList<GPSPoints>();
							idleTimeCalculation = 0;
						}
					}
				}
			}
			index++;
		}
		data_map.put(Constants.EVENT_COUNT, event_count);
		data_map.put(Constants.FIELD_AMBER_REPORT_IDLE_TIME, total_idel_time);
		return data_map;
	}

	public void createTelemeticsEventReportGPSPoints(TelematicsEventReport overspeed_EventReport) {
		for (GPSPoints gpsPoints : this.over_speed_gps_points) {
			event_data_map = new HashMap<String, Object>();
			start_time = gpsPoints.getTime();
			end_time = null;
			// total_timeDiff = gpsPoints.getTimeDiff();
			total_distanceDiff = 0f;
			max_speed = 0;
			event_id = overspeed_EventReport.get_id();
			setStartPointCoordinates(gpsPoints);
			setEndPointCoordinates(null);
			setTelematicsEventData();
			createTelemeticsEventReport(Constants.EVENTREPORT_IDELTIME_GPSPOINT, event_data_map);
		}
	}

	public static void main(String a[]) {
		TripsLogic logic = new TripsLogic();
		BasicDBObject allQuery = new BasicDBObject();
		allQuery.append(Constants.FIELD_AMBER_AUTH_TOKEN, "EL8J51T3P2VF");
		allQuery.append(Constants.FIELD_TRIP_ID, "EL8J51T3P2VF20190624174303");
		Trips modelByQuery = logic.getModelByQuery(allQuery);
		TelematicsSettings settings = new TelematicsSettings();
		IdlingEvents idlingEvents = new IdlingEvents();
		idlingEvents.setCondition("gte");
		idlingEvents.setIdlingTime(600);
		settings.setIdlingEvent(idlingEvents);
		settings.setCreatedBy("ADMIN");
		IdleTimeEventCalculator calculator = new IdleTimeEventCalculator(modelByQuery, settings);
		calculator.executeEventCalculation();
		System.out.println(modelByQuery.getRunTime() + "," + calculator.getData_map());
	}
}
