package com.amber.event;

import java.util.HashMap;
import java.util.Map;

import com.amber.logic.DevicesLogic;
import com.amber.model.Devices;
import com.amber.model.EndPointCoordinates;
import com.amber.model.GPSPoints;
import com.amber.model.StartPointCoordinates;
import com.amber.model.TelematicsSettings;
import com.amber.model.Trips;
import com.amber.util.Constants;
import com.amber.util.UtilFunction;
import com.model.subdoc.TelematicsSettings.FatigueEvent;

public class FatigueNightEventCalculator extends TelematicsEventsCalculator {
	public StartPointCoordinates startPointCoordinates;
	public EndPointCoordinates endPointCoordinates;

	public StartPointCoordinates getStartPointCoordinates() {
		return startPointCoordinates;
	}

	public void setStartPointCoordinates(GPSPoints gpsPoints) {
		if (gpsPoints != null) {
			this.startPointCoordinates = new StartPointCoordinates();
			startPointCoordinates.setLatitude(gpsPoints.getLatitude());
			startPointCoordinates.setLongitude(gpsPoints.getLongitude());
		}
	}

	public EndPointCoordinates getEndPointCoordinates() {
		return endPointCoordinates;
	}

	public void setEndPointCoordinates(GPSPoints gpsPoints) {
		if (gpsPoints != null) {
			this.endPointCoordinates = new EndPointCoordinates();
			endPointCoordinates.setLatitude(gpsPoints.getLatitude());
			endPointCoordinates.setLongitude(gpsPoints.getLongitude());
		}
	}

	public FatigueNightEventCalculator(Trips trip, TelematicsSettings telematicsSettings) {
		init(trip, telematicsSettings);
	}

	@Override
	public void init(Trips trip, TelematicsSettings telematicsSettings) {
		super.trip = trip;
		super.trip= tripsLogic.getTripByTripId(trip.getTripId());
		if (trip.getAmberAuthToken() != null) {
			Devices devices = DevicesLogic.getInstance().getModelByDeviceAuthenticationToken(trip.getAmberAuthToken());
			if (devices.getGroupId() != null) {
				super.group_id = devices.getGroupId();
			}
			if (devices.getSubGroupId() != null) {
				super.sub_group_id = devices.getSubGroupId();
			}
		}
		super.telematicsSettings = telematicsSettings;
		super.initDataMap();
	}

	@Override
	public void setTelematicsSettings() {
		// TODO Auto-generated method stub
	}

	@Override
	public Map<String, Object> executeEventCalculation() {
		FatigueEvent fatigueNightEvent = telematicsSettings.getFatigueNightEvent();
		Map<String, Object> countMap = new HashMap<String, Object>();
		countMap.put(Constants.EVENT_COUNT, 0f);
		if ((fatigueNightEvent.getDriveDistance() != null && (trip.getDistance() / 1000) >= Float.parseFloat(fatigueNightEvent.getDriveDistance()))
				|| (fatigueNightEvent.getDriveTime() != null && (trip.getRunTime() / 1000) >= Float.parseFloat(fatigueNightEvent.getDriveTime()))) {
			if (fatigueNightEvent != null) {
				String trip_start_time = UtilFunction.getHrMinSec(trip.getStartTime());
				if (UtilFunction.isBetweenTwoHrS(fatigueNightEvent.getStartTimeUTC(), fatigueNightEvent.getEndTimeUTC(), trip_start_time)) {
					countMap.put(Constants.EVENT_COUNT, 1f); 
					setStartPointCoordinates( trip.getGPSPoints().get(0)); 
					setEndPointCoordinates(trip.getGPSPoints().get(trip.getGPSPoints().size()-1));
					setTelematicsEventData();
					createTelemeticsEventReport(Constants.EVENTREPORT_FATIGUE_NIGHT, event_data_map);
				}
			}
		}
		return countMap;
	}

	@Override
	public void setTelematicsEventData() {
		int i= (int) (0+trip.getRunTime()); 
		event_data_map.put(Constants.FIELD_EVENT_REPORT_START_TIME, trip.getStartTime());
		event_data_map.put(Constants.FIELD_EVENT_REPORT_END_TIME, trip.getEndTime());
		event_data_map.put(Constants.FIELD_DURATION, i);
		event_data_map.put(Constants.FIELD_EVENT_REPORT_DISTANCE, trip.getDistance());
		event_data_map.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, trip.getTopSpeed());
		event_data_map.put(Constants.FIELD_EVENT_ID, null);
		event_data_map.put(Constants.FIELD_START_POINT_COORDINATES, startPointCoordinates);
		event_data_map.put(Constants.FIELD_END_POINT_COORDINATES, endPointCoordinates);
		event_data_map.put(Constants.FIELD_AMBER_REPORT_IDLE_TIME, trip.getIdleTime());
		event_data_map.put(Constants.FIELD_TRIP_START_POINT, trip.getTripStartPoint());
		event_data_map.put(Constants.FIELD_TRIP_END_POINT, trip.getTripEndPoint());
	}
}
