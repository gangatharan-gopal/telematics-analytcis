package com.amber.event;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.amber.logic.DevicesLogic;
import com.amber.logic.TripsLogic;
import com.amber.model.Devices;
import com.amber.model.EndPointCoordinates;
import com.amber.model.GPSPoints;
import com.amber.model.StartPointCoordinates;
import com.amber.model.TelematicsSettings;
import com.amber.model.Trips;
import com.amber.util.Constants;
import com.model.subdoc.TelematicsSettings.SuddenAccelEvent;
import com.mongodb.BasicDBObject;

public class HarshAccelarationEventCalculator extends TelematicsEventsCalculator {
	public StartPointCoordinates startPointCoordinates;
	public EndPointCoordinates endPointCoordinates;
	private Integer max_speed = 0;
	private Date start_time;
	private Date end_time;
	private Integer total_timeDiff;
	private Float total_distanceDiff;

	public HarshAccelarationEventCalculator(Trips trip, TelematicsSettings telematicsSettings) {
		init(trip, telematicsSettings);
	}

	@Override
	public void init(Trips trip, TelematicsSettings telematicsSettings) {
		super.trip = trip;
		super.telematicsSettings = telematicsSettings;
		if (trip.getAmberAuthToken() != null) {
			Devices devices = DevicesLogic.getInstance().getModelByDeviceAuthenticationToken(trip.getAmberAuthToken());
			if (devices.getGroupId() != null) {
				super.group_id = devices.getGroupId();
			}
			if (devices.getSubGroupId() != null) {
				super.sub_group_id = devices.getSubGroupId();
			}
		}
		super.initDataMap();
	}

	@Override
	public void setTelematicsSettings() {
		// TODO Auto-generated method stub
	}

	@Override
	public Map<String, Object> executeEventCalculation() {
		if (telematicsSettings.getSuddenAccelEvent() != null) {
			return calculateHACount();
		} else {
			return baseCalculateHACount();
		}
	}

	public StartPointCoordinates getStartPointCoordinates() {
		return startPointCoordinates;
	}

	public void setStartPointCoordinates(GPSPoints gpsPoints) {
		if (gpsPoints != null) {
			this.startPointCoordinates = new StartPointCoordinates();
			startPointCoordinates.setLatitude(gpsPoints.getLatitude());
			startPointCoordinates.setLongitude(gpsPoints.getLongitude());
		}
	}

	public EndPointCoordinates getEndPointCoordinates() {
		return endPointCoordinates;
	}

	public void setEndPointCoordinates(GPSPoints gpsPoints) {
		if (gpsPoints != null) {
			this.endPointCoordinates = new EndPointCoordinates();
			endPointCoordinates.setLatitude(gpsPoints.getLatitude());
			endPointCoordinates.setLongitude(gpsPoints.getLongitude());
		}
	}

	public Map<String, Object> calculateHACount() {
		if (telematicsSettings.getSuddenAccelEvent() != null && telematicsSettings.getSuddenAccelEvent().getSpeedingTime() != null
				&& telematicsSettings.getSuddenAccelEvent().getSpeedLimit() != null) {
			List<GPSPoints> gpsPoints = trip.getGPSPoints();
			int event_count = 0;
			if (gpsPoints != null && gpsPoints.size() > 0) {
				for (int i = 1; i < gpsPoints.size() - 1; i++) {
					GPSPoints pre_gpsPoint = gpsPoints.get(i - 1);
					GPSPoints gpsPoint = gpsPoints.get(i);
					long time_diff = gpsPoint.getTime().getTime() - pre_gpsPoint.getTime().getTime();
					time_diff = time_diff / 1000;
					int speed = gpsPoint.getSpeed() - pre_gpsPoint.getSpeed();
					if (time_diff > 0 && speed >= telematicsSettings.getSuddenAccelEvent().getSpeedLimit()
							&& time_diff <= telematicsSettings.getSuddenAccelEvent().getSpeedingTime()) {
						event_count++;
						setStartPointCoordinates(pre_gpsPoint);
						setEndPointCoordinates(gpsPoint);
						start_time = pre_gpsPoint.getTime();
						end_time = gpsPoint.getTime();
						this.max_speed = gpsPoint.getSpeed();
						this.total_timeDiff = (int) time_diff;
						this.total_distanceDiff = gpsPoint.getDistanceDiff();
						setTelematicsEventData();
						createTelemeticsEventReport(Constants.EVENTREPORT_SUDDENACCELARATION, event_data_map);
					}
				}
			}
			data_map.put(Constants.EVENT_COUNT, event_count);
			return data_map;
		} else {
			return super.baseCalculateHACount();
		}
	}

	@Override
	public void setTelematicsEventData() {
		event_data_map.put(Constants.FIELD_EVENT_REPORT_START_TIME, start_time);
		event_data_map.put(Constants.FIELD_EVENT_REPORT_END_TIME, end_time);
		event_data_map.put(Constants.FIELD_DURATION, total_timeDiff);
		event_data_map.put(Constants.FIELD_EVENT_REPORT_DISTANCE, total_distanceDiff);
		event_data_map.put(Constants.FIELD_AMBER_REPORT_MAX_SPEED, max_speed);
		event_data_map.put(Constants.FIELD_EVENT_ID, null);
		event_data_map.put(Constants.FIELD_START_POINT_COORDINATES, startPointCoordinates);
		event_data_map.put(Constants.FIELD_END_POINT_COORDINATES, endPointCoordinates);
	}

	public static void main(String a[]) {
		// "_id" : "TEST_EVENTREPORT20150921113637",
		// "AmberAuthToken" : "TEST_EVENTREPORT",
		TripsLogic logic = new TripsLogic();
		BasicDBObject allQuery = new BasicDBObject();
		allQuery.append(Constants.FIELD_AMBER_AUTH_TOKEN, "C3FP5M8DB1G2");
		allQuery.append(Constants.FIELD_TRIP_ID, "C3FP5M8DB1G220180529113744");
		Trips modelByQuery = logic.getModelByQuery(allQuery); 
		TelematicsSettings settings = new TelematicsSettings();
		settings.setFleetId("959506");
		settings.setSettingType("FLEET");
		settings.setGroupId("GR!");
		settings.setSubGroupId("subgr1");
//		SuddenAccelEvent suddenAccelEvent = new SuddenAccelEvent();
//		suddenAccelEvent.setSpeedingTime(10);
//		suddenAccelEvent.setSpeedLimit(30);
//		settings.setSuddenAccelEvent(suddenAccelEvent);
		settings.setCreatedBy(Constants.TELEMATICS_REPORT_JOB+"1");
		HarshAccelarationEventCalculator calculator = new HarshAccelarationEventCalculator(modelByQuery, settings);
		calculator.calculateHACount();
	}
}
