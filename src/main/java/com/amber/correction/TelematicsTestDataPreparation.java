package com.amber.correction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.amber.logic.DevicesLogic;
import com.amber.logic.TelematicsDailyDriverReportLogic;
import com.amber.logic.TelematicsDailyDrivingFleetReportLogic;
import com.amber.logic.TelematicsDailyDrivingReportLogic;
import com.amber.logic.TelematicsEventReportLogic;
import com.amber.logic.TelematicsSettingsLogic;
import com.amber.model.Devices;
import com.amber.model.TelematicsDailyDriverReport;
import com.amber.model.TelematicsDailyDrivingReport;
import com.amber.model.TelematicsDailyFleetReport;
import com.amber.model.TelematicsEventReport;
import com.amber.model.TelematicsSettings;
import com.amber.util.AmberDailyDrivingReportMonitorStatus;
import com.amber.util.AmberDrivingReportType;
import com.amber.util.Constants;
import com.amber.util.UtilFunction;
import com.mongodb.BasicDBObject;

public class TelematicsTestDataPreparation {
	private TelematicsDailyDrivingFleetReportLogic dailyFleetReportLogic = new TelematicsDailyDrivingFleetReportLogic();
	private TelematicsDailyDriverReportLogic dailyDriverReportLogic = new TelematicsDailyDriverReportLogic();
	private TelematicsDailyDrivingReportLogic dailyDrivingReportLogic = new TelematicsDailyDrivingReportLogic();
	private TelematicsEventReportLogic eventReportLogic = new TelematicsEventReportLogic();
	private TelematicsSettingsLogic settingsLogic = new TelematicsSettingsLogic();
	private Map<AmberDailyDrivingReportMonitorStatus, TelematicsDailyDrivingReport> fleet_report_by_tyep = new LinkedHashMap<AmberDailyDrivingReportMonitorStatus, TelematicsDailyDrivingReport>();

	public void createFile() throws IOException {
		File file = new File("append.txt");
		if (!file.exists()) {
			file.createNewFile();
		} else {
			file.delete();
			file.createNewFile();
		}
	}

	private void writeToFile(String data) throws IOException {
		File file = new File("append.txt");
		FileWriter fr = new FileWriter(file, true);
		fr.write("\n" + data);
		fr.close();
	}

	protected List<TelematicsSettings> writeData(String amber_auth_token, String date_, int count) throws IOException {
		List<TelematicsSettings> settings = new ArrayList<TelematicsSettings>();
		DevicesLogic devicesLogic = new DevicesLogic();
		Devices device = devicesLogic.getModelByDeviceAuthenticationToken(amber_auth_token);
		if (device != null) {
			String fleet_id = device.getFleetId();
			List<TelematicsSettings> allModelByFleetId = settingsLogic.getAllModelByFleetId(fleet_id);
			for (TelematicsSettings telematicsSettings : allModelByFleetId) {
				writeToFile("Telematics Settings: " + telematicsSettings.toJson());
				writeDrivingReportByTokenAndDate(amber_auth_token, date_, telematicsSettings, count);
				writeFleetReport(fleet_id, date_, telematicsSettings, count);
				writeTelematicsEventReport(telematicsSettings, amber_auth_token, date_);
				writeDriverReport(telematicsSettings, amber_auth_token, date_);
				writeToFile("----------------------------------------------------");
			}
		}
		return settings;
	}

	private void writeDriverReport(TelematicsSettings telematicsSettings, String amber_auth_token, String date_)
			throws IOException {
		String[] split = date_.split("-");
		Date report_date = UtilFunction.getMongoDate(split[0], split[1], split[2], "00", "00", "00");
		String dateWeekMonthYear[] = UtilFunction.getDateWeekMonthYear(report_date).split("_");
		int week_of_month = Integer.parseInt(dateWeekMonthYear[1]);
		int week_of_year = UtilFunction.getWeekOfYear(report_date);
		BasicDBObject basicDBObject = new BasicDBObject();
		basicDBObject.append(Constants.FIELD_FLEET_REPORT_FLEET_ID, telematicsSettings.getFleetId());
		basicDBObject.append(Constants.FIELD_GROUP_ID, telematicsSettings.getGroupId());
		basicDBObject.append(Constants.FIELD_SUB_GROUP_ID, telematicsSettings.getSubGroupId());
		basicDBObject.append(Constants.FIELD_RISK_SCORE_TELEMATICS_ID, telematicsSettings.getTelematicsId());
		basicDBObject.append(Constants.FIELD_AMBER_AUTH_TOKEN, amber_auth_token);
		basicDBObject.append(Constants.FIELD_AMBER_REPORTING_DATE, report_date);
		basicDBObject.append(Constants.FIELD_YEAR, Integer.parseInt(split[2]));
		basicDBObject.append(Constants.FIELD_MONTH, Integer.parseInt(split[1]));
		basicDBObject.append(Constants.FIELD_AMBER_REPORT_WEEK_OF_MONTH, week_of_month);
		basicDBObject.append(Constants.FIELD_AMBER_REPORT_WEEK_OF_YEAR, week_of_year);
		basicDBObject.append(Constants.FIELD_AMBER_REPORT_DATE, Integer.parseInt(split[0]));
		AmberDailyDrivingReportMonitorStatus status = AmberDailyDrivingReportMonitorStatus.DAY;
		TelematicsDailyDriverReport modelByQuery = null;
		switch (status) {
		case DAY:
			modelByQuery = dailyDriverReportLogic.getModelByQuery(basicDBObject);
			if (modelByQuery != null)
				writeToFile("TelematicsDailyDriverReport Day: \n" + modelByQuery.toJson());
		case WEEK:
			if (modelByQuery != null) {
				TelematicsDailyDriverReport driver_report = dailyDriverReportLogic
						.getModelByQuery(TelematicsDailyDriverReportLogic.getSearchQuery(AmberDrivingReportType.WEEK,
								modelByQuery, modelByQuery.getSettingType()));
				writeToFile("TelematicsDailyDriverReport Week: \n" + driver_report.toJson());
			}
		case MONTH:
			if (modelByQuery != null) {
				TelematicsDailyDriverReport driver_report = dailyDriverReportLogic
						.getModelByQuery(TelematicsDailyDriverReportLogic.getSearchQuery(AmberDrivingReportType.MONTH,
								modelByQuery, modelByQuery.getSettingType()));
				writeToFile("TelematicsDailyDriverReport Month: \n" + driver_report.toJson());

			}
		case YEAR:
			if (modelByQuery != null) {
				TelematicsDailyDriverReport driver_report = dailyDriverReportLogic
						.getModelByQuery(TelematicsDailyDriverReportLogic.getSearchQuery(AmberDrivingReportType.YEAR,
								modelByQuery, modelByQuery.getSettingType()));
				writeToFile("TelematicsDailyDriverReport Month: \n" + driver_report.toJson());
			}
		default:

			break;
		}
	}

	private void writeTelematicsEventReport(TelematicsSettings telematicsSettings, String amber_auth_token,
			String date_) throws IOException {
		BasicDBObject basicDBObject = new BasicDBObject();
		String[] split = date_.split("-");
		Date report_date = UtilFunction.getMongoDate(split[0], split[1], split[2], "00", "00", "00");
		basicDBObject.append(Constants.FIELD_FLEET_REPORT_FLEET_ID, telematicsSettings.getFleetId());
		basicDBObject.append(Constants.FIELD_GROUP_ID, telematicsSettings.getGroupId());
		basicDBObject.append(Constants.FIELD_SUB_GROUP_ID, telematicsSettings.getSubGroupId());
		basicDBObject.append(Constants.FIELD_RISK_SCORE_TELEMATICS_ID, telematicsSettings.getTelematicsId());
		basicDBObject.append(Constants.FIELD_AMBER_AUTH_TOKEN, amber_auth_token);
		basicDBObject.append(Constants.FIELD_REPORT_DATE, report_date);
		List<TelematicsEventReport> modelByQueries = eventReportLogic.getAllRecords(basicDBObject);
		System.out.println(modelByQueries.size());
		for (TelematicsEventReport eventReport : modelByQueries) {
			writeToFile("TelematicsEventReport " + eventReport.toJson());
		}

	}

	private void writeFleetReport(String fleet_id, String date_, TelematicsSettings telematicsSettings, int count)
			throws IOException {
		for (AmberDailyDrivingReportMonitorStatus status : fleet_report_by_tyep.keySet()) {
			TelematicsDailyDrivingReport telematicsDailyDrivingReport = fleet_report_by_tyep.get(status);
			BasicDBObject query = dailyFleetReportLogic.getSearchQueryForReport(telematicsDailyDrivingReport);
			TelematicsDailyFleetReport modelByQuery = dailyFleetReportLogic.getModelByQuery(query);
			writeToFile("TelematicsDailyFleetReport (" + status + ") :" + modelByQuery.getAmberDrivingReportType()
					+ ": \n" + modelByQuery.toJson());

		}

	}

	private TelematicsDailyDrivingReport writeDrivingReportByTokenAndDate(String amber_auth_token, String date_,
			TelematicsSettings telematicsSettings, int count) throws IOException {
		String[] split = date_.split("-");
		Date report_date = UtilFunction.getMongoDate(split[0], split[1], split[2], "00", "00", "00");
		String dateWeekMonthYear[] = UtilFunction.getDateWeekMonthYear(report_date).split("_");
		BasicDBObject basicDBObject = new BasicDBObject();
		basicDBObject.append(Constants.FIELD_AMBER_AUTH_TOKEN, amber_auth_token);
		basicDBObject.append(Constants.FIELD_YEAR, Integer.parseInt(split[2]));
		basicDBObject.append(Constants.FIELD_MONTH, Integer.parseInt(split[1]));
		int week_of_month = Integer.parseInt(dateWeekMonthYear[1]);
		int week_of_year = UtilFunction.getWeekOfYear(report_date);
		basicDBObject.append(Constants.FIELD_AMBER_REPORT_WEEK_OF_MONTH, week_of_month);
		basicDBObject.append(Constants.FIELD_AMBER_REPORT_WEEK_OF_YEAR, week_of_year);
		basicDBObject.append(Constants.FIELD_AMBER_REPORT_DATE, Integer.parseInt(split[0]));
		basicDBObject.append(Constants.FIELD_FLEET_REPORT_FLEET_ID, telematicsSettings.getFleetId());
		basicDBObject.append(Constants.FIELD_GROUP_ID, telematicsSettings.getGroupId());
		basicDBObject.append(Constants.FIELD_SUB_GROUP_ID, telematicsSettings.getSubGroupId());
		basicDBObject.append(Constants.FIELD_RISK_SCORE_TELEMATICS_ID, telematicsSettings.getTelematicsId());
		TelematicsDailyDrivingReport modelByQuery = null;
		AmberDailyDrivingReportMonitorStatus status = AmberDailyDrivingReportMonitorStatus.DAY;
		switch (status) {
		case DAY:
			modelByQuery = dailyDrivingReportLogic.getModelByQuery(basicDBObject);
			if (modelByQuery != null) {
				fleet_report_by_tyep.put(AmberDailyDrivingReportMonitorStatus.DAY, modelByQuery);
				writeToFile("TelematicsDailyDrivingReport Day: \n" + modelByQuery.toJson());

			}
		case WEEK:
			BasicDBObject search_query = null;
			TelematicsDailyDrivingReport from_db = null;
			BasicDBObject basicDBObject_document = null;
			if (modelByQuery != null) {
				search_query = dailyDrivingReportLogic.getSearchQuery(AmberDrivingReportType.WEEK, modelByQuery);
				from_db = dailyDrivingReportLogic.getModelByQuery(search_query);
				fleet_report_by_tyep.put(AmberDailyDrivingReportMonitorStatus.WEEK, from_db);
				writeToFile("TelematicsDailyDrivingReport Week: \n" + modelByQuery.toJson());

			}
		case MONTH:
			if (modelByQuery != null) {
				search_query = dailyDrivingReportLogic.getSearchQuery(AmberDrivingReportType.MONTH, modelByQuery);
				from_db = dailyDrivingReportLogic.getModelByQuery(search_query);
				fleet_report_by_tyep.put(AmberDailyDrivingReportMonitorStatus.MONTH, from_db);
				writeToFile("TelematicsDailyDrivingReport Month: \n" + modelByQuery.toJson());

			}
		case YEAR:
			if (modelByQuery != null) {
				search_query = dailyDrivingReportLogic.getSearchQuery(AmberDrivingReportType.YEAR, modelByQuery);
				from_db = dailyDrivingReportLogic.getModelByQuery(search_query);
				fleet_report_by_tyep.put(AmberDailyDrivingReportMonitorStatus.YEAR, from_db);
				writeToFile("TelematicsDailyDrivingReport Year: \n" + modelByQuery.toJson());

			}
		default:
			if (modelByQuery != null) {
				// remove(basicDBObject);
			}
			break;
		}

		return modelByQuery;
	}

	public static void main(String a[]) throws IOException {
		TelematicsTestDataPreparation dataPreparation = new TelematicsTestDataPreparation();
		dataPreparation.createFile();
		System.out.println("*** Telematics Correction ***");
		System.out.println("-----------------------------");
		System.out.println("Enter the Fleet ");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String fleet_id = reader.readLine();
		dataPreparation.writeToFile("Fleet : " + fleet_id);
		System.out.println("Enter the amber_token ( if multiple comma separated )  ");
		String token = reader.readLine();
		dataPreparation.writeToFile("Fleet : " + token);
		System.out.println("Enter date dd-MM-YYYY");
		String date = reader.readLine();
		System.out.println("Enter count to be reduced");
		int count = Integer.parseInt(reader.readLine());
		dataPreparation.writeToFile("date : " + date);
		dataPreparation.writeData(token, date, count);
	}
}
