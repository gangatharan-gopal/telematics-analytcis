package com.amber.correction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.amber.logic.DevicesLogic;
import com.amber.logic.FleetUsersLogic;
import com.amber.logic.TelematicsDailyDriverReportLogic;
import com.amber.logic.TelematicsDailyDrivingFleetReportLogic;
import com.amber.logic.TelematicsDailyDrivingReportLogic;
import com.amber.model.Devices;
import com.amber.model.TelematicsDailyDrivingReport;
import com.amber.util.UtilFunction;

public class CorrectionUtil {
	private DevicesLogic devicesLogic = new DevicesLogic();

	private void writeToFile(String data) {
		File file = new File("append.txt");
		try {
			FileWriter fr = new FileWriter(file, true);
			fr.write("\n" + data);
			fr.close();
		} catch (Exception e) {
			System.out.println("Failed to write to file" + e);
		}
	}

	private void createFile() throws IOException {
		File file = new File("append.txt");
		if (!file.exists()) {
			file.createNewFile();
		} else {
			file.delete();
			file.createNewFile();
		}
	}

	public void execute(String amber_auth_token, List<Date> dates) {
		getExistingDataBackup(amber_auth_token, dates);
		DevicesLogic devicesLogic = new DevicesLogic();
		TelematicsDailyDrivingReportLogic dailyDrivingReportLogic = new TelematicsDailyDrivingReportLogic();
		TelematicsDailyDrivingFleetReportLogic fleetReportLogic=new TelematicsDailyDrivingFleetReportLogic(); 
		
		Devices devices = devicesLogic.getModelByDeviceAuthenticationToken(amber_auth_token);
		if (devices != null) {
			String fleet_id = devices.getFleetId();
			FleetUsersLogic fleetUsersLogic = new FleetUsersLogic();
			Map<String, List<String>> allFleetUsersByFleetId = fleetUsersLogic
					.getAllFleetUsersByFleetId(devices.getFleetId());
			for (String group_id : allFleetUsersByFleetId.keySet()) {
				List<String> sub_group_ids = allFleetUsersByFleetId.get(group_id);
				for (String sub_group_id : sub_group_ids) {
					for (Date date : dates) {
						TelematicsDailyDrivingReport day_report = dailyDrivingReportLogic.rollBack(amber_auth_token, fleet_id, group_id, sub_group_id, date);
						fleetReportLogic.rollBack(day_report,fleet_id, group_id, sub_group_id, date);
					}
				}
			}
		}
	}

	private void getExistingDataBackup(String amber_auth_token, List<Date> dates) {
		TelematicsTestDataPreparation dataPreparation = new TelematicsTestDataPreparation();
		try {
			createFile();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		writeToFile("date : " + dates.toString());
		for (Date date : dates) {
			String dateMonthYear = UtilFunction.getDateMonthYear(date);
			dateMonthYear = dateMonthYear.replaceAll("_", "-");
			try {
				dataPreparation.writeData(amber_auth_token, dateMonthYear, 0);
			} catch (IOException e) {
				System.out.println("Failed to  write data, " + e);
			}
		}
	}

	public static void main(String a[]) throws IOException {
		/**
		 * DEV
		 * ---
		 * 
		 * Dev Fleet ID: 651187, 
		 * Amber Auth Token: RKAEXNIWV3P8
		 */
		CorrectionUtil telematicsCorrection = new CorrectionUtil();
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("*** Telematics Correction ***");
		System.out.println("-----------------------------");
		System.out.println("Enter Amber Auth Token ");
		//String amber_auth_token = reader.readLine();
		String amber_auth_token="RKAEXNIWV3P8";
		System.out.println("[1] By List of Dates(, separated). [2] Date Range ");
		System.out.println("Enter option ");
		//String option = reader.readLine();
		String option = "1";
		System.out.println("Date format dd-MM-yyyy [Ex. 20/12/1986]");		
		if (option.equals("1")) {
			System.out.println("Enter Dates ... ?");
			String dates_str = reader.readLine();
			String dates[] = dates_str.split(",");
			List<Date> dates_list = UtilFunction.getDateFromString(dates);
			telematicsCorrection.execute(amber_auth_token, dates_list);
		} else if (option.equals("2")) {
			System.out.println("Start Date ?");
			String start_date = reader.readLine();
			System.out.println("End Date ?");
			String end_date = reader.readLine();
			List<Date> dates_list = UtilFunction.getListOfDatesBetweenTwoDate(start_date, end_date);
			telematicsCorrection.execute(amber_auth_token, dates_list);
		}
	}
}
